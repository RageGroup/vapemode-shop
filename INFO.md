# Creating a web site using node.js

## Templating

```sh
npm install --save handlebars
npm install -g handlebars
yarn add handlebars
yarn global add handlebars
```

```js
var Handlebars = require('handlebars');
var Handlebars = require('handlebars/runtime');

var source = '<div class="entry"><h1>{{title}}</h1><div class="body">{{body}}</div></div>';
var template = Handlebars.compile(source);

var context = {title: "My New Post", body: "This is my first post!"};
var html    = template(context);

var compiled = Handlebars.precompile(personTemplate);
Handlebars.templates.person(context, options); // usage of precompiled file 'person.handlebars'
```

## Translation

See: https://ctrlq.org/code/19909-google-translate-api

## Columns & Fields

Text table: own columns, no fields

| Case                          | Base table/class          | Table/Class                           |
|-------------------------------|---------------------------|---------------------------------------|
| without base table            |                           | own columns, own fields               |
| with base table, same table   | no columns, own fields    | own & base columns, own fields        |
| with base table, new table    | own columns, own fields   | own & base columns, own fields        |

## Counting Lines of Code

* cloc --file-encoding=utf-8 --exclude-dir=node_modules --by-percent c .
