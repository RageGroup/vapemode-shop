const UTIL = require('../util');
const ConstraintDefinition = require('./ConstraintDefinition.js');
const ForeignKeyColumnDefinition = require('./ForeignKeyColumnDefinition.js');

module.exports = class ForeignKeyDefinition extends ConstraintDefinition {

  get name() { return this._name; }
  get referencedTableName() { return this._referencedTableName; }
  get columns() { return this._columns; }

  constructor(name, tableName, referencedTableName, constraintNameSuffix) {
    super(tableName + constraintNameSuffix + 'Fk');
    this._name = name;
    this._referencedTableName = referencedTableName;
    this._columns = [];
  }

  addColumn(columnName, referencedColumnName) {
    this._columns.push(new ForeignKeyColumnDefinition(columnName, referencedColumnName));
  }

  toString() {
    return `Foreign Key "${this.constraintName}"`;
  }

}
