const UTIL = require('../util');
const WrapperIterable = require('../util/WrapperIterable.js');
const QualifiedIterable = require('../util/QualifiedIterable.js');
const PrimaryKeyDefinition = require('./PrimaryKeyDefinition.js');
const UniqueKeyDefinition = require('./UniqueKeyDefinition.js');
const CheckDefinition = require('./CheckDefinition.js');

/**
 * Represents a database model.
 */
module.exports = class DbModel {

  get codeLanguage() { return this._codeLanguage; }
  get dbSystemCode() { return this._dbSystemCode; }
  get modelName() { return this._modelName; }
  get modelNamespace() { return this._modelNamespace; }

  set codeLanguage(value) { this._codeLanguage = value; }
  set dbSystemCode(value) { this._dbSystemCode = value; }
  set modelName(value) { this._modelName = value; }
  set modelNamespace(value) { this._modelNamespace = value; }

  /**
   * Get the tables for this model.
   */
  get tables() {
    let tables = new WrapperIterable(this, function*() {
      for (let entity of this._entities) {
        // Only non-replaced entities get a table.
        if (entity.replacingTable == null) {
          if (entity.baseTable == null ||
              entity.baseTable.replacingTable == null)
          {
            yield entity;
            if (entity.textTable != null) {
              yield entity.textTable;
            }
          }
        }
        else {
          yield entity.replacingTable;
        }
      }
    });
    return [...tables];
    // return tables;
  }

  /**
   * Get the classes for this model.
   */
  get classes() {
    let classes = new WrapperIterable(this, function*() {
      for (let entity of this._entities) {
        // Only text entities get no seperate class.
        if (!entity.isTextEntity) {
          yield entity;
        }
      }
    });
    return [...classes];
    // return classes;
  }

  constructor(codeLanguage, dbSystemCode, modelName, modelNamespace, entities) {
    this._codeLanguage = UTIL.coalesce(codeLanguage);
    this._dbSystemCode = UTIL.coalesce(dbSystemCode);
    this._modelName = UTIL.coalesce(modelName);
    this._modelNamespace = UTIL.coalesce(modelNamespace);
    this._entities = [];
    this._entitiesByClassNamespace = new Map();
    if (entities) {
      entities.forEach((v, i, a) => this.addEntity(v));
    }
  }

  getEntitiesByClassNamespace(classNamespace) {
    let entities = this._entitiesByClassNamespace.get(classNamespace)
    if (!UTIL.isNull(entities)) {
      return entities;
    }
    return [];
  }

  addEntity(entity) {
    let namespace = UTIL.coalesce(entity.classNamespace, this._modelNamespace);
    if (this._entitiesByClassNamespace.has(namespace)) {
      this._entitiesByClassNamespace.get(namespace).push(entity);
    }
    else {
      this._entitiesByClassNamespace.set(namespace, [entity]);
    }
    this._entities.push(entity);
  }

  toString() { return `DbModel`; }

}
