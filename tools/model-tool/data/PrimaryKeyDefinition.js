const UTIL = require('../util');
const ConstraintDefinition = require('./ConstraintDefinition.js');

module.exports = class PrimaryKeyDefinition extends ConstraintDefinition {

  get columns() { return this._columns; }

  constructor(tableName, columns) {
    super(tableName + "Pk");
    // console.log(JSON.stringify(columns));
    this._columns = UTIL.coalesce(columns, []);
  }

  addColumn(column) {
    this._columns.push(column);
  }

  toString() { return `Primary Key "${this.constraintName}"`; }

}
