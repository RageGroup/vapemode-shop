const FS = require('fs');
const PATH = require('path');
const HANDLEBARS = require('./handlebars-helpers.js')(require('handlebars'));

const DbDataType = require('../DbDataType.js');
const ObjectMappingGenerator = require('./ObjectMappingGenerator.js');

module.exports = class PhpMappingGenerator extends ObjectMappingGenerator {

  get fileExtension() { return 'php'; }

  constructor() {
    super();
    this.contextTpl = HANDLEBARS.compile(
      FS.readFileSync(
        PATH.join(__dirname, './templates/PhpContext.php.handlebars'),
        ObjectMappingGenerator.fileOptions()).toString());
    this.tableTpl = HANDLEBARS.compile(
      FS.readFileSync(
        PATH.join(__dirname, './templates/PhpTable.php.handlebars'),
        ObjectMappingGenerator.fileOptions()).toString());
    this.rowTpl = HANDLEBARS.compile(
      FS.readFileSync(
        PATH.join(__dirname, './templates/PhpRow.php.handlebars'),
        ObjectMappingGenerator.fileOptions()).toString());
  }

  writeContextClass(writer, model) {
    let userTable = null;
    for (let table of model.tables) {
      if (table.tableName == 'Users') {
        userTable = table;
      }
    }
    if (userTable == null) {
      throw new Error('No user table could be identified.');
    }
    writer.write(this.contextTpl({
      'generator': this,
      'model': model,
      'userTable': userTable
    }));
  }

  writeTableClass(writer, contextName, table) {
    writer.write(this.tableTpl({
      'generator': this,
      'contextName': contextName,
      'table': table
    }));
  }

  writeRowClass(writer, table) {
    writer.write(this.rowTpl({
      'generator': this,
      'table': table
    }));
  }

  static getDataTypePascalCase(column) {
    return PhpMappingGenerator.getDataType(column, false);
  }

  static getDataTypeCamelCase(column) {
    return PhpMappingGenerator.getDataType(column, true);
  }

  static getDataType(column, isLower) {
    let dataType = '';
    switch (column.dataType) {
      case DbDataType.UUID:
        dataType = 'Uuid';
        break;
      case DbDataType.I8:
      case DbDataType.I16:
      case DbDataType.I32:
        dataType = isLower ? 'int' : 'Int';
        break;
      case DbDataType.N:
      case DbDataType.F32:
      case DbDataType.F64:
        dataType = isLower ? 'float' : 'Float';
        break;
      case DbDataType.D:
      case DbDataType.DT:
      case DbDataType.TS:
        dataType = 'DateTime';
        break;
      case DbDataType.T:
        dataType = 'Time';
        break;
      case DbDataType.VB:
      case DbDataType.B:
      case DbDataType.VC:
      case DbDataType.C:
      case DbDataType.LVC:
      case DbDataType.LC:
        dataType = isLower ? 'string' : 'String';
        break;
      default:
        dataType = '';
        break;
    }
    return dataType;
  }

  static escape(str) {
    // console.log(str);
    if (str) {
      return str.replace(/'/g, "\\'");
    }
    return str;
  }

  toString() { return 'PhpMappingGenerator'; }

}
