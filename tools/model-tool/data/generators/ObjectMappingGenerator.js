module.exports = class ObjectMappingGenerator {

  static fileOptions() { return { encoding: 'utf-8' }; }

  get fileExtension() { return ''; }

  constructor() {}

  writeContextClass(writer, contextName, tables) {}
  writeTableClass(writer, contextName, table) {}
  writeRowClass(writer, table) {}

}
