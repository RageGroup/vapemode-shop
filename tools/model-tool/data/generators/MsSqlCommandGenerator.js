const FS = require('fs');
const PATH = require('path');
const HANDLEBARS = require('./handlebars-helpers.js')(require('handlebars'));

const SqlCommandGenerator = require('./SqlCommandGenerator.js');

module.exports = class MsSqlCommandGenerator extends SqlCommandGenerator {

  constructor(writer) {
    super(writer);
    engine.setProperty(RuntimeConstants.RESOURCE_LOADER, 'classpath');
    engine.setProperty('classpath.resource.loader.class', ClasspathResourceLoader.class.getName());
    engine.init();
  }

  getCreateDatabaseCommands(dbName, tables) {
    let modelTpl = 'com/vapemode/model_tool/data/generators/templates/MsSqlModel.sql.vm';
    try {
      let context = new VelocityContext();
      context.put('generator', this);
      context.put('dbName', dbName);
      context.put('tables', tables);

      let template = this.engine.getTemplate(modelTpl, encoding);
      template.merge(context, this.writer);
    }
    catch (e) {
    }
  }

  getInsertRowCommands(dbName, tableContents) {
    let dataTpl = 'com/vapemode/model_tool/data/generators/templates/MsSqlData.sql.vm';
    try {
      let context = new VelocityContext();
      context.put('generator', this);
      context.put('dbName', dbName);
      context.put('tableContents', tableContents);

      let template = this.engine.getTemplate(modelTpl, encoding);
      template.merge(context, this.writer);
    }
    catch (e) {
      console.log(e.getMessage());
      console.log(e.getStackTrace());
    }
  }

  getObjectTypeCode(dbObject) {
    if (dbObject instanceof EntityDefinition) {
      return 'U';
    }
    return '';
  }

  getDataType(column) {
    let dataType = '';
    switch (column.getDataType()) {
      case UUID:
        dataType = 'binary(16)';
        break;
      case I8:
        dataType = 'tinyint';
        break;
      case I16:
        dataType = 'smallint';
        break;
      case I32:
        dataType = 'int';
        break;
      case I64:
        dataType = 'bigint';
        break;
      case UI8:
        dataType = 'unsigned tinyint';
        break;
      case UI16:
        dataType = 'unsigned smallint';
        break;
      case UI32:
        dataType = 'unsigned int';
        break;
      case UI64:
        dataType = 'unsigned bigint';
        break;
      case F32:
        dataType = 'float';
        break;
      case F64:
        dataType = 'double';
        break;
      case N:
        dataType = `decimal(${column.getLength()}, ${column.getPrecision()})`;
        break;
      case D:
        dataType = 'date';
        break;
      case DT:
        dataType = 'datetime';
        break;
      case T:
        dataType = 'time';
        break;
      case B:
        dataType = `binary(${column.getLength()})`;
        break;
      case VB:
        dataType = `varbinary(${column.getLength()})`;
        break;
      case C:
        dataType = `char(${column.getLength()})`;
        break;
      case VC:
        dataType = `varchar(${column.getLength()})`;
        break;
      default:
        dataType = '';
    }
    return StringUtils.rightPad(dataType, this.maxDataTypeLen);
  }

  toString() { return 'MsSqlCommandGenerator'; }

}
