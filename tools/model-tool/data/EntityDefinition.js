const UTIL = require('../util');
const WrapperIterable = require('../util/WrapperIterable.js');
const DbDataType = require('./DbDataType.js');
const EntityAttributeDefinition = require('./EntityAttributeDefinition.js');
const PrimaryKeyDefinition = require('./PrimaryKeyDefinition.js');
const ForeignKeyDefinition = require('./ForeignKeyDefinition.js');
const UniqueKeyDefinition = require('./UniqueKeyDefinition.js');
const CheckDefinition = require('./CheckDefinition.js');

/**
 * Represents the definition of an entity.
 */
module.exports = class EntityDefinition {

  get tableName() { return this._tableName; }
  get tableNameCamelCase() { return this._tableNameCamelCase; }
  get className() { return this._className; }
  get classNamespace() { return this._classNamespace; }
  get baseTable() { return this._baseTable; }
  get baseTableName() { return this._baseTableName; }
  get baseClassName() { return this._baseClassName; }
  get baseClassNamespace() { return this._baseClassNamespace; }
  get replacingTable() { return this._replacingTable; }
  get singularDescription() { return this._singularDescription; }
  get pluralDescription() { return this._pluralDescription; }
  get localizedSingularDescriptions() { return this._localizedSingularDescriptions; }
  get localizedPluralDescriptions() { return this._localizedPluralDescriptions; }
  get textTable() { return this._textTable; }
  get attributes() { return this._attributes; }
  get primaryKey() { return this._primaryKey; }
  get foreignKeys() { return [...this._foreignKeysByName.values()]; }
  get uniqueKeys() { return [...this._uniqueKeysByName.values()]; }
  get checks() { return this._checks; }

  /**
   * Get the table constraints for this entity.
   */
  get constraints() {
    let constraints = new WrapperIterable(this, function*() {
      yield this.primaryKey;
      for (let constraint of this._uniqueKeysByName.values()) {
        yield constraint;
      }
      for (let constraint of this.foreignKeys) {
        yield constraint;
      }
      for (let constraint of this._checks) {
        yield constraint;
      }
    });
    return [...constraints];
    // return constraints;
  }

  /**
   * The table columns for this entity.
   */
  get columns() {
    let columns = new WrapperIterable(this, function*() {
      if (this._baseTable != null) {
        for (let column of this._baseTable.columns) {
          yield column;
        }
      }
      for (let attribute of this._attributes) {
        if (this.isTextEntity || (
            attribute.dataType != DbDataType.LC &&
            attribute.dataType != DbDataType.LVC))
        {
          yield attribute;
        }
      }
    });
    return [...columns];
    // return columns;
  }

  /**
   * The table columns for this entity by their names.
   */
  get columnsByName() {
    let columnsByName = new Map();
    for (let column of this.columns) {
      columnsByName.set(column.columnName, column);
    }
    return columnsByName;
  }

  /**
   * The public, mandatory table columns for this entity.
   */
  get publicMandatoryColumns() {
    let publicMandatoryColumns = new WrapperIterable(this, function*() {
      for (let column of this.columns) {
        if (!column.isOptional && !column.isPrivate) {
          yield column;
        }
      }
    });
    return [...publicMandatoryColumns];
    // return publicMandatoryColumns;
  }

  /**
   * The public, mandatory, changable table columns for this entity.
   */
  get publicMandatoryChangableColumns() {
    let publicMandatoryChangableColumns = new WrapperIterable(this, function*() {
      for (let column of this.columns) {
        if (!column.isOptional && !column.isPrivate && (column.isForeignKey ||
          !(column.isPrimaryKey && column.dataType == DbDataType.UUID)))
        {
          yield column;
        }
      }
    });
    return [...publicMandatoryChangableColumns];
    // return publicMandatoryChangableColumns;
  }

  /**
   * The class fields for this entity.
   */
  get fields() {
    let fields = new WrapperIterable(this, function*() {
      if (!this.isTextEntity) {
        for (let attribute of this._attributes) {
          yield attribute;
        }
      }
    });
    return [...fields];
    // return fields;
  }

  get nonPrimaryKeyColumns() {
    if (this._primaryKey == null) {
      return this._attributes;
    }
    let cols = [];
    for (let col of this._attributes) {
      if (!this._primaryKey.columns.contains(col)) {
        cols.push(col);
      }
    }
    return cols;
  }

  get hasBaseTable() { return this._baseTable != null; }

  get hasBaseTableName() {
    return !UTIL.isNullOrWhiteSpace(this._baseTableName);
  }

  get hasBaseClassName() {
    return !UTIL.isNullOrWhiteSpace(this._baseClassName);
  }

  get hasBaseClassNamespace() {
    return !UTIL.isNullOrWhiteSpace(this._baseClassNamespace);
  }

  get isReplaced() { return this._replacingTable != null; }
  get hasLocalizedText() { return this._textTable != null; }
  get isTextEntity() { return this._className.substr(-4, 4) == 'Text'; }

  constructor(tableName, className, classNamespace,
    baseTable, baseTableName, baseClassName, baseClassNamespace,
    singularDescription, pluralDescription,
    localizedSingularDescriptions = new Map(),
    localizedPluralDescriptions = new Map(),
    attributes = [])
  {
    this._tableName = tableName;
    this._tableNameCamelCase = UTIL.uncapitalize(tableName);
    this._className = className;
    this._classNamespace = classNamespace;
    this._baseTable = UTIL.coalesce(baseTable, null);
    this._baseTableName = UTIL.coalesce(baseTableName, null);
    this._baseClassName = UTIL.coalesce(baseClassName, null);
    this._baseClassNamespace = UTIL.coalesce(baseClassNamespace, null);
    this._replacingTable = null;
    this._singularDescription = singularDescription;
    this._pluralDescription = pluralDescription;
    this._localizedSingularDescriptions = localizedSingularDescriptions;
    this._localizedPluralDescriptions = localizedPluralDescriptions;
    this._textTable = null;
    this._attributes = [];
    this._primaryKey = new PrimaryKeyDefinition(tableName);
    this._foreignKeysByName = new Map();
    this._uniqueKeysByName = new Map();
    this._checks = [];

    if (this._baseTable != null) {
      if (this._baseTable._tableName == this._tableName) {
        this._baseTable._replacingTable = this;
      }
      for (let column of this._baseTable.primaryKey.columns) {
        this.primaryKey.addColumn(column);
      }
    }

    if (attributes) {
      for (let attribute of attributes) {
        this.addAttribute(attribute);
      }
    }
  }

  addAttribute(attribute) {
    this._attributes.push(attribute);
    if (attribute.isPrimaryKey) {
      this._primaryKey.addColumn(attribute);
      if (this._textTable != null) {
        this._textTable.addAttribute(attribute);
      }
    }
    if (attribute.isForeignKey) {
      let fkn = UTIL.coalesce(attribute.foreignKeyName, attribute.foreignKeyTable);
      if (!this._foreignKeysByName.has(fkn)) {
        this._foreignKeysByName.set(
          fkn,
          new ForeignKeyDefinition(
            attribute.foreignKeyName,
            this.tableName,
            attribute.foreignKeyTable,
            UTIL.coalesce(attribute.foreignKeyName,
                          attribute.columnName.substring(0, attribute.columnName.length - 2))));
      }
      this._foreignKeysByName.get(fkn).
        addColumn(attribute.columnName, attribute.foreignKeyColumn);
    }
    if (!UTIL.isNullOrWhiteSpace(attribute.uniqueKey)) {
      let uniqueKey;
      if (!this._uniqueKeysByName.has(attribute.uniqueKey)) {
        uniqueKey = new UniqueKeyDefinition(attribute.uniqueKey, this.tableName);
        this._uniqueKeysByName.set(attribute.uniqueKey, uniqueKey);
      } else {
        uniqueKey = this._uniqueKeysByName.get(attribute.uniqueKey);
      }
      uniqueKey.addColumn(attribute);
    }
    if (!UTIL.isNullOrWhiteSpace(attribute.checks)) {
      this._checks.push(new CheckDefinition(this.tableName, attribute.columnName, attribute.checks));
    }

    // It's a localizable column,
    // that has to be outsourced into the associated text table.
    if (!this.isTextEntity &&
      (attribute.dataType == DbDataType.LC || attribute.dataType == DbDataType.LVC))
    {
      if (this._textTable == null) {
        this._textTable = new EntityDefinition(`${this._className}Texts`,
          `${this._className}Text`, this._classNamespace,
          null, null, null, null,
          `${this._singularDescription} Text`,
          `${this._singularDescription} Texts`);
        let c = 1;
        for (let column of this._primaryKey.columns) {
          // this._textTable.addAttribute(column);
          this._textTable.addAttribute(new EntityAttributeDefinition(
            c++, column.columnName, column.description,
            column.dataType, column.length, column.precision,
            false, false, true, null, null, this._tableName, column.columnName));
        }
        this._textTable.addAttribute(new EntityAttributeDefinition(
          1, 'LanguageCode', 'Language Code',
          DbDataType.C, 2, 0, false, false, true,
          null, null, 'Languages', 'LanguageCode'));
      }
      this._textTable.addAttribute(attribute);
    }
  }

  toString() { return `Table '${this.tableName}'`; }

}
