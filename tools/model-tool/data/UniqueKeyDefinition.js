const ConstraintDefinition = require('./ConstraintDefinition.js');

module.exports = class UniqueKeyDefinition extends ConstraintDefinition {

  get columns() { return this._columns; }

  constructor(constraintName, tableName) {
      super(tableName + constraintName + "Uq");
      this._columns = [];
      this._columnsByName = new Map();
  }

  addColumn(column) {
    this._columns.push(column);
    this._columnsByName.set(column.columnName, column);
  }

  hasColumn(columnName) { return this._columnsByName.has(columnName); }

  toString() { return `Unique Key "${this.constraintName}"`; }

}
