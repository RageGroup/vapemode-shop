const FS = require('fs');
const PATH = require('path');
const XLSX = require('xlsx');

const UTIL = require('./util');
const FileProcessor = require('./FileProcessor.js');
const ExcelTableRowIterator = require('./ExcelTableRowIterator.js');
const TableCellContent = require('./data/TableCellContent.js');
const TableRowContent = require('./data/TableRowContent.js');
const TableContent = require('./data/TableContent.js');
const GENERATORS = require('./data/generators');

module.exports = class DataProcessor extends FileProcessor {

  constructor(modelProcessor, baseDataFilePath, dataFilePath, sqlFileName) {
    super();
    this._baseDataFilePath = baseDataFilePath;
    this._dataFilePath = dataFilePath;
    this._modelProcessor = modelProcessor;
    this._sqlFileName = sqlFileName;
    this._tableContents = [];
  }

  process() {
    this.getData(this._baseDataFilePath);
    this.getDataFromTableDefinitions();
    this.getData(this._dataFilePath);
    this.createSqlFiles(this._modelProcessor.sourceDirPath, this._sqlFileName);
  }

  /**
   * Get the table data from the table definitions.
   */
  getDataFromTableDefinitions() {
    let columnsByName = null;
    // Table "Tables"
    let tables = null;
    let tablesTableName = null;
    // Table "TableTexts"
    let tableTexts = null;
    let tableTextsTableName = null;
    let tableTextsLanguageCode = null;
    let tableTextsTableDesc = null;
    let tableTextsClassDesc = null;
    // Table "Columns"
    let columns = null;
    let columnsTableName = null;
    let columnsColumnName = null;
    // Table "ColumnTexts"
    let columnTexts = null;
    let columnTextsTableName = null;
    let columnTextsColumnName = null;
    let columnTextsLanguageCode = null;
    let columnTextsColumnDesc = null;

    // Get the table definition for the tables containing the definitions.
    for (let tableDefinition of this._modelProcessor.model.tables) {
      switch (tableDefinition.tableName) {
        case 'Tables':
          columnsByName = tableDefinition.columnsByName;
          tablesTableName = columnsByName.get('TableName');
          tables = new TableContent(tableDefinition, [tablesTableName]);
          break;
        case 'TableTexts':
          columnsByName = tableDefinition.columnsByName;
          tableTextsTableName = columnsByName.get('TableName');
          tableTextsLanguageCode = columnsByName.get('LanguageCode');
          tableTextsTableDesc = columnsByName.get('TableDescription');
          tableTextsClassDesc = columnsByName.get('ClassDescription');
          tableTexts = new TableContent(tableDefinition, [
            tableTextsTableName, tableTextsLanguageCode,
            tableTextsTableDesc, tableTextsClassDesc]);
          break;
        case 'Columns':
          columnsByName = tableDefinition.columnsByName;
          columnsTableName = columnsByName.get('TableName');
          columnsColumnName = columnsByName.get('ColumnName');
          columns = new TableContent(tableDefinition, [
            columnsTableName, columnsColumnName]);
          break;
        case 'ColumnTexts':
          columnsByName = tableDefinition.columnsByName;
          columnTextsTableName = columnsByName.get('TableName');
          columnTextsColumnName = columnsByName.get('ColumnName');
          columnTextsLanguageCode = columnsByName.get('LanguageCode');
          columnTextsColumnDesc = columnsByName.get('ColumnDescription');
          columnTexts = new TableContent(tableDefinition, [
            columnTextsTableName, columnTextsColumnName,
            columnTextsLanguageCode, columnTextsColumnDesc]);
          break;
        default:
          break;
      }
    }

    if (tables != null && tableTexts != null &&
        columns != null && columnTexts != null)
    {
      for (let tableDefinition of this._modelProcessor.model.tables) {
        if (!tableDefinition.isTextEntity) {
          tables.rows.push(new TableRowContent(
            tables.tableDefinition,
            tables.columnDefinitions, [
              new TableCellContent(tablesTableName, tableDefinition.tableName)]));
          for (let languageCode of tableDefinition.localizedSingularDescriptions.keys()) {
            tableTexts.rows.push(new TableRowContent(
              tableTexts.tableDefinition,
              tableTexts.columnDefinitions, [
                new TableCellContent(tableTextsTableName, tableDefinition.tableName),
                new TableCellContent(tableTextsLanguageCode, languageCode),
                new TableCellContent(tableTextsTableDesc, tableDefinition.localizedPluralDescriptions.get(languageCode)),
                new TableCellContent(tableTextsClassDesc, tableDefinition.localizedSingularDescriptions.get(languageCode))]));
          }
          for (let column of tableDefinition.columns) {
            columns.rows.push(new TableRowContent(
              columns.tableDefinition,
              columns.columnDefinitions, [
                new TableCellContent(columnsTableName, tableDefinition.tableName),
                new TableCellContent(columnsColumnName, column.columnName)]));
            for (let languageCode of column.localizedDescriptions.keys()) {
              columnTexts.rows.push(new TableRowContent(
                columnTexts.tableDefinition,
                columnTexts.columnDefinitions, [
                  new TableCellContent(columnTextsTableName, tableDefinition.tableName),
                  new TableCellContent(columnTextsColumnName, column.columnName),
                  new TableCellContent(columnTextsLanguageCode, languageCode),
                  new TableCellContent(columnTextsColumnDesc, column.localizedDescriptions.get(languageCode))]));
            }
          }
          if (tableDefinition.hasLocalizedText) {
            tables.rows.push(new TableRowContent(
              tables.tableDefinition,
              tables.columnDefinitions, [
                new TableCellContent(tablesTableName, tableDefinition._textTable.tableName)]));
            for (let column of tableDefinition._textTable.columns) {
              columns.rows.push(new TableRowContent(
                columns.tableDefinition,
                columns.columnDefinitions, [
                  new TableCellContent(columnsTableName, tableDefinition._textTable.tableName),
                  new TableCellContent(columnsColumnName, column.columnName)]));
              if (column.isLocalized) {
                for (let languageCode of column.localizedDescriptions.keys()) {
                  columnTexts.rows.push(new TableRowContent(
                    columnTexts.tableDefinition,
                    columnTexts.columnDefinitions, [
                      new TableCellContent(columnTextsTableName, tableDefinition.textTable.tableName),
                      new TableCellContent(columnTextsColumnName, column.columnName),
                      new TableCellContent(columnTextsLanguageCode, languageCode),
                      new TableCellContent(columnTextsColumnDesc, column.localizedDescriptions.get(languageCode))]));
                }
              }
            }
          }
        }
      }
      if (tables.rows.length > 0) { this._tableContents.push(tables); }
      if (tableTexts.rows.length > 0) { this._tableContents.push(tableTexts); }
      if (columns.rows.length > 0) { this._tableContents.push(columns); }
      if (columnTexts.rows.length > 0) { this._tableContents.push(columnTexts); }
    }
  }

  /**
   * Get the data contained in the excel file with the given path.
   */
  getData(dataFilePath) {
    let workbook = XLSX.readFile(dataFilePath);

    for (let tableDefinition of this._modelProcessor.model.tables) {
      if (!tableDefinition.isTextEntity) {
        let excelTable = new ExcelTableRowIterator(workbook.Sheets[tableDefinition.tableName]);
        let columnsByName = tableDefinition.columnsByName;
        let locColumnsByName = null;
        let colsByIndex = new Map();
        let locColsByIndex = new Map();
        if (excelTable.isLocalized) {
          locColumnsByName = tableDefinition.textTable.columnsByName;
          for (let languageCode of excelTable.usedLanguageCodes) {
            let lc = new Map();
            locColsByIndex.set(languageCode, lc);
            lc.set(-1, locColumnsByName.get('LanguageCode'));
          }
        }

        // Determine the columns to use.
        for (let cx = 0; cx < excelTable.columnCount; cx++) {
          let columnName = excelTable.columnNames[cx];
          if (columnsByName.has(columnName)) {
            let column = columnsByName.get(columnName);
            colsByIndex.set(cx, column);
            if (excelTable.isLocalized && column.isPrimaryKey) {
              // Primary key columns have to be used for texts too.
              for (let languageCode of excelTable.usedLanguageCodes) {
                locColsByIndex.get(languageCode).set(cx, locColumnsByName.get(columnName));
              }
            }
          }
          else if (excelTable.isLocalized && locColumnsByName.has(columnName)) {
            let languageCode = excelTable.languageCodes[cx];
            locColsByIndex.get(languageCode).set(cx, locColumnsByName.get(columnName));
          }
        }
        let cols = [...colsByIndex.values()];
        let locCols = [];
        let rows = [];
        let locRows = new Map();
        if (excelTable.isLocalized) {
          for (let languageCode of excelTable.usedLanguageCodes) {
            if (locCols.length == 0) {
              locCols = [...locColsByIndex.get(languageCode).values()];
            }
            locRows.set(languageCode, []);
          }
        }

        // Select the data.
        for (let excelRow of excelTable) {
          let value = excelRow.getValueString(0);
          let cells = [];
          let locCells = null;
          if (excelTable.isLocalized) {
            locCells = new Map();
            for (let languageCode of excelTable.usedLanguageCodes) {
              let lc = [new TableCellContent(locColumnsByName.get('LanguageCode'), languageCode)];
              locCells.set(languageCode, lc);
            }
          }
          for (let cx = 0; cx < excelTable.columnCount; cx++) {
            if (colsByIndex.has(cx)) {
              let column = colsByIndex.get(cx)
              let cellValue = excelTable.getValueString(cx)
              cells.push(new TableCellContent(column, cellValue));
              if (column.isPrimaryKey) {
                for (let languageCode of excelTable.usedLanguageCodes) {
                  if (locColsByIndex.get(languageCode).has(cx)) {
                    locCells.get(languageCode).
                      push(new TableCellContent(locColsByIndex.get(languageCode).get(cx), cellValue));
                  }
                }
              }
            }
            else {
              let languageCode = excelTable.languageCodes[cx];
              if (languageCode != null) {
                if (locColsByIndex.get(languageCode).has(cx)) {
                  locCells.get(languageCode).
                    push(new TableCellContent(locColsByIndex.get(languageCode).get(cx), excelTable.getValueString(cx)));
                }
              }
              else {
                for (let languageCode of excelTable.usedLanguageCodes) {
                  if (locColsByIndex.get(languageCode).has(cx)) {
                    locCells.get(languageCode).
                      push(new TableCellContent(locColsByIndex.get(languageCode).get(cx), excelTable.getValueString(cx)));
                  }
                }
              }
            }
          }
          rows.push(new TableRowContent(tableDefinition, cols, cells));
          if (excelTable.isLocalized) {
            for (let languageCode of excelTable.usedLanguageCodes) {
              locRows.get(languageCode).
                push(new TableRowContent(tableDefinition.textTable,
                                         locCols,
                                         locCells.get(languageCode)));
            }
          }
        }
        if (rows.length > 0) {
          this._tableContents.push(new TableContent(tableDefinition, cols, rows));
        }
        if (excelTable.isLocalized) {
          let lr = [];
          for (let languageCode of excelTable.usedLanguageCodes) {
            lr = lr.concat(locRows.get(languageCode));
          }
          this._tableContents.push(new TableContent(tableDefinition.textTable, locCols, lr));
        }
      }
    }
  }

  /**
   * Create the sql files with the available table contents.
   */
  createSqlFiles(sourceDirPath, sqlFileName) {
    let sqlFilePath = PATH.join(__dirname, sourceDirPath, 'sql', sqlFileName);
    if (!UTIL.isNullOrWhiteSpace(sqlFilePath)) {
      sqlFilePath = PATH.normalize(sqlFilePath);
      let writer = FS.createWriteStream(sqlFilePath, FileProcessor.fileOptions());
      let sqlGenerator = GENERATORS.forSqlCommands(this._modelProcessor.dbSystemCode);
      sqlGenerator.getInsertRowCommands(writer, this._modelProcessor.modelName, this._tableContents);
      writer.end();
    }
  }

}
