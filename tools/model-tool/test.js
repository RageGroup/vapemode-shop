const MySqlCommandGenerator = require('./data/generators/MySqlCommandGenerator.js');
const Flags = require('./util/Flags.js');

const TestFlags = Flags.define('FIRST', 'SECOND', 'THIRD', 'FOURTH');
console.log(TestFlags.values);
let testFlagsValue = TestFlags.SECOND;
console.log('testFlagsValue.name:                       ' + testFlagsValue.name);
console.log('testFlagsValue.id:                         ' + testFlagsValue.id);
console.log('testFlagsValue.hasFlag(TestFlags.FIRST):   ' + testFlagsValue.hasFlag(TestFlags.FIRST));
console.log('testFlagsValue.hasFlag(TestFlags.SECOND):  ' + testFlagsValue.hasFlag(TestFlags.SECOND));
console.log('testFlagsValue.hasFlag(TestFlags.THIRD):   ' + testFlagsValue.hasFlag(TestFlags.THIRD));
console.log('testFlagsValue.hasFlag(TestFlags.FOURTH):  ' + testFlagsValue.hasFlag(TestFlags.FOURTH));
testFlagsValue = testFlagsValue.add(TestFlags.FOURTH);
console.log('testFlagsValue.name:                       ' + testFlagsValue.name);
console.log('testFlagsValue.id:                         ' + testFlagsValue.id);
console.log('testFlagsValue.hasFlag(TestFlags.FIRST):   ' + testFlagsValue.hasFlag(TestFlags.FIRST));
console.log('testFlagsValue.hasFlag(TestFlags.SECOND):  ' + testFlagsValue.hasFlag(TestFlags.SECOND));
console.log('testFlagsValue.hasFlag(TestFlags.THIRD):   ' + testFlagsValue.hasFlag(TestFlags.THIRD));
console.log('testFlagsValue.hasFlag(TestFlags.FOURTH):  ' + testFlagsValue.hasFlag(TestFlags.FOURTH));
testFlagsValue = testFlagsValue.remove(TestFlags.SECOND);
console.log('testFlagsValue.name:                       ' + testFlagsValue.name);
console.log('testFlagsValue.id:                         ' + testFlagsValue.id);
console.log('testFlagsValue.hasFlag(TestFlags.FIRST):   ' + testFlagsValue.hasFlag(TestFlags.FIRST));
console.log('testFlagsValue.hasFlag(TestFlags.SECOND):  ' + testFlagsValue.hasFlag(TestFlags.SECOND));
console.log('testFlagsValue.hasFlag(TestFlags.THIRD):   ' + testFlagsValue.hasFlag(TestFlags.THIRD));
console.log('testFlagsValue.hasFlag(TestFlags.FOURTH):  ' + testFlagsValue.hasFlag(TestFlags.FOURTH));

let HANDLEBARS = require('handlebars');

HANDLEBARS.registerHelper('call', function(obj, methodName, arg1, arg2, arg3) {
  // console.log('----------');
  // console.log('obj: ' + JSON.stringify(obj));
  // console.log('methodName: ' + methodName);
  // console.log('arg1: ' + arg1);
  // console.log('arg2: ' + arg2);
  // console.log('arg3: ' + arg3);
  if (obj[methodName] != undefined &&
    typeof obj[methodName] == 'function')
  {
    return obj[methodName](arg1, arg2, arg3);
    // return obj[methodName]();
  }
  else if (obj['prototype'] != undefined &&
    obj.prototype[methodName] != undefined &&
    typeof obj.prototype[methodName] == 'function')
  {
    return obj.prototype[methodName](arg1, arg2, arg3);
    // return obj.prototype[methodName]();
  }
  return '';
});

// HANDLEBARS.registerHelper('call', function(method) {
//   console.log('this: ' + JSON.stringify(this));
//   console.log('method: ' + method);
//   if (typeof method == "function") {
//     return method.apply(this);
//   }
//   return '';
// });

// var source = '<div class="entry">\n\
//   <h1>{{title}}</h1>\n\
//   <div class="body">{{call body \'toUpperCase\'}}</div>\n\
// </div>';
// var source = '<div class="entry">\n\
//   <h1>{{title}}</h1>\n\
//   <div class="body">{{call (call body \'slice\' 11 21) \'toUpperCase\'}}</div>\n\
// </div>';
var source = '\
<div class="entries">\n\
  {{#each entries}}\n\
  <div class="entry">\n\
    <h1>{{title}}</h1>\n\
    <div class="body">{{call (call body \'slice\' 11 21) \'toUpperCase\'}}</div>\n\
    <div>{{call this \'testFn\' (call body \'slice\' 11 17) }}</div>\n\
  </div>\n\
  {{/each}}\n\
</div>';
var template = HANDLEBARS.compile(source);

var testFn = function(par) { return 'testFn: ' + par; };

// var context = {title: "My New Post", body: "This is my first post!"};
var context = { entries: [
  { title: "My New Post", body: "This is my first post!", testFn: testFn },
  { title: "My Other New Post", body: "This is my second post!", testFn: testFn }
] };
var html = template(context);

console.log('----------');
console.log(html);

const workingDirPath = "../../..";
const modelDirPath = "doc/Model";
const dataDirPath = "doc/Data";
const modelFileName = "RM.xlsx";
const dataFileName = "Data.xlsx";
const sourceDirPath = "src/";
const modelSqlFileName = "Generated-Create-Tables.sql";
const dataSqlFileName = "Generated-Insert-Rows.sql";

module.exports = function main() {
  console.log('----------');
  console.log("model-tool: start");
  // process.argv.forEach(function (val, index, array) {
  //   console.log(index + ': ' + val);
  // });
  console.log("model-tool: end");
  console.log('----------');
}
