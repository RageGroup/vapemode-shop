const FS = require('fs');
const PATH = require('path');
const XLSX = require('xlsx');
const MemoryStream = require('memorystream');

const UTIL = require('./util');
const FileProcessor = require('./FileProcessor.js');
const ExcelTableRowIterator = require('./ExcelTableRowIterator.js');
const DbDataType = require('./data/DbDataType.js');
const DbModel = require('./data/DbModel.js');
const EntityDefinition = require('./data/EntityDefinition.js');
const EntityAttributeDefinition = require('./data/EntityAttributeDefinition.js');
const GENERATORS = require('./data/generators');

module.exports = class ModelProcessor extends FileProcessor {

  get workingDirPath() { return this._workingDirPath; }
  get sourceDirPath() { return this._sourceDirPath; }
  get createTablesFileName() { return this._createTablesFileName; }
  get model() { return this._model; }
  get codeLanguage() { return this._model.codeLanguage; }
  get dbSystemCode() { return this._model.dbSystemCode; }
  get modelName() { return this._model.modelName; }
  get modelNamespace() { return this._model.modelNamespace; }

  constructor(workingDirPath, sourceDirPath, modelDirPath, baseModelFilePath, modelFilePath, createTablesFileName) {
    super();
    this._baseModelFilePath = baseModelFilePath;
    this._modelFilePath = modelFilePath;
    this._workingDirPath = workingDirPath;
    this._sourceDirPath = sourceDirPath;
    this._createTablesFileName = createTablesFileName;
    this._model = null;
  }

  process() {
    this._sourceDirPath = PATH.join(this._workingDirPath, this._sourceDirPath);
    this._model = new DbModel();
    this.processExcelFile(this._baseModelFilePath);
    this.processExcelFile(this._modelFilePath);
    this.createSqlFiles(this._sourceDirPath, this._createTablesFileName);
    this.createCodeFiles(this._sourceDirPath);
  }

  /**
   * Process the excel file with the given path.
   */
  processExcelFile(excelFilePath) {
    let workbook = XLSX.readFile(excelFilePath);
    let modelSheet = workbook.Sheets['Model'];
    this._model.codeLanguage = 'PHP';
    this._model.dbSystemCode = ExcelTableRowIterator.getCell(modelSheet, 0, 1);
    this._model.modelName = ExcelTableRowIterator.getCell(modelSheet, 1, 1);
    this._model.modelNamespace = ExcelTableRowIterator.getCell(modelSheet, 2, 1);
    this.getDefinitions(workbook, modelSheet);
    // console.log(workbook);
    // console.log(modelSheet);
    // console.log(this);
  }

  /**
   * Get all table definitions available in the given excel workbook.
   */
  getDefinitions(workbook, modelSheet) {
    let excelTable = new ExcelTableRowIterator(modelSheet, 4);

    for (let excelRow of excelTable) {
      let c = 0;
      let tableName = excelRow.getValueString(c++);
      let localizedSingularDescriptions = new Map();
      let localizedPluralDescriptions = new Map();
      this.getTable(tableName, // tableName
        excelRow.getValueString(c++), // className
        excelRow.getValueString(c++), // classNamespace
        excelRow.getValueString(c++), // baseTableName
        excelRow.getValueString(c++), // baseClassName
        excelRow.getValueString(c++), // baseClassNamespace
        excelRow.getValueStringFlattened(c++), // singularDescription
        excelRow.getValueStringFlattened(c++), // pluralDescription
        localizedSingularDescriptions, // localizedSingularDescriptions
        localizedPluralDescriptions, // localizedPluralDescriptions
        workbook.Sheets[tableName]); // sheet
      for (let languageCode of excelTable.usedLanguageCodes) {
        // It is expected that two values of the same language
        // are listed in the right order!
        localizedSingularDescriptions.set(languageCode, excelRow.getValueStringFlattened(c++));
        localizedPluralDescriptions.set(languageCode, excelRow.getValueStringFlattened(c++));
      }
    }
  }

  /**
   * Get the information of an expected table.
   */
  getTable(tableName, className, classNamespace,
    baseTableName, baseClassName, baseClassNamespace,
    singularDescription, pluralDescription,
    localizedSingularDescriptions, localizedPluralDescriptions,
    sheet)
  {
    let baseTable = null;
    if (!UTIL.isNullOrWhiteSpace(baseTableName) &&
        !UTIL.isNullOrWhiteSpace(baseClassNamespace))
    {
      for (let table of this._model.getEntitiesByClassNamespace(baseClassNamespace)) {
        if (table.tableName == baseTableName) {
          baseTable = table;
          break;
        }
      }
    }
    let entity = new EntityDefinition(tableName,
      className, classNamespace,
      baseTable, baseTableName, baseClassName, baseClassNamespace,
      singularDescription, pluralDescription,
      localizedSingularDescriptions, localizedPluralDescriptions);
    this._model.addEntity(entity);

    let excelTable = new ExcelTableRowIterator(sheet);

    for (let excelRow of excelTable) {
      let c = 0;
      let columnName = excelRow.getValueString(c++);
      let columnDescription = excelRow.getValueStringFlattened(c++);
      let localizedDescriptions = new Map();
      let dataType = DbDataType[excelRow.getValueString(c++)];
      entity.addAttribute(new EntityAttributeDefinition(
        excelRow.rowIndex, // columnId
        columnName, columnDescription, dataType, // dataType
        excelRow.getValueInteger(c++), // length
        excelRow.getValueInteger(c++), // precision
        excelRow.getValueBoolean(c++), // private
        excelRow.getValueBoolean(c++), // optional
        excelRow.getValueBoolean(c++), // primaryKey
        excelRow.getValueString(c++), // defaultValue
        excelRow.getValueString(c++), // foreignKeyName
        excelRow.getValueString(c++), // foreignKeyTable
        excelRow.getValueString(c++), // foreignKeyColumn
        excelRow.getValueString(c++), // checks
        excelRow.getValueString(c++), // uniqueKey
        localizedDescriptions));
      c++; // Excel table column "Indexes" currently unused.
      for (let languageCode of excelTable.usedLanguageCodes) {
        localizedDescriptions.set(languageCode, excelRow.getValueStringFlattened(c++));
      }
    }
  }

  /**
   * Create the sql files representing the model.
   */
  createSqlFiles(sourceDirPath, createTablesFileName) {
    let createTablesSqlFilePath = PATH.join(__dirname, sourceDirPath, 'sql', createTablesFileName);
    // console.log(__dirname);
    // console.log(sourceDirPath);
    // console.log(createTablesFileName);
    if (!UTIL.isNullOrWhiteSpace(createTablesSqlFilePath)) {
      createTablesSqlFilePath = PATH.normalize(createTablesSqlFilePath);
      // console.log(createTablesSqlFilePath);
      // let writer = IoHelper.getNewTextFileWriter(createTablesSqlFilePath);
      let writer = FS.createWriteStream(createTablesSqlFilePath, FileProcessor.fileOptions());
      // let writer = new MemoryStream();
      let sqlGenerator = GENERATORS.forSqlCommands(this._model.dbSystemCode);
      // console.log(this);
      sqlGenerator.getCreateDatabaseCommands(writer, this._model.modelName, this._model.tables);
      writer.end();
    }
  }

  /**
   * Create the code files representing the model.
   */
  createCodeFiles(sourceDirPath) {
    let mappingGenerator = GENERATORS.forObjectMappings(this._model.codeLanguage);
    let mappingPath = PATH.join(__dirname, sourceDirPath, mappingGenerator.fileExtension, this._model.modelNamespace);
    let filepath = PATH.join(mappingPath, `${this._model.modelName}Context.${mappingGenerator.fileExtension}`);
    let writer = FS.createWriteStream(filepath, FileProcessor.fileOptions());
    mappingGenerator.writeContextClass(writer, this._model);
    writer.end();

    for (let table of this._model.classes) {
      let mappingPath = PATH.join(__dirname, sourceDirPath, mappingGenerator.fileExtension, table.classNamespace);
      filepath = PATH.join(mappingPath, `${table.className}Table.${mappingGenerator.fileExtension}`);
      writer = FS.createWriteStream(filepath, FileProcessor.fileOptions());
      mappingGenerator.writeTableClass(writer, this._model.modelName, table);
      writer.end();
      filepath = PATH.join(mappingPath, `${table.className}Row.${mappingGenerator.fileExtension}`);
      writer = FS.createWriteStream(filepath, FileProcessor.fileOptions());
      mappingGenerator.writeRowClass(writer, table);
      writer.end();
    }
  }

}
