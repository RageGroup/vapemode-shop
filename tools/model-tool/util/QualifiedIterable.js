const UTIL = require('../util');

module.exports = class QualifiedIterable {

  constructor(elements, predicate, successor) {
    this._elements = elements;
    this._predicate = UTIL.coalesce(predicate, (element) => true);
    this._successor = UTIL.coalesce(successor, (element) => null);
  }

  *[Symbol.iterator]() {
    for (let element of this._elements) {
      if (this._predicate(element)) {
        yield element;
        element = this._successor(element);
        if (!UTIL.isNull(element)) {
          yield element;
        }
      }
    }
  }

}
