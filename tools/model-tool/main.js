const PATH = require('path');

const ModelProcessor = require('./ModelProcessor.js');
const DataProcessor = require('./DataProcessor.js');

module.exports = function main(args) {
  console.log('Start...');

  let workingDirPath = '../..';
  let modelDirPath = 'doc/Model';
  let dataDirPath = 'doc/Data';
  let baseModelFileName = 'Base-Relational-Model.xlsx';
  let modelFileName = 'Relational-Model.xlsx';
  let baseDataFileName = 'Base-Data.xlsx';
  let dataFileName = 'Data.xlsx';
  let sourceDirPath = 'src/';
  let modelSqlFileName = 'Generated-Create-Tables.sql';
  let dataSqlFileName = 'Generated-Insert-Rows.sql';

  workingDirPath = PATH.normalize(workingDirPath);
  let baseModelFilePath = PATH.join(workingDirPath, modelDirPath, baseModelFileName);
  let modelFilePath = PATH.join(workingDirPath, modelDirPath, modelFileName);
  let baseDataFilePath = PATH.join(workingDirPath, dataDirPath, baseDataFileName);
  let dataFilePath = PATH.join(workingDirPath, dataDirPath, dataFileName);

  let modelProcessor = new ModelProcessor(
    workingDirPath, sourceDirPath, modelDirPath,
    baseModelFilePath, modelFilePath, modelSqlFileName);
  modelProcessor.process();

  let dataProcessor = new DataProcessor(
    modelProcessor, baseDataFilePath, dataFilePath, dataSqlFileName);
  dataProcessor.process();

  console.log('End.');
}
