const EntityDefinition = require('./data/EntityDefinition.js');
const EntityAttributeDefinition = require('./data/EntityAttributeDefinition.js');

/**
 * The definition of the table for all available languages.
 */
const languageEntityDefinition = new EntityDefinition(
  'Languages', 'Language', 'Core\\Models', '', '', '', 'Language', 'Sprache', 'Languages', 'Sprachen', [
    new EntityAttributeDefinition(1, 'LanguageCode', 'Language ID', 'Sprach-ID', 'VC', 2, 0, false, false, true),
    new EntityAttributeDefinition(2, 'LanguageName', 'Language Name', 'Sprachname', 'VC', 40)
  ]);

/**
 * The definition of the table for all tables.
 */
const tableEntityDefinition = new EntityDefinition(
  'Tables', 'Table', 'Core\\Models', '', '', '', 'Table', 'Tabelle', 'Tables', 'Tabellen', [
    new EntityAttributeDefinition(1, 'TableName', 'Table Name', 'Tabellemname', 'VC', 40, 0, false, false, true),
    new EntityAttributeDefinition(2, 'TableDescription', 'Table Description', 'Tabellenbeschreibung', 'VC', 40)
  ]);

/**
 * The definition of the table for all table columns.
 */
const tableColumnEntityDefinition = new EntityDefinition(
  'TableColumns', 'TableColumn', 'Core\\Models', '', '', '', 'TableColumn', 'Tabellenspalte', 'TableColumns', 'Tabellenspalten', [
    new EntityAttributeDefinition(1, 'TableName', 'Table Name', 'Tabellemname', 'VC', 40, 0, false, false, true),
    new EntityAttributeDefinition(2, 'ColumnName', 'Column Name', 'Spaltenname', 'VC', 40, 0, false, false, true),
    new EntityAttributeDefinition(3, 'ColumnDescription', 'Column Description', 'Spaltenbeschreibung', 'VC', 40)
  ]);

/**
 * The definition of the table for all object descriptions.
 */
const tableColumnEntityDefinition = new EntityDefinition(
  'ObjectDescriptions', 'ObjectDescription', 'Core\\Models', '', '', '', 'ObjectDescription', 'Tabellenspalte', 'ObjectDescriptions', 'Tabellenspalten', [
    new EntityAttributeDefinition(1, 'TableName', 'Table Name', 'Tabellemname', 'VC', 40, 0, false, false, true),
    new EntityAttributeDefinition(2, 'ColumnName', 'Column Name', 'Spaltenname', 'VC', 40, 0, false, false, true),
    new EntityAttributeDefinition(3, 'ColumnDescription', 'Column Description', 'Spaltenbeschreibung', 'VC', 40)
  ]);

/**
 * The definition of the table for users.
 */
const userEntityDefinition = new EntityDefinition(
  'Users', 'User', 'Core\\Models', '', '', '', 'User', 'Benutzer', 'Users', 'Benutzer', [
    new EntityAttributeDefinition(1, 'UserId', 'User ID', 'Benutzer-ID', 'UUID', 0, 0, false, false, true),
    new EntityAttributeDefinition(2, 'Email', 'E-Mail Address', 'E-Mail-Adresse', 'VC', 320, 0, false, false, false, null, null, null, 'Email'),
    new EntityAttributeDefinition(3, 'FirstName', 'First Name', 'Vorname', 'VC', 40),
    new EntityAttributeDefinition(4, 'LastName', 'Last Name', 'Nachname', 'VC', 40),
    new EntityAttributeDefinition(5, 'UserRole', `User Role (
    'A' => Admin,
    'C' => Customer,
    'G' => Guest)`, `Benutzerrolle (
    'A' => Admin,
    'C' => Kunde,
    'G' => Gast)`, 'C', 1, 0, false, false, false, null, null, null, `UserRole in ('A', 'C', 'G')`),
    new EntityAttributeDefinition(6, 'PasswordHash', 'Password Hash', 'Passwort-Hash', 'VC', 255, 0, true),
    new EntityAttributeDefinition(7, 'ExpirationDate', 'Expiration Date', 'Ablaufdatum', 'DT', 0, 0, false, true),
    new EntityAttributeDefinition(8, 'LastLoginDate', 'Last Login Date', 'Letzes Login-Datum', 'DT', 0, 0, false, true),
    new EntityAttributeDefinition(9, 'UserAgent', 'User Agent', 'Browser-Kennung', 'VC', 200, 0, false, true),
    new EntityAttributeDefinition(10, 'RemoteIpAddress', 'Remote IP Address', 'Remote-IP-Adresse', 'VC', 45, 0, false, true),
    new EntityAttributeDefinition(11, 'ForwardedIpAddress', 'Forwarded IP Address', 'Weiterleitungs-IP-Adresse', 'VC', 45, 0, false, true)
  ]);

/**
 * The definition of the table for roles.
 */
const rolesEntityDefinition = new EntityDefinition(
  'Roles', 'Role', 'Core\\Models', '', '', '', 'Role', 'Rolle', 'Roles', 'Rollen', [
    new EntityAttributeDefinition(1, 'RoleId', 'Role ID', 'Rollen-ID', 'UUID', 0, 0, false, false, true),
    new EntityAttributeDefinition(2, 'RoleName', 'Role Name', 'Rollenname', 'VC', 40)
  ]);

/**
 * The definition of the table for user roles.
 */
const userRolesEntityDefinition = new EntityDefinition(
  'UserRoles', 'UserRole', 'Core\\Models', '', '', '', 'UserRole', 'Benutzerrolle', 'UserRoles', 'Benutzerrollen', [
    new EntityAttributeDefinition(1, 'UserRoleId', 'User Role ID', 'Benutzerrollen-ID', 'UUID', 0, 0, false, false, true),
    new EntityAttributeDefinition(2, 'UserId', 'User ID', 'Benutzer-ID', 'UUID', 0, 0, false, false, false, null, 'Users', 'UserId'),
    new EntityAttributeDefinition(3, 'RoleId', 'Role ID', 'Rollen-ID', 'UUID', 0, 0, false, false, false, null, 'Roles', 'RoleId')
  ]);

module.exports = {
  languages = languageEntityDefinition,
  table = tableEntityDefinition,
  tableColumn = tableColumnEntityDefinition,
  users = userEntityDefinition,
  roles = rolesEntityDefinition,
  userRoles = userRolesEntityDefinition
}
