@echo off

setlocal EnableDelayedExpansion

set "SCRIPT_PATH=%~dp0"
set "SCRIPT_PATH=%SCRIPT_PATH:~0,-1%"

cd /d "%SCRIPT_PATH%"

cd model-tool-js

yarn start

endlocal
