function removeClass(element, className) {
  element.classList.remove(className);
}

function toggleClass(element, className) {
  element.classList.toggle(className);
}

function prepareForJs(element) {
  if (element != null) {
    element.classList.remove('vm-nojs');
    element.classList.add('vm-js');
  }
}

function onLoad() {
  prepareForJs(document.getElementById('vm-header'));
  prepareForJs(document.getElementById('vm-menu'));
}

function menuClick() {
  var header = document.getElementById('vm-header');
  var menu = document.getElementById('vm-menu');
  toggleClass(header, 'vm-clicked');
  toggleClass(menu, 'vm-clicked');
}

function submenuClick(linkElement) {
  var currentSubmenu = linkElement.parentElement;
  toggleClass(currentSubmenu, 'vm-clicked')
  var menu = document.getElementById('vm-menu');
  var menuList = menu.children.item(1);
  for (var i = 0; i < menuList.children.length; i++) {
    if (menuList.children[i] != currentSubmenu) {
      removeClass(menuList.children[i], 'vm-clicked')
    }
  }
}
