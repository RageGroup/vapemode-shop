use `VapeMode`;

/* Table for Languages */

insert into `Languages`(`LanguageCode`, `LanguageName`)
values ('en', 'English');

insert into `Languages`(`LanguageCode`, `LanguageName`)
values ('de', 'Deutsch');

/* Table for Tables */

insert into `Tables`(`TableName`)
values ('Languages');

insert into `Tables`(`TableName`)
values ('Tables');

insert into `Tables`(`TableName`)
values ('TableTexts');

insert into `Tables`(`TableName`)
values ('Columns');

insert into `Tables`(`TableName`)
values ('ColumnTexts');

insert into `Tables`(`TableName`)
values ('Users');

insert into `Tables`(`TableName`)
values ('Roles');

insert into `Tables`(`TableName`)
values ('RoleTexts');

insert into `Tables`(`TableName`)
values ('UserRoles');

insert into `Tables`(`TableName`)
values ('Addresses');

insert into `Tables`(`TableName`)
values ('Customers');

insert into `Tables`(`TableName`)
values ('Products');

insert into `Tables`(`TableName`)
values ('ProductTexts');

insert into `Tables`(`TableName`)
values ('Orders');

insert into `Tables`(`TableName`)
values ('OrderItems');

insert into `Tables`(`TableName`)
values ('BasketItems');

/* Table for Table Texts */

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Languages', 'en', 'Languages', 'Language');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Languages', 'de', 'Sprachen', 'Sprache');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Tables', 'en', 'Tables', 'Table');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Tables', 'de', 'Tabellen', 'Tabelle');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Columns', 'en', 'Table Columns', 'Table Column');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Columns', 'de', 'Tabellespalten', 'Tabellespalte');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Users', 'en', 'Users', 'User');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Users', 'de', 'Benutzer', 'Benutzer');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Roles', 'en', 'Roles', 'Role');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Roles', 'de', 'Rollen', 'Rolle');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('UserRoles', 'en', 'User Roles', 'User Role');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('UserRoles', 'de', 'Benutzerrollen', 'Benutzerrolle');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Addresses', 'en', 'Addresses', 'Address');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Addresses', 'de', 'Adressen', 'Adresse');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Customers', 'en', 'Customers', 'Customer');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Customers', 'de', 'Kunden', 'Kunde');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Products', 'en', 'Products', 'Product');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Products', 'de', 'Produkte', 'Produkt');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Orders', 'en', 'Orders', 'Order');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('Orders', 'de', 'Bestellungen', 'Bestellung');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('OrderItems', 'en', 'Order Items', 'Order Item');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('OrderItems', 'de', 'Bestellpositionen', 'Bestellposition');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('BasketItems', 'en', 'Basket Items', 'Basket Item');

insert into `TableTexts`(`TableName`, `LanguageCode`, `TableDescription`, `ClassDescription`)
values ('BasketItems', 'de', 'Warenkorbpositionen', 'Warenkorbposition');

/* Table for Table Columns */

insert into `Columns`(`TableName`, `ColumnName`)
values ('Languages', 'LanguageCode');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Languages', 'LanguageName');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Tables', 'TableName');

insert into `Columns`(`TableName`, `ColumnName`)
values ('TableTexts', 'TableName');

insert into `Columns`(`TableName`, `ColumnName`)
values ('TableTexts', 'LanguageCode');

insert into `Columns`(`TableName`, `ColumnName`)
values ('TableTexts', 'TableDescription');

insert into `Columns`(`TableName`, `ColumnName`)
values ('TableTexts', 'ClassDescription');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Columns', 'TableName');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Columns', 'ColumnName');

insert into `Columns`(`TableName`, `ColumnName`)
values ('ColumnTexts', 'TableName');

insert into `Columns`(`TableName`, `ColumnName`)
values ('ColumnTexts', 'ColumnName');

insert into `Columns`(`TableName`, `ColumnName`)
values ('ColumnTexts', 'LanguageCode');

insert into `Columns`(`TableName`, `ColumnName`)
values ('ColumnTexts', 'ColumnDescription');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Users', 'UserId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Users', 'UserName');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Users', 'PasswordHash');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Users', 'ExpirationDate');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Users', 'LastLoginDate');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Users', 'LastActivityDate');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Users', 'Email');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Users', 'FirstName');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Users', 'LastName');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Users', 'UserAgent');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Users', 'RemoteIpAddress');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Users', 'ForwardedIpAddress');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Roles', 'RoleId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Roles', 'RoleName');

insert into `Columns`(`TableName`, `ColumnName`)
values ('RoleTexts', 'RoleId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('RoleTexts', 'LanguageCode');

insert into `Columns`(`TableName`, `ColumnName`)
values ('RoleTexts', 'RoleDescription');

insert into `Columns`(`TableName`, `ColumnName`)
values ('UserRoles', 'UserId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('UserRoles', 'RoleId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Addresses', 'AddressId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Addresses', 'Postcode');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Addresses', 'City');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Addresses', 'StreetHouseNo');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Customers', 'CustomerId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Customers', 'IdCardNo');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Customers', 'BillingAddressId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Customers', 'DefaultShippingAddressId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Products', 'ProductId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Products', 'ProductType');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Products', 'Stock');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Products', 'UnitPrice');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Products', 'Nicotine');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Products', 'Content');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Products', 'MixRecommendationMin');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Products', 'MixRecommendationMax');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Products', 'Manufacturer');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Products', 'ImageFileName');

insert into `Columns`(`TableName`, `ColumnName`)
values ('ProductTexts', 'ProductId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('ProductTexts', 'LanguageCode');

insert into `Columns`(`TableName`, `ColumnName`)
values ('ProductTexts', 'ProductName');

insert into `Columns`(`TableName`, `ColumnName`)
values ('ProductTexts', 'Flavour');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Orders', 'OrderId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Orders', 'OrderDate');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Orders', 'OrderState');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Orders', 'CustomerId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Orders', 'ShippingAddressId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('Orders', 'TotalPrice');

insert into `Columns`(`TableName`, `ColumnName`)
values ('OrderItems', 'OrderLineId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('OrderItems', 'OrderId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('OrderItems', 'CreationDate');

insert into `Columns`(`TableName`, `ColumnName`)
values ('OrderItems', 'ProductId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('OrderItems', 'Quantity');

insert into `Columns`(`TableName`, `ColumnName`)
values ('OrderItems', 'UnitPrice');

insert into `Columns`(`TableName`, `ColumnName`)
values ('BasketItems', 'BasketItemId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('BasketItems', 'CustomerId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('BasketItems', 'CreationDate');

insert into `Columns`(`TableName`, `ColumnName`)
values ('BasketItems', 'ProductId');

insert into `Columns`(`TableName`, `ColumnName`)
values ('BasketItems', 'Quantity');

/* Table for Table Column Texts */

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Languages', 'LanguageCode', 'en', 'Language Code');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Languages', 'LanguageCode', 'de', 'Sprachkode');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Languages', 'LanguageName', 'en', 'Language Name');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Languages', 'LanguageName', 'de', 'Name der Sprache');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Tables', 'TableName', 'en', 'Table Name');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Tables', 'TableName', 'de', 'Tabellenname');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('TableTexts', 'TableDescription', 'en', 'Table Description');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('TableTexts', 'TableDescription', 'de', 'Tabellenbezeichnung');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('TableTexts', 'ClassDescription', 'en', 'Class Description');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('TableTexts', 'ClassDescription', 'de', 'Klassenbezeichnung');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Columns', 'TableName', 'en', 'Table Name');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Columns', 'TableName', 'de', 'Tabellenname');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Columns', 'ColumnName', 'en', 'Column Name');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Columns', 'ColumnName', 'de', 'Spaltenname');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('ColumnTexts', 'ColumnDescription', 'en', 'Column Description');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('ColumnTexts', 'ColumnDescription', 'de', 'Spaltenbeschreibung');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'UserId', 'en', 'User ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'UserId', 'de', 'Benutzer-ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'UserName', 'en', 'User Name');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'UserName', 'de', 'Benutzername');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'PasswordHash', 'en', 'Password Hash');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'PasswordHash', 'de', 'Passwort-Hash');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'ExpirationDate', 'en', 'Expiration Date');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'ExpirationDate', 'de', 'Ablaufdatum');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'LastLoginDate', 'en', 'Last Login Date');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'LastLoginDate', 'de', 'Letzes Login-Datum');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'LastActivityDate', 'en', 'Last Login Date');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'LastActivityDate', 'de', 'Letzes Login-Datum');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'Email', 'en', 'E-Mail Address');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'Email', 'de', 'E-Mail-Adresse');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'FirstName', 'en', 'First Name');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'FirstName', 'de', 'Vorname');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'LastName', 'en', 'Last Name');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'LastName', 'de', 'Nachname');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'UserAgent', 'en', 'User Agent');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'UserAgent', 'de', 'Browser-Kennung');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'RemoteIpAddress', 'en', 'Remote IP Address');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'RemoteIpAddress', 'de', 'Remote-IP-Adresse');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'ForwardedIpAddress', 'en', 'Forwarded IP Address');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Users', 'ForwardedIpAddress', 'de', 'Weiterleitungs-IP-Adresse');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Roles', 'RoleId', 'en', 'Role ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Roles', 'RoleId', 'de', 'Rollen-ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Roles', 'RoleName', 'en', 'Role Name');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Roles', 'RoleName', 'de', 'Rollenname');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('RoleTexts', 'RoleDescription', 'en', 'Role Description');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('RoleTexts', 'RoleDescription', 'de', 'Rollenbeschreibung');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('UserRoles', 'UserId', 'en', 'User ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('UserRoles', 'UserId', 'de', 'Benutzer-ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('UserRoles', 'RoleId', 'en', 'Role ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('UserRoles', 'RoleId', 'de', 'Rollen-ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Addresses', 'AddressId', 'en', 'Address ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Addresses', 'AddressId', 'de', 'Adressnummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Addresses', 'Postcode', 'en', 'Postcode');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Addresses', 'Postcode', 'de', 'Postleitzahl');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Addresses', 'City', 'en', 'City');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Addresses', 'City', 'de', 'Ort');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Addresses', 'StreetHouseNo', 'en', 'Street and House No.');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Addresses', 'StreetHouseNo', 'de', 'Straße und Hausnummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Customers', 'CustomerId', 'en', 'Customer ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Customers', 'CustomerId', 'de', 'Kundennummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Customers', 'IdCardNo', 'en', 'ID Card No.');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Customers', 'IdCardNo', 'de', 'Ausweisnummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Customers', 'BillingAddressId', 'en', 'Billing Address ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Customers', 'BillingAddressId', 'de', 'Rechnungsadressnummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Customers', 'DefaultShippingAddressId', 'en', 'Default Shipping Address ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Customers', 'DefaultShippingAddressId', 'de', 'Standard-Lieferadressnummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'ProductId', 'en', 'Product ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'ProductId', 'de', 'Produktnummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'ProductType', 'en', 'Produkt Type');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'ProductType', 'de', 'Produkttyp');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'Stock', 'en', 'Stock');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'Stock', 'de', 'Bestand');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'UnitPrice', 'en', 'Unit Price');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'UnitPrice', 'de', 'Stückpreis');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'Nicotine', 'en', 'Nicotine');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'Nicotine', 'de', 'Nikotin');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'Content', 'en', 'Content');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'Content', 'de', 'Inhalt');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'MixRecommendationMin', 'en', 'Mix Recommendation, Minumum');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'MixRecommendationMin', 'de', 'Mischungsempfehlung, Minumum');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'MixRecommendationMax', 'en', 'Mix Recommendation, Maximum');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'MixRecommendationMax', 'de', 'Mischungsempfehlung, Maximum');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'Manufacturer', 'en', 'Manufacturer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'Manufacturer', 'de', 'Hersteller');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'ImageFileName', 'en', 'Image File Name');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Products', 'ImageFileName', 'de', 'Bilddateiname');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('ProductTexts', 'ProductName', 'en', 'Product Name');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('ProductTexts', 'ProductName', 'de', 'Produktname');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('ProductTexts', 'Flavour', 'en', 'Flavour');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('ProductTexts', 'Flavour', 'de', 'Geschmack');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Orders', 'OrderId', 'en', 'Order ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Orders', 'OrderId', 'de', 'Bestellnummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Orders', 'OrderDate', 'en', 'Order Date');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Orders', 'OrderDate', 'de', 'Bestelldatum');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Orders', 'OrderState', 'en', 'Order State');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Orders', 'OrderState', 'de', 'Bestellstatus');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Orders', 'CustomerId', 'en', 'Customer ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Orders', 'CustomerId', 'de', 'Kundennummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Orders', 'ShippingAddressId', 'en', 'Shipping Address ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Orders', 'ShippingAddressId', 'de', 'Lieferadressnummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Orders', 'TotalPrice', 'en', 'Total Price');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('Orders', 'TotalPrice', 'de', 'Gesamtpreis');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('OrderItems', 'OrderLineId', 'en', 'Order Line ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('OrderItems', 'OrderLineId', 'de', 'Bestellpositionsnummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('OrderItems', 'OrderId', 'en', 'Order ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('OrderItems', 'OrderId', 'de', 'Bestellnummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('OrderItems', 'CreationDate', 'en', 'Creation Date');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('OrderItems', 'CreationDate', 'de', 'Erstelldatum');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('OrderItems', 'ProductId', 'en', 'Pruduct ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('OrderItems', 'ProductId', 'de', 'Produktnummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('OrderItems', 'Quantity', 'en', 'Quantity');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('OrderItems', 'Quantity', 'de', 'Bestellmenge');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('OrderItems', 'UnitPrice', 'en', 'Unit Price');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('OrderItems', 'UnitPrice', 'de', 'Stückpreis');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('BasketItems', 'BasketItemId', 'en', 'Basket Item ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('BasketItems', 'BasketItemId', 'de', 'Warenkorbpositionsnummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('BasketItems', 'CustomerId', 'en', 'Customer ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('BasketItems', 'CustomerId', 'de', 'Kundennummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('BasketItems', 'CreationDate', 'en', 'Creation Date');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('BasketItems', 'CreationDate', 'de', 'Erstelldatum');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('BasketItems', 'ProductId', 'en', 'Product ID');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('BasketItems', 'ProductId', 'de', 'Produktnummer');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('BasketItems', 'Quantity', 'en', 'Quantity');

insert into `ColumnTexts`(`TableName`, `ColumnName`, `LanguageCode`, `ColumnDescription`)
values ('BasketItems', 'Quantity', 'de', 'Bestellmenge');

/* Table for Users */

insert into `Users`(`UserId`, `UserName`, `Email`, `FirstName`, `LastName`, `PasswordHash`)
values (unhex('334ffb4c754747a08e6b0d996d38a588'), 'wude', 'stefan.woyde@fh-erfurt.de', 'Stefan', 'Woyde', '$2y$11$ThD0rTVE63L43UEOdTacDunzGs5/stjKN4rJsh21pmPp8SCX2kIw2');

insert into `Users`(`UserId`, `UserName`, `Email`, `FirstName`, `LastName`, `PasswordHash`)
values (unhex('e432f357094e4e9387f94786883c7d07'), 'finalonragemode', 'christoph.dueben@fh-erfurt.de', 'Christoph', 'Düben', '$2y$11$J.Q281waeAHXpc9iwJFQSuZhvTWzA4Vidgi..ohsD7KEaZXW.fHb6');

insert into `Users`(`UserId`, `UserName`, `Email`, `FirstName`, `LastName`, `PasswordHash`)
values (unhex('a1448bad5ab14dfabe8870dfcb31eabc'), 'rolf.kruse', 'rolf.kruse@fh-erfurt.de', 'Rolf', 'Kruse', '$2y$11$z7D6YVJpv/r7CImqQCefN.KhMEXxo.3oML3kN5CgibPMYO8qd7P2y');

insert into `Users`(`UserId`, `UserName`, `Email`, `FirstName`, `LastName`, `PasswordHash`)
values (unhex('d57930b8c4734376a6566da650901850'), 'kristof.friess', 'kristof.friess@fh-erfurt.de', 'Kristof', 'Friess', '$2y$11$z7D6YVJpv/r7CImqQCefN.KhMEXxo.3oML3kN5CgibPMYO8qd7P2y');

/* Table for Addresses */

insert into `Addresses`(`AddressId`, `Postcode`, `City`, `StreetHouseNo`)
values (unhex('bcb64ca07c414040b365e77a7af3b385'), '99085', 'Erfurt', 'Altonaer Straße 25');

/* Table for Customers */

insert into `Customers`(`CustomerId`, `IdCardNo`, `BillingAddressId`, `DefaultShippingAddressId`)
values (unhex('a1448bad5ab14dfabe8870dfcb31eabc'), '1220001297d640812517103198', unhex('bcb64ca07c414040b365e77a7af3b385'), unhex('bcb64ca07c414040b365e77a7af3b385'));

insert into `Customers`(`CustomerId`, `IdCardNo`, `BillingAddressId`, `DefaultShippingAddressId`)
values (unhex('d57930b8c4734376a6566da650901850'), 't22000129364081252010315d4', unhex('bcb64ca07c414040b365e77a7af3b385'), unhex('bcb64ca07c414040b365e77a7af3b385'));

/* Table for Products */

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('f3c62e09cfb34e0fa602e8d757f8cf51'), 'L', 50, 0, 25, '12Monkeys', 'matata-twelve-monkeys-50-ml.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('5578c11eca924b5cab5dae21aa807e21'), 'L', 50, 0, 25, '12Monkeys', 'twelve_monkeys_e_liquid_harambae_50_ml_image.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('2160e31214134c1397cb61e49363c6ea'), 'L', 50, 0, 25, '12Monkeys', 'twelve_monkeys_e_liquid_hakuna_50_ml_image.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('1fb96a51bfbd4820bf81a00d92c08b38'), 'L', 50, 0, 25, '12Monkeys', '12m_origins_lemur-monkey-mix_einzelnd.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('479a0dbab81948f7b343c9394c3db4f9'), 'L', 50, 0, 25, '12Monkeys', '12m_origins_patas-pipe-monkey-mix_einzelnd.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('1dabfc6fbe904d4fa3cd7247a8f7c636'), 'L', 50, 0, 25, 'Vampire Vape', 'vampire_vape_liquid_pinkman_00.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('e5a7c436cda64c95a876daebd0ade4d3'), 'L', 50, 0, 25, 'Vampire Vape', 'vampire_vape_liquid_heisenberg_00.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('3878e1546b554ede800645b80223e5a5'), 'L', 50, 0, 25, 'Vampire Vape', 'vampire_vape_liquid_blood_sukka_00.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('d355ee038f4645a38cb3033e9fb6a994'), 'L', 50, 0, 25, 'Vampire Vape', 'use_concentrate_mock-ups_clear_bottle_blood_sukka.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('8b2cdb75b1c14327b547260ebc7d03ed'), 'L', 100, 0, 30, 'The Six Licks', 'six_licks_e_liquid_100_ml_truth_or_pear.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('40ce16e928624ced8e0d4706c6747396'), 'L', 100, 0, 30, 'The Six Licks', 'six_licks_e_liquid_100_ml_love_bite.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('2a396d773dda4c8fbb3f1054a19eb492'), 'L', 100, 0, 30, 'The Six Licks', 'six_licks_e_liquid_100_ml_bite_the_bullet.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('7691c940885647d29976d31e02c85f15'), 'L', 100, 0, 30, 'The Six Licks', 'sixs-licks-liquids-bluemonia-0mg-100-ml-283716-65058710diL9rmvrdYx9x_600x600.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('7df77e10c60c4b31af19f9f572e86a16'), 'L', 10, 20, 5.99, 'Damfa X', 'tricky-tuff_3474_0.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('fc0699bd1d084129b2c6a026aab3a34a'), 'L', 10, 20, 5.99, 'Damfa X', '10022_smokerstore_liquid_legendary.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('ba56aaa514dc4646abfdfb2fcb9230be'), 'L', 10, 20, 5.99, 'Damfa X', '10009_smokerstore_Liquid_FunkyVamp_600x600@2x.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('450994c2ebf24b7db12bae9c13e1494f'), 'L', 50, 0, 25, 'Acid E-Juice', 'acid-e-juice-apple-sour-candy-50ml.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('1c22f883a3e449f3af45ee7fb3525a30'), 'L', 50, 0, 25, 'Acid E-Juice', 'acid-e-juice-pineapple-sour-candy-50ml.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('e2ad68af13464337a168c390d1920189'), 'L', 50, 0, 25, 'Acid E-Juice', 'watermelon-sour-candy-50ml-e-liquid-acid-juice.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('eca789c334c14471ad4266bcf64dcf1c'), 'A', 10, 0, 7.9, 'K-BOOM', 'k-boom_aromen_cherry_bomb.jpg', 7, 10, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('4ef4f9f4913e4a70ae47a7b2e063a2ad'), 'A', 10, 0, 7.9, 'K-BOOM', 'k-boom_aromen_blue_cake_bomb.jpg', 8, 10, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('32674659c05f4ccfb52d78c203f71c57'), 'A', 10, 0, 7.9, 'K-BOOM', 'k-boom_aromen_sweet_bomb.jpg', 6, 10, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('04218042cfe34562b939478416b9f358'), 'A', 10, 0, 7.9, 'K-BOOM', 'k-boom_aromen_strawberry_bomb.jpg', 4, 10, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('cc064703ab234d34ab1b4a9189506875'), 'A', 10, 0, 7.9, 'K-BOOM', 'k-boom_aromen_green_bomb.jpg', 8, 10, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('376949bb6d4e479aa726ae6670471d83'), 'A', 13, 0, 7.9, 'Rocket Girl Aroma', 'rocket_girl_liquid_apple_eqiunox.jpg', 6, 10, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('a3003262e69447b8864459491dcb4b1e'), 'A', 13, 0, 7.9, 'Rocket Girl Aroma', 'rocket_girl_liquid_peach_parallax.jpg', 7, 10, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('8ce8e910aff7441685f3c189da870bb2'), 'A', 13, 0, 7.9, 'Rocket Girl Aroma', 'rocket_girl_liquid_astral_orange_ice.jpg', 6, 10, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('630528be0586465dabc2467466c147b8'), 'A', 15, 0, 9.9, 'Bang Juice', 'bang_juice_aroma_concentrate_evil_creeple.jpg', 5, 10, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('baf010bb975b4f7081f36f8c85ee3e1e'), 'A', 15, 0, 9.9, 'Bang Juice', '65427_bang_juice_aroma_concentrate_tropenhazard.jpg', 2, 10, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('82e6455a1c2d4f819ada36940074a28a'), 'A', 15, 0, 9.9, 'Bang Juice', '65426_bang_juice_aroma_concentrate_lieblingswaffel.jpg', 8, 10, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('af281b23008c4703ac4916018fc4ffc1'), 'A', 15, 0, 9.9, 'Bang Juice', 'bang_juice_aroma_concentrate_bangover.jpg', 5, 10, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('1e86364981c641b4b39fdf3be9b04d01'), 'A', 15, 0, 9.9, 'Bang Juice', 'bang_juice_aroma_concentrate_oktobeer.jpg', 9, 10, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('08c8c0f199b947aa8edbe773310147de'), 'A', 15, 0, 9.9, 'Bang Juice', 'bang_juice_aroma_concentrate_grapagne.jpg', 2, 12, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('8989af0e4e744412a7418ceb04f7e3d1'), 'A', 15, 0, 9.9, 'Bang Juice', 'bang_juice_aroma_concentrate_radioactea_kool.jpg', 5, 12, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('91f605e966464e20a9b46dc141f36fa4'), 'A', 15, 0, 9.9, 'Bang Juice', 'bang_juice_aroma_concentrate_kola_cannonball.jpg', 6, 12, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('932e5605bb134b41a9aae095aa6386de'), 'A', 30, 0, 14.95, 'Vampire Vape', 'heisenberg_aroma_vampire_vape.jpg', 4, 8, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('de78560e7e4b45e8b5809ab5abafd2c7'), 'A', 30, 0, 14.95, 'Vampire Vape', 'pinkman_aroma_vampire_vape.jpg', 4, 8, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('64f3da7d33724416878c46a698bd7bd0'), 'A', 30, 0, 14.95, 'Vampire Vape', 'coffee_cake_aroma_vampire_vape.jpg', 7, 8, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('7746ec534c1b4e22919e1f08ae10e2dd'), 'A', 30, 0, 14.95, 'Vampire Vape', 'blood_sukka_aroma_vampire_vape.jpg', 3, 8, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('d37e0b19f5474713a8f39cd90d4e0f9f'), 'B', 100, 0, 2.95, 'Riccardo', 'base_liquid_100ml.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('e85ca58bdab84db89eeb390088e30484'), 'B', 500, 0, 5.95, 'Riccardo', 'base_liquid_100ml.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('f73b343462894d36993abd3d0170aa77'), 'B', 1000, 0, 9.95, 'Riccardo', 'base_liquid_100ml.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('d850d6f24c044926ac07344fb74b3db8'), 'B', 100, 0, 2.95, 'Riccardo', 'base_liquid_100ml.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('eceff942038d4f5295d323122987533e'), 'B', 500, 0, 5.95, 'Riccardo', 'base_liquid_100ml.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('0034f2e6665d446cbd0f8cb7a4068978'), 'B', 1000, 0, 9.95, 'Riccardo', 'base_liquid_100ml.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('29050a7af9784f8184f5978dc7d11bcd'), 'B', 100, 0, 2.95, 'Riccardo', 'base_liquid_100ml.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('162d24736c4e4e418873071d0a94ea49'), 'B', 500, 0, 5.95, 'Riccardo', 'base_liquid_100ml.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('c40a110d440b4822bc5e43db69ecc4f9'), 'B', 1000, 0, 9.95, 'Riccardo', 'base_liquid_100ml.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('c43e3d225e0f4e388845aedd426d8d72'), 'B', 10, 20, 1, 'Riccardo', 'nicotinshot_20mg.jpg', 0, 0, 200);

insert into `Products`(`ProductId`, `ProductType`, `Content`, `Nicotine`, `UnitPrice`, `Manufacturer`, `ImageFileName`, `MixRecommendationMin`, `MixRecommendationMax`, `Stock`)
values (unhex('a92d93da8e1d4d85b695536035566e21'), 'B', 50, 20, 5, 'Riccardo', 'nicotinshot_20mg.jpg', 0, 0, 200);

/* Table for Product Texts */

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('f3c62e09cfb34e0fa602e8d757f8cf51'), 'Matata', 'Apple, Grape');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('5578c11eca924b5cab5dae21aa807e21'), 'Harambae', 'Grapefruit, Lemon, Lime, Blood Orange, Guava');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('2160e31214134c1397cb61e49363c6ea'), 'Hakuna', 'Apple, Cranberry');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('1fb96a51bfbd4820bf81a00d92c08b38'), 'Lemur', 'Lemon, Lime');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('479a0dbab81948f7b343c9394c3db4f9'), 'Patas Pipe', 'Tabacco, Dark Chocolate, Vanilla');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('1dabfc6fbe904d4fa3cd7247a8f7c636'), 'Pinkman', 'Berry Menthol');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('e5a7c436cda64c95a876daebd0ade4d3'), 'Heisenberg', 'Multi fruit Menthol');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('3878e1546b554ede800645b80223e5a5'), 'Blood Sukka', 'Cherry, Wild Berries, Eucalyptus, Anise');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('d355ee038f4645a38cb3033e9fb6a994'), 'Pie Eyed', 'Puff Pastry, Lemon');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('8b2cdb75b1c14327b547260ebc7d03ed'), 'Truth Or Pear', 'Erdbeere Limette Birne');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('40ce16e928624ced8e0d4706c6747396'), 'Love Bite', 'Blood Orange, Grapefruit');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('2a396d773dda4c8fbb3f1054a19eb492'), 'Bite the Bullet', 'Black Currant Anise');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('7691c940885647d29976d31e02c85f15'), 'Bluemonia', 'Blueberry, Raspberry, Menthol');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('7df77e10c60c4b31af19f9f572e86a16'), 'Tricky Tuff', 'Prickly Pear, Lychee');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('fc0699bd1d084129b2c6a026aab3a34a'), 'Legendary v2', 'Blueberry, Melon');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('ba56aaa514dc4646abfdfb2fcb9230be'), 'Funky Vamp', 'Fruit Mix');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('450994c2ebf24b7db12bae9c13e1494f'), 'Apple Sour Candy', 'Sour Appel Sweets');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('1c22f883a3e449f3af45ee7fb3525a30'), 'Pineapple Sour Candy', 'Pineapple Sweets');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('e2ad68af13464337a168c390d1920189'), 'Watermelon Sour Candy', 'Melon Sweets');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('eca789c334c14471ad4266bcf64dcf1c'), 'Cola Cherry Bomb', 'Cola, Cherry, Raspberry');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('4ef4f9f4913e4a70ae47a7b2e063a2ad'), 'Blue Cake Bomb', 'Blaubeer Käsekuchen');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('32674659c05f4ccfb52d78c203f71c57'), 'Sweet Bomb', 'Vanilla, Banana, Blackberry, Cinnamon');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('04218042cfe34562b939478416b9f358'), 'Strawberry Bomb', 'Strawberry');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('cc064703ab234d34ab1b4a9189506875'), 'Green Bomb', 'Cactus, Kiwi Koolada');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('376949bb6d4e479aa726ae6670471d83'), 'Apple Equinox', 'Green Appel, Strawberry, Butterscotch');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('a3003262e69447b8864459491dcb4b1e'), 'Peach Parallax', 'Peach, Steinfrüchte Frische');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('8ce8e910aff7441685f3c189da870bb2'), 'Astral Orange', 'Banana, Strawberry Fresh');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('630528be0586465dabc2467466c147b8'), 'Evil Creeple', 'Cerealien brauner Zucker Bratapfel');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('baf010bb975b4f7081f36f8c85ee3e1e'), 'Tropenhazard', 'Mango Maracuja');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('82e6455a1c2d4f819ada36940074a28a'), 'Winter - Favorite Waffel', 'Waffel weiße Schokolade');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('af281b23008c4703ac4916018fc4ffc1'), 'Bangcover', 'Himbeer Limonade Brausepulver');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('1e86364981c641b4b39fdf3be9b04d01'), 'Oktobeer', 'Radler');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('08c8c0f199b947aa8edbe773310147de'), 'Grapange', 'Grape Mint');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('8989af0e4e744412a7418ceb04f7e3d1'), 'Radioactea', 'Zitroneneistee');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('91f605e966464e20a9b46dc141f36fa4'), 'Kola Cannonball', 'Cola Limette');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('932e5605bb134b41a9aae095aa6386de'), 'Heisenberg', 'Multi Fruit Menthol');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('de78560e7e4b45e8b5809ab5abafd2c7'), 'Pinkman', 'Berry Menthol');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('64f3da7d33724416878c46a698bd7bd0'), 'Coffee Cake', 'Pound Cake, Coffee');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('7746ec534c1b4e22919e1f08ae10e2dd'), 'Blood Sukka', 'Cherry, Wild Berry, Eucalyptus, Anise');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('d37e0b19f5474713a8f39cd90d4e0f9f'), 'Base-Liquid 100 ml 50/50', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('e85ca58bdab84db89eeb390088e30484'), 'Base-Liquid 500 ml 50/50', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('f73b343462894d36993abd3d0170aa77'), 'Base-Liquid 1000 ml 70/30', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('d850d6f24c044926ac07344fb74b3db8'), 'Base-Liquid 100 ml 70/30', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('eceff942038d4f5295d323122987533e'), 'Base-Liquid 500 ml 70/30', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('0034f2e6665d446cbd0f8cb7a4068978'), 'Base-Liquid 1000 ml 70/30', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('29050a7af9784f8184f5978dc7d11bcd'), 'Base-Liquid 100 ml 90/10', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('162d24736c4e4e418873071d0a94ea49'), 'Base-Liquid 500 ml 90/10', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('c40a110d440b4822bc5e43db69ecc4f9'), 'Base-Liquid 1000 ml 90/10', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('c43e3d225e0f4e388845aedd426d8d72'), 'Nicotine Shots', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('en', unhex('a92d93da8e1d4d85b695536035566e21'), 'Nicotine Shots 5 pack', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('f3c62e09cfb34e0fa602e8d757f8cf51'), 'Matata', 'Apfel, Traube');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('5578c11eca924b5cab5dae21aa807e21'), 'Harambae', 'Grapefruit, Zitrone, Limette, Blutorange, Guave');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('2160e31214134c1397cb61e49363c6ea'), 'Hakuna', 'Apfel, Cranberry');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('1fb96a51bfbd4820bf81a00d92c08b38'), 'Lemur', 'Zitrone, Limette');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('479a0dbab81948f7b343c9394c3db4f9'), 'Patas Pipe', 'Tabak, dunkle Schokolade, Vanille');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('1dabfc6fbe904d4fa3cd7247a8f7c636'), 'Pinkman', 'Beeren Menthol');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('e5a7c436cda64c95a876daebd0ade4d3'), 'Heisenberg', 'Mehrfrucht Menthol');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('3878e1546b554ede800645b80223e5a5'), 'Blood Sukka', 'Kirsche Waldfrucht Eukalyptus Anis');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('d355ee038f4645a38cb3033e9fb6a994'), 'Pie Eyed', 'Blätterteig Zitrone');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('8b2cdb75b1c14327b547260ebc7d03ed'), 'Truth Or Pear', 'Erdbeere Limette Birne');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('40ce16e928624ced8e0d4706c6747396'), 'Love Bite', 'Blutorange Grapefruit');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('2a396d773dda4c8fbb3f1054a19eb492'), 'Bite the Bullet', 'Schwarze Johannisbeere Anis');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('7691c940885647d29976d31e02c85f15'), 'Bluemonia', 'Blaubeere Himbeere Menthol');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('7df77e10c60c4b31af19f9f572e86a16'), 'Tricky Tuff', 'Kaktusfeige Lychee');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('fc0699bd1d084129b2c6a026aab3a34a'), 'Legendary v2', 'Blaubeere Melone');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('ba56aaa514dc4646abfdfb2fcb9230be'), 'Funky Vamp', 'Fruchtmix');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('450994c2ebf24b7db12bae9c13e1494f'), 'Apple Sour Candy', 'Saurer Apfel Süßigkeit');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('1c22f883a3e449f3af45ee7fb3525a30'), 'Pineapple Sour Candy', 'Ananas Süßigkeit');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('e2ad68af13464337a168c390d1920189'), 'Watermelon Sour Candy', 'Melone Süßigkeit');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('eca789c334c14471ad4266bcf64dcf1c'), 'Cola Cherry Bomb', 'Cola, Kirsche, Himbeer');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('4ef4f9f4913e4a70ae47a7b2e063a2ad'), 'Blue Cake Bomb', 'Blaubeer Käsekuchen');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('32674659c05f4ccfb52d78c203f71c57'), 'Sweet Bomb', 'Vanille, Banane, Brombeere, Zimt');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('04218042cfe34562b939478416b9f358'), 'Strawberry Bomb', 'Erdbeere');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('cc064703ab234d34ab1b4a9189506875'), 'Green Bomb', 'Kaktus Kiwi Koolada');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('376949bb6d4e479aa726ae6670471d83'), 'Apple Equinox', 'Grüner Apfel, Erdbeere, Butterscotch');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('a3003262e69447b8864459491dcb4b1e'), 'Peach Parallax', 'Pfirsich Steinfrüchte Frische');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('8ce8e910aff7441685f3c189da870bb2'), 'Astral Orange', 'Banane, Erdbeere Frische');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('630528be0586465dabc2467466c147b8'), 'Evil Creeple', 'Cerealien brauner Zucker Bratapfel');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('baf010bb975b4f7081f36f8c85ee3e1e'), 'Tropenhazard', 'Mango Maracuja');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('82e6455a1c2d4f819ada36940074a28a'), 'Winter - Lieblingswaffel', 'Waffel weiße Schokolade');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('af281b23008c4703ac4916018fc4ffc1'), 'Bangcover', 'Himbeer Limonade Brausepulver');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('1e86364981c641b4b39fdf3be9b04d01'), 'Oktobeer', 'Radler');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('08c8c0f199b947aa8edbe773310147de'), 'Grapange', 'Traube Minze');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('8989af0e4e744412a7418ceb04f7e3d1'), 'Radioactea', 'Zitroneneistee');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('91f605e966464e20a9b46dc141f36fa4'), 'Kola Cannonball', 'Cola Limette');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('932e5605bb134b41a9aae095aa6386de'), 'Heisenberg', 'Mehrfrucht Menthol');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('de78560e7e4b45e8b5809ab5abafd2c7'), 'Pinkman', 'Beeren Menthol');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('64f3da7d33724416878c46a698bd7bd0'), 'Coffee Cake', 'Rührkuchen, Kaffee');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('7746ec534c1b4e22919e1f08ae10e2dd'), 'Blood Sukka', 'Kirsche Waldfrucht Eukalyptus Anis');

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('d37e0b19f5474713a8f39cd90d4e0f9f'), 'Basis-Liquid 100 ml 50/50', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('e85ca58bdab84db89eeb390088e30484'), 'Basis-Liquid 500 ml 50/50', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('f73b343462894d36993abd3d0170aa77'), 'Basis-Liquid 1000 ml 70/30', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('d850d6f24c044926ac07344fb74b3db8'), 'Basis-Liquid 100 ml 70/30', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('eceff942038d4f5295d323122987533e'), 'Basis-Liquid 500 ml 70/30', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('0034f2e6665d446cbd0f8cb7a4068978'), 'Basis-Liquid 1000 ml 70/30', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('29050a7af9784f8184f5978dc7d11bcd'), 'Basis-Liquid 100 ml 90/10', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('162d24736c4e4e418873071d0a94ea49'), 'Basis-Liquid 500 ml 90/10', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('c40a110d440b4822bc5e43db69ecc4f9'), 'Basis-Liquid 1000 ml 90/10', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('c43e3d225e0f4e388845aedd426d8d72'), 'Nikotin Shots', null);

insert into `ProductTexts`(`LanguageCode`, `ProductId`, `ProductName`, `Flavour`)
values ('de', unhex('a92d93da8e1d4d85b695536035566e21'), 'Nikotin Shots 5er', null);

