create database if not exists `VapeMode` character set utf8mb4 collate utf8mb4_unicode_ci;

use `VapeMode`;

/* Table for Languages */
create table if not exists `Languages`(
  `LanguageCode` char(2) not null comment 'Language Code (ISO 639-1)',
  `LanguageName` varchar(40) not null comment 'Language Name',
  constraint `LanguagesPk` primary key (`LanguageCode`))
  comment 'Languages';

/* Table for Tables */
create table if not exists `Tables`(
  `TableName` varchar(30) not null comment 'Table Name',
  constraint `TablesPk` primary key (`TableName`))
  comment 'Tables';

/* Table for Table Texts */
create table if not exists `TableTexts`(
  `TableName` varchar(30) not null comment 'Table Name',
  `LanguageCode` char(2) not null comment 'Language Code',
  `TableDescription` varchar(40) not null comment 'Table Description',
  `ClassDescription` varchar(40) not null comment 'Class Description',
  constraint `TableTextsTableNaFk` foreign key (`TableName`) references `Tables`(`TableName`),
  constraint `TableTextsLanguageCoFk` foreign key (`LanguageCode`) references `Languages`(`LanguageCode`),
  constraint `TableTextsPk` primary key (`TableName`, `LanguageCode`))
  comment 'Table Texts';

/* Table for Table Columns */
create table if not exists `Columns`(
  `TableName` varchar(30) not null comment 'Table Name',
  `ColumnName` varchar(30) not null comment 'Column Name',
  constraint `ColumnsTableNaFk` foreign key (`TableName`) references `Tables`(`TableName`),
  constraint `ColumnsPk` primary key (`TableName`, `ColumnName`))
  comment 'Table Columns';

/* Table for Table Column Texts */
create table if not exists `ColumnTexts`(
  `TableName` varchar(30) not null comment 'Table Name',
  `ColumnName` varchar(30) not null comment 'Column Name',
  `LanguageCode` char(2) not null comment 'Language Code',
  `ColumnDescription` varchar(40) not null comment 'Column Description',
  constraint `ColumnTextsTableNaFk` foreign key (`TableName`, `ColumnName`) references `Columns`(`TableName`, `ColumnName`),
  constraint `ColumnTextsLanguageCoFk` foreign key (`LanguageCode`) references `Languages`(`LanguageCode`),
  constraint `ColumnTextsPk` primary key (`TableName`, `ColumnName`, `LanguageCode`))
  comment 'Table Column Texts';

/* Table for Users */
create table if not exists `Users`(
  `UserId` binary(16) not null comment 'User ID',
  `UserName` varchar(40) not null comment 'User Name',
  `PasswordHash` varchar(255) not null comment 'Password Hash',
  `ExpirationDate` datetime null comment 'Expiration Date',
  `LastLoginDate` datetime null comment 'Last Login Date',
  `LastActivityDate` datetime null comment 'Last Login Date',
  `Email` varchar(320) not null comment 'E-Mail Address',
  `FirstName` varchar(40) not null comment 'First Name',
  `LastName` varchar(40) not null comment 'Last Name',
  `UserAgent` varchar(200) null comment 'User Agent',
  `RemoteIpAddress` varchar(45) null comment 'Remote IP Address',
  `ForwardedIpAddress` varchar(45) null comment 'Forwarded IP Address',
  constraint `UsersEmailUq` unique key (`Email`(191)),
  constraint `UsersPk` primary key (`UserId`))
  comment 'Users';

/* Table for Roles */
create table if not exists `Roles`(
  `RoleId` binary(16) not null comment 'Role ID',
  `RoleName` varchar(30) not null comment 'Role Name',
  constraint `RolesRoleNameUq` unique key (`RoleName`),
  constraint `RolesPk` primary key (`RoleId`))
  comment 'Roles';

/* Table for Role Texts */
create table if not exists `RoleTexts`(
  `RoleId` binary(16) not null comment 'Role ID',
  `LanguageCode` char(2) not null comment 'Language Code',
  `RoleDescription` varchar(40) not null comment 'Role Description',
  constraint `RoleTextsRoleFk` foreign key (`RoleId`) references `Roles`(`RoleId`),
  constraint `RoleTextsLanguageCoFk` foreign key (`LanguageCode`) references `Languages`(`LanguageCode`),
  constraint `RoleTextsPk` primary key (`RoleId`, `LanguageCode`))
  comment 'Role Texts';

/* Table for User Roles */
create table if not exists `UserRoles`(
  `UserId` binary(16) not null comment 'User ID',
  `RoleId` binary(16) not null comment 'Role ID',
  constraint `UserRolesUserFk` foreign key (`UserId`) references `Users`(`UserId`),
  constraint `UserRolesRoleFk` foreign key (`RoleId`) references `Roles`(`RoleId`),
  constraint `UserRolesPk` primary key (`UserId`, `RoleId`))
  comment 'User Roles';

/* Table for Addresses */
create table if not exists `Addresses`(
  `AddressId` binary(16) not null comment 'Address ID',
  `Postcode` char(5) not null comment 'Postcode',
  `City` varchar(50) not null comment 'City',
  `StreetHouseNo` varchar(80) not null comment 'Street and House No.',
  constraint `AddressesFullTextUq` unique key (`Postcode`, `City`, `StreetHouseNo`),
  constraint `AddressesPk` primary key (`AddressId`))
  comment 'Addresses';

/* Table for Customers */
create table if not exists `Customers`(
  `CustomerId` binary(16) not null comment 'Customer ID',
  `IdCardNo` varchar(26) not null comment 'ID Card No.',
  `BillingAddressId` binary(16) not null comment 'Billing Address ID',
  `DefaultShippingAddressId` binary(16) not null comment 'Default Shipping Address ID',
  constraint `CustomersCustomerFk` foreign key (`CustomerId`) references `Users`(`UserId`),
  constraint `CustomersBillAddrFk` foreign key (`BillingAddressId`) references `Addresses`(`AddressId`),
  constraint `CustomersDefShipAddrFk` foreign key (`DefaultShippingAddressId`) references `Addresses`(`AddressId`),
  constraint `CustomersPk` primary key (`CustomerId`))
  comment 'Customers';

/* Table for Products */
create table if not exists `Products`(
  `ProductId` binary(16) not null comment 'Product ID',
  `ProductType` char(1) not null comment 'Produkt Type (''A'' => Aroma, ''B'' => Base, ''L'' => Liquid)',
  `Stock` int not null default 0 comment 'Stock',
  `UnitPrice` decimal(8, 2) not null comment 'Unit Price',
  `Nicotine` smallint not null default 0 comment 'Nicotine',
  `Content` smallint not null comment 'Content',
  `MixRecommendationMin` smallint null comment 'Mix Recommendation, Minumum',
  `MixRecommendationMax` smallint null comment 'Mix Recommendation, Maximum',
  `Manufacturer` varchar(50) not null comment 'Manufacturer',
  `ImageFileName` varchar(120) not null comment 'Image File Name',
  constraint `ProductsProductTypeCk` check ('ProductType in (''A'', ''B'', ''L'')'),
  constraint `ProductsPk` primary key (`ProductId`))
  comment 'Products';

/* Table for Product Texts */
create table if not exists `ProductTexts`(
  `ProductId` binary(16) not null comment 'Product ID',
  `LanguageCode` char(2) not null comment 'Language Code',
  `ProductName` varchar(80) not null comment 'Product Name',
  `Flavour` varchar(64) null comment 'Flavour',
  constraint `ProductTextsProductFk` foreign key (`ProductId`) references `Products`(`ProductId`),
  constraint `ProductTextsLanguageCoFk` foreign key (`LanguageCode`) references `Languages`(`LanguageCode`),
  constraint `ProductTextsPk` primary key (`ProductId`, `LanguageCode`))
  comment 'Product Texts';

/* Table for Orders */
create table if not exists `Orders`(
  `OrderId` binary(16) not null comment 'Order ID',
  `OrderDate` datetime not null default current_timestamp comment 'Order Date',
  `OrderState` char(1) not null default 'O' comment 'Order State (''O'' => Open, ''C'' => Closed, ''I'' => Invalid)',
  `CustomerId` binary(16) not null comment 'Customer ID',
  `ShippingAddressId` binary(16) not null comment 'Shipping Address ID',
  `TotalPrice` decimal(8, 2) not null default 0 comment 'Total Price',
  constraint `OrdersOrderStateCk` check ('OrderState in (''O'', ''C'', ''I'')'),
  constraint `OrdersCustomerIdUq` unique key (`CustomerId`),
  constraint `OrdersCustomerFk` foreign key (`CustomerId`) references `Customers`(`CustomerId`),
  constraint `OrdersShippingAddressFk` foreign key (`ShippingAddressId`) references `Addresses`(`AddressId`),
  constraint `OrdersPk` primary key (`OrderId`))
  comment 'Orders';

/* Table for Order Items */
create table if not exists `OrderItems`(
  `OrderLineId` binary(16) not null comment 'Order Line ID',
  `OrderId` binary(16) not null comment 'Order ID',
  `CreationDate` datetime not null default current_timestamp comment 'Creation Date',
  `ProductId` binary(16) null comment 'Pruduct ID',
  `Quantity` int null comment 'Quantity',
  `UnitPrice` decimal(8, 2) null comment 'Unit Price',
  constraint `OrderItemsOrderFk` foreign key (`OrderId`) references `Orders`(`OrderId`),
  constraint `OrderItemsProductFk` foreign key (`ProductId`) references `Products`(`ProductId`),
  constraint `OrderItemsPk` primary key (`OrderLineId`))
  comment 'Order Items';

/* Table for Basket Items */
create table if not exists `BasketItems`(
  `BasketItemId` binary(16) not null comment 'Basket Item ID',
  `CustomerId` binary(16) not null comment 'Customer ID',
  `CreationDate` datetime not null default current_timestamp comment 'Creation Date',
  `ProductId` binary(16) not null comment 'Product ID',
  `Quantity` int not null comment 'Quantity',
  constraint `BasketItemsCustomerFk` foreign key (`CustomerId`) references `Customers`(`CustomerId`),
  constraint `BasketItemsProductFk` foreign key (`ProductId`) references `Products`(`ProductId`),
  constraint `BasketItemsPk` primary key (`BasketItemId`))
  comment 'Basket Items';
