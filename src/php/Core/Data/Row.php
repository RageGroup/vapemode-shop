<?php namespace Core\Data;

// use \DateTime;
use \Core\Data\Table;

/**
 * A basic table row.
 */
abstract class Row {

  protected $table;
  protected $data;
  protected $origData = [];
  protected $locStrs = [];
  protected $origLocStrs = [];
  protected $inserted;
  protected $updated;

  public function getTable(): Table { return $this->table; }
  public function getData(): array { return $this->data; }
  public function getOrigData(): array { return $this->origData; }
  public function getLocStrs(): array { return $this->locStrs; }
  public function getOrigLocStrs(): array { return $this->origLocStrs; }
  public function isInserted(): bool { return $this->inserted; }
  public function isUpdated(): bool { return $this->updated; }

  public function __construct(Table $table, array $data = null, bool $inserted = true) {
    $this->table = $table;
    $this->data = $data ?? [];
    $this->inserted = $inserted;
    $this->updated = false;
  }

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  abstract protected function getPkHash(): string;

  public function hasColumn(string $columnName): bool { return isset($this->data[$columnName]); }

  public function get(string $columnName) { return $this->data[$columnName] ?? null; }
  public function getInt(string $columnName): ?int { return (int) ($this->data[$columnName] ?? null); }
  public function getFloat(string $columnName): ?float { return (float) ($this->data[$columnName] ?? null); }
  public function getString(string $columnName): ?string { return (string) ($this->data[$columnName] ?? null); }

  public function getDateTime(string $columnName): ?DateTime {
    $value = $this->data[$columnName] ?? null;
    if ($value instanceof DateTime) {
      return $value;
    }
    return new DateTime((string) $value);
  }

  public function getUuid(string $columnName): ?Uuid {
    $value = $this->data[$columnName] ?? null;
    if ($value instanceof Uuid) {
      return $value;
    }
    return Uuid::createFromBin($this->getString($columnName));
  }

  public function set(string $columnName, $value): void {
    if ($this->inserted) {
      $this->origData[$columnName] = $this->data[$columnName] ?? null;
      $this->updated = true;
      $this->table->markRowAsUpdated($this);
    }
    $this->data[$columnName] = $value;
  }

  /**
   * Set a localized string in the associated text table.
   *
   * @param string $columnName The name of the localized text column.
   * @param string $languageCode The id of the language.
   * @param string $value The localized text value to set.
   */
  protected function setLocalizedString(string $columnName, ?string $languageCode, ?string $value): void {
    if (\is_null($this->locStrs[$languageCode])) {
      $this->locStrs[$languageCode] = [];
      $this->origLocStrs[$languageCode] = [];
    }
    $this->origLocStrs[$languageCode][$columnName] = $this->locStrs[$languageCode][$columnName];
    $this->locStrs[$languageCode][$columnName] = $value;
  }

}
