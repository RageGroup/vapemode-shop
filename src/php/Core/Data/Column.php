<?php namespace Core\Data;

final class Column {

  protected $columnName;
  protected $description;
  protected $localizedDescription;
  protected $primaryKey;
  protected $dataType;

  public function getColumnName(): string { return $this->columnName; }
  public function getDescription(): string { return $this->description; }
  public function getLocalizedDescription(): string { return $this->localizedDescription; }
  public function isPrimaryKey(): bool { return $this->primaryKey; }
  public function getDataType(): string { return $this->dataType; }

  public function __construct(
    string $columnName,
    string $description,
    string $localizedDescription,
    bool $primaryKey = false,
    string $dataType = '')
  {
    $this->columnName = $columnName;
    $this->description = $description;
    $this->localizedDescription = $localizedDescription;
    $this->primaryKey = $primaryKey;
    $this->dataType = $dataType;
  }

}
