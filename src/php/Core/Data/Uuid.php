<?php namespace Core\Data;

final class Uuid {

  private $time_low;
  private $time_mid;
  private $time_hi_and_version;
  private $clk_seq_hi_res_and_low;
  private $node;

  private function __construct(
    string $time_low,
    string $time_mid,
    string $time_hi_and_version,
    string $clk_seq_hi_res_and_low,
    string $node)
  {
    $this->time_low = $time_low;
    $this->time_mid = $time_mid;
    $this->time_hi_and_version = $time_hi_and_version;
    $this->clk_seq_hi_res_and_low = $clk_seq_hi_res_and_low;
    $this->node = $node;
  }

  public static function createFromHexParts(
    string $time_low,
    string $time_mid,
    string $time_hi_and_version,
    string $clk_seq_hi_res_and_low,
    string $node): Uuid
  {
    return new Uuid($time_low, $time_mid, $time_hi_and_version, $clk_seq_hi_res_and_low, $node);
  }

  /**
  * Get a UUID from a binary 16-bytes string.
  *
  * @param string $uuid_bin_string
  * @return Uuid
  */
  public static function createFromBin(string $uuid_bin_string): Uuid {
    return Uuid::createFromHex(bin2hex($uuid_bin_string));
  }

  /**
  * Get a UUID from a unformatted, 32-bytes string.
  *
  * @param string $uuid_hex_string
  * @return Uuid
  */
  public static function createFromHex(string $uuid_hex_string): Uuid {
    $time_low = substr($uuid_hex_string, 0, 8);
    $time_mid = substr($uuid_hex_string, 8, 4);
    $time_hi_and_version = substr($uuid_hex_string, 12, 4);
    $clk_seq_hi_res_and_low = substr($uuid_hex_string, 16, 4);
    $node = substr($uuid_hex_string, 20, 12);
    return new Uuid($time_low, $time_mid, $time_hi_and_version, $clk_seq_hi_res_and_low, $node);
  }

  public static function createFromFormattedHex(string $uuid_hex_string): Uuid {
    $time_low = substr($uuid_hex_string, 0, 8);
    $time_mid = substr($uuid_hex_string, 8 + 1, 4);
    $time_hi_and_version = substr($uuid_hex_string, 12 + 2, 4);
    $clk_seq_hi_res_and_low = substr($uuid_hex_string, 16 + 3, 4);
    $node = substr($uuid_hex_string, 20 + 4, 12);
    return new Uuid($time_low, $time_mid, $time_hi_and_version, $clk_seq_hi_res_and_low, $node);
  }

  public static function generate(): Uuid {
    return self::createFromHex(self::generateNewVersion4Hex());
  }

  private static function generateNewVersion4(): Uuid {
    return self::createFromHex(self::generateNewVersion4Hex());
  }

  private static function generateNewVersion4Hex(): string {
    return sprintf('%04x%04x%04x%04x%04x%04x%04x%04x',
      // 32 bits for "time_low"
      random_int(0, 0xffff), random_int(0, 0xffff),

      // 16 bits for "time_mid"
      random_int(0, 0xffff),

      // 16 bits for "time_hi_and_version",
      // four most significant bits holds version number 4
      random_int(0, 0x0fff) | 0x4000,

      // 16 bits, 8 bits for "clk_seq_hi_res",
      // 8 bits for "clk_seq_low",
      // two most significant bits holds zero and one for variant DCE1.1
      random_int(0, 0x3fff) | 0x8000,

      // 48 bits for "node"
      random_int(0, 0xffff), random_int(0, 0xffff), random_int(0, 0xffff)
    );
  }

  public function __toString() {
    return hex2bin(sprintf('%s%s%s%s%s',
      $this->time_low,
      $this->time_mid,
      $this->time_hi_and_version,
      $this->clk_seq_hi_res_and_low,
      $this->node));
  }

  public function toString(): string {
    return hex2bin(sprintf('%s%s%s%s%s',
      $this->time_low,
      $this->time_mid,
      $this->time_hi_and_version,
      $this->clk_seq_hi_res_and_low,
      $this->node));
  }

  public function toHexString(): string {
    return sprintf('%s%s%s%s%s',
      $this->time_low,
      $this->time_mid,
      $this->time_hi_and_version,
      $this->clk_seq_hi_res_and_low,
      $this->node);
  }

  public function toFormattedHexString(): string {
    return sprintf('%s-%s-%s-%s-%s',
      $this->time_low,
      $this->time_mid,
      $this->time_hi_and_version,
      $this->clk_seq_hi_res_and_low,
      $this->node);
  }

}
