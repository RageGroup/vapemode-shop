<?php namespace Core\Data;

/**
* A sql parameter.
*/
final class SqlParameter {

  /**
  * The column name that this parameter is connected to.
  *
  * @var string
  */
  private $columnName;

  /**
  * Get the column name that this parameter is connected to.
  *
  * @return string
  */
  public function getColumnName(): string { return $this->columnName; }

  /**
  * The comparator for this parameter.
  *
  * @var string
  */
  private $comparator;

  /**
  * Get the comparator for this parameter.
  *
  * @return string
  */
  public function getComparator(): string { return $this->comparator; }

  /**
  * The column value that this parameter holds.
  *
  * @var string
  */
  private $columnValue;

  /**
  * Get the column value that this parameter holds.
  *
  * @return string
  */
  public function getColumnValue(): string { return $this->columnValue; }

  /**
  * Create a new sql parameter.
  *
  * @param string $columnName
  * @param string $comparator
  * @param string $columnValue
  */
  public function __construct(string $columnName, string $comparator, string $columnValue) {
    $this->columnName = $columnName;
    $this->comparator = $comparator;
    $this->columnValue = $columnValue;
  }

}
