<?php namespace Core\Data;

use \Core\IO;

/**
 * Represents static localization.
 */
class Localization {

  protected $data;

  /**
   * Get a localized text.
   *
   * @param string $textId The text ID of the localized text to get.
   * @param string $languageCode The language ID of the localized text to get.
   * @return string A localized text.
   */
  public function getText(string $textId, string $languageCode): ?string {
    $text = null;
    if (isset($this->data[$textId])) {
      $text = $this->data[$textId][$languageCode] ?? null;
    }
    return $text;
  }

  /**
   * Create a localization object.
   *
   * @param Table $table
   * @param array $data
   */
  public function __construct(string $filepath, string $filename = '../../config/l10n-config.json') {
    $this->data = IO::getJsonData($filepath, $filename);
  }

}
