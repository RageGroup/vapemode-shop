<?php namespace Core\Data;

use \PDO;
use \Core\Data\DbConnection;

final class MySqlConnection extends DbConnection {

  public function __construct(array $config) {
    parent::__construct($config, [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]);
  }

  /**
   * Command text for selection of one or more table rows.
   *
   * @param array &$args The arguments for command execution.
   * @param array $tableName The name of the table to select from.
   * @param array $conditions The conditions for the select command. An array of strings mixed with sql parameters.
   * @param array $columnNames The names of the columns to select.
   * @return array The data that the select command provides. A array of mapping arrays.
   */
  protected function selectCommandText(
    array &$args,
    string $tableName,
    array $conditions,
    array $columnNames,
    int $count,
    int $offset):
    string
  {
    $commandText = parent::selectCommandText($args, $tableName, $conditions, $columnNames, $count, $offset);
    if ($count > 0) {
      $commandText .= ' limit ' . ($offset > 0 ? $offset . ', ' : '') . $count;
    }
    return $commandText;
  }

  /**
  * Command text for inserting of a new table row.
  *
  * @param string $tableName
  * @param array $columnData
  * @return void
  */
  protected function insertCommandText(string $tableName, array $columnData): string {
    $commandText = parent::insertCommandText($tableName, $columnData);
    // $commandText .= ' on duplicate key update';
    return $commandText;
  }

  /**
  * Command text for updating a table row chosen by its ID.
  *
  * @param string $tableName
  * @param array $idColumnNames
  * @param array $oldData
  * @param array $newData
  * @return void
  */
  protected function updateCommandText(string $tableName, array $idColumnNames, array $oldData, array $newData): string {
    return parent::updateCommandText($tableName, $idColumnNames, $oldData, $newData);
  }

  protected function getTableColumns(string $tableName): array {
    $rows = $this->execute(
      'select COLUMN_NAME, COLUMN_COMMENT, COLUMN_KEY ' .
      'from `INFORMATION_SCHEMA`.`COLUMNS` ' .
      'where TABLE_NAME = :TABLE_NAME',
      ['TABLE_SCHEMA' => $this->dbname, 'TABLE_NAME' => $tableName]);
    $columns = [];
    foreach ($rows as $row) {
      $columnName = $row['COLUMN_NAME'];
      $description = $row['COLUMN_COMMENT'];
      $isPrimaryKey = $row['COLUMN_KEY'] == 'PRI';
      $columns[$columnName] = new Column($columnName, $description, $isPrimaryKey);
    }
    return $columns;
  }

}
