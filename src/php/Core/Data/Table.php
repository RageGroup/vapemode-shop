<?php namespace Core\Data;

use \Core\Data\DbContext;
use \Core\Data\SqlParameter;
use \Core\Data\Row;

abstract class Table {

  protected $context;
  protected $tableName;
  protected $idColumnNames;
  protected $columns = [];
  protected $primaryKeyColumns = [];
  protected $nonPrimaryKeyColumns = [];
  protected $selectedRows = [];
  protected $insertedRows = [];
  protected $updatedRows = [];
  protected $deletedRows = [];

  public function getContext(): DbContext { return $this->context; }
  public function getTableName(): string { return $this->tableName; }
  public function getTextTableName(): string { return $this->textTableName; }
  public function getIdColumnNames(): array { return $this->idColumnNames; }
  public function getColumns(): array { return $this->columns; }
  public function getPrimaryKeyColumns(): array { return $this->primaryKeyColumns; }
  public function getNonPrimaryKeyColumns(): array { return $this->nonPrimaryKeyColumns; }

  public function __construct(
    DbContext $context,
    string $tableName,
    string $textTableName,
    array $idColumnNames,
    array &$columns)
  {
    $this->context = $context;
    $this->tableName = $tableName;
    $this->textTableName = $textTableName;
    $this->idColumnNames = $idColumnNames;
    $this->columns = $columns;
    $this->columnTextsByLangId = [];
    // foreach ($columns as $columnName => $column) {
    //   if ($column->isPrimaryKey()) {
    //     $this->primaryKeyColumns[$columnName] = $column;
    //   }
    //   else {
    //     $this->nonPrimaryKeyColumns[$columnName] = $column;
    //   }
    // }
  }

  public function getDescription(
    string $columnName,
    ?string $languageCode = null,
    bool $columnIslocalized = false):
    ?string
  {
    $languageCode = $languageCode ?? $this->context->getLanguageCode();
    if (!isset($this->columnTextsByLangId[$languageCode])) {
      $this->columnTextsByLangId[$languageCode] = [];
    }
    if (!isset($this->columnTextsByLangId[$languageCode][$columnName])) {
      $conditions = [new SqlParameter('TableName', '=', ($columnIslocalized ? $this->textTableName : $this->tableName)),
                     ' and ',
                     new SqlParameter('LanguageCode', '=', $languageCode)];
      $rowDataArray = $this->context->getDbConnection()->select(
        'ColumnTexts', $conditions, ['ColumnName', 'ColumnDescription']);
      $this->columnTextsByLangId[$languageCode][$columnName] = null;
      foreach ($rowDataArray as $rowData) {
        $this->columnTextsByLangId[$languageCode][$rowData['ColumnName']] =
          $rowData['ColumnDescription'];
      }
    }
    return $this->columnTextsByLangId[$languageCode][$columnName];
  }

  /**
   * Get a new row object from an array fetched by a pdo statement.
   *
   * @param array $data
   * @return Row
   */
  abstract protected function newRow(array $data): Row;

  /**
   * Mark the given row as updated.
   *
   * @param Row $row
   * @return void
   */
  public function markRowAsUpdated(Row $row): void {
    $this->updatedRows[$row->getPkHash()] = $row;
  }

  /**
   * Mark the given row as deleted.
   *
   * @param Row $row
   * @return void
   */
  public function delete(Row $row): void {
    $hash = $row->getPkHash();
    if (isset($this->updatedRows[$hash])) {
      unset($this->updatedRows[$hash]);
    }
    $this->deletedRows[$hash] = $row;
  }

  /**
   * Select table data.
   * @param array $conditions The conditions for the select command.
   * @param array $columnNames The names of the columns to select.
   * @return array The data rows that the select command provides. An array of rows by their ids.
   */
  protected function select(array $conditions = [], array $columnNames = [], int $count = 0, int $offset = 0): array {
    $rows = [];
    $rowDataArray = $this->context->getDbConnection()->select($this->tableName, $conditions, $columnNames, $count, $offset);
    foreach ($rowDataArray as $rowData) {
      $row = $this->newRow($rowData);
      $rows[] = $row;
    }
    return $rows;
  }

  /**
   * Get the count of rows that match the given conditions.
   *
   * @param array $conditions The conditions as array with item types SqlParameter or string
   * @return array
   */
  public function count(array $conditions = []): int {
    $rowDataArray = $this->context->getDbConnection()->select($this->tableName, $conditions, ['count(*)']);
    $count = 0;
    foreach ($rowDataArray as $rowData) {
      $count += (int) $rowData['count(*)'];
    }
    return $count;
  }

  /**
   * Get rows that match the given conditions.
   *
   * @param array $conditions The conditions as array with item types SqlParameter or string
   * @param int $count The count of rows to fetch.
   * @param int $offset The offset of rows to fetch.
   * @return array
   */
  public function getRows(array $conditions = [], int $count = 0, int $offset = 0): array {
    return $this->select($conditions, [], $count, $offset);;
  }

  /**
   * Get the first row that matches the given conditions.
   *
   * @param array $conditions The conditions as array with item types SqlParameter or string
   * @return ?Row
   */
  public function getFirstRow(array $conditions = []): ?Row {
    $rows = $this->select($conditions);
    if (count($rows) == 0) {
      return null;
    }
    return $rows[0];
  }

  /**
   * Get a single row that matches the given conditions.
   *
   * @param array $conditions The conditions as array with item types SqlParameter or string
   * @return ?Row
   */
  public function getSingleRow(array $conditions = []): ?Row {
    $rows = $this->select($conditions);
    if (count($rows) == 1) {
      return $rows[0];
    }
    return null;
  }

  /**
   * Save changes for this table.
   *
   * @return void
   */
  public function saveChanges(): void {
    foreach ($this->insertedRows as $row) {
      $this->context->getDbConnection()->insert(
        $this->tableName,
        $row->getData());
    }
    foreach ($this->updatedRows as $row) {
      $this->context->getDbConnection()->update(
        $this->tableName,
        $this->idColumnNames,
        $row->getOrigData(),
        $row->getData());
    }
    foreach ($this->deletedRows as $row) {
      $this->context->getDbConnection()->delete(
        $this->tableName,
        $this->idColumnNames,
        $row->getData());
    }
  }

}
