<?php namespace Core\Data;

use \PDO;
use \Core\IO;
use \Core\Logging\Logger;
use \Core\Data\SqlParameter;
use \Core\Data\MySqlConnection;

abstract class DbConnection {

  protected $dbtype;
  protected $host;
  protected $dbname;
  protected $login;
  protected $password;
  protected $options;
  protected $pdo;

  public function __construct(array $config, array $options = []) {
    $this->dbtype = $config['type'];
    $this->host = $config['host'];
    $this->dbname = $config['name'];
    $this->login = $config['login'];
    $this->password = $config['password'];
    $this->options = $options;
    $this->options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
    $this->options[PDO::ATTR_DEFAULT_FETCH_MODE] = PDO::FETCH_ASSOC;
    $dns = $this->dbtype .
      ':host=' . $this->host .
      ';dbname=' . $this->dbname .
      ';charset=UTF8';
    $this->pdo = new PDO($dns, $this->login, $this->password, $this->options);
  }

  public static function getNewConnection(
    string $filepath,
    string $filename = '../../config/db-config.json'):
    DbConnection
  {
    $config = IO::getJsonData($filepath, $filename);
    switch ($config['type']) {
      case 'mysql':
        $connection = new MySqlConnection($config);
        break;
      default:
        throw new \Exception('Unknown database type');
    }
    return $connection;
  }

  public function beginTransaction(): void { $this->pdo->beginTransaction(); }
  public function commit(): void { $this->pdo->commit(); }
  public function rollback(): void { $this->pdo->rollback(); }

  /**
   * Execute a database command and fetch the results.
   * @param string $commandText The command text to execute.
   * @param array $args The arguments of the command to execute.
   * @return array The data that the command execution provides. A array of mapping arrays.
   */
  public function executeAndFetch(string $commandText, array $args = []): array {
    $cmd = $this->pdo->prepare($commandText);
    foreach ($args as $argName => $argValue) {
      $cmd->bindValue(':' . $argName, $argValue);
    }
    if ($cmd->execute()) {
      return $cmd->fetchAll();
    }
    return [];
  }

  /**
   * Execute a database command.
   * @param string $commandText The command text to execute.
   * @param array $args The arguments of the command to execute.
   * @return bool Has the command executed successfully?
   */
  public function execute(string $commandText, array $args = []): bool {
    $cmd = $this->pdo->prepare($commandText);
    foreach ($args as $argName => $argValue) {
      $cmd->bindValue(':' . $argName, $argValue);
    }
    // var_dump($commandText);
    // var_dump($args);
    // Logger::get()->logNotice($commandText);
    // Logger::get()->logNotice(serialize($args));
    return $cmd->execute();
  }

  /**
   * Select one or more table rows.
   *
   * @param array &$args The arguments for command execution.
   * @param array $tableName The name of the table to select from.
   * @param array $conditions The conditions for the select command. An array of strings mixed with sql parameters.
   * @param array $columnNames The names of the columns to select.
   * @return array The data that the select command provides. A array of mapping arrays.
   */
  protected function selectCommandText(
    array &$args,
    string $tableName,
    array $conditions,
    array $columnNames,
    int $count,
    int $offset):
    string
  {
    $commandText = 'select ';
    if (count($columnNames) > 0) {
      for ($i = 0; $i < count($columnNames); $i++) {
        if ($i > 0) {
          $commandText .= ', ';
        }
        $commandText .= $columnNames[$i];
      }
    }
    else {
      $commandText .= '*';
    }
    $commandText .= ' from ' . $tableName;
    if (count($conditions) > 0) {
      $commandText .= ' where ';
      foreach ($conditions as $condition) {
        if ($condition instanceof SqlParameter) {
          $columnName = $condition->getColumnName();
          $commandText .= $columnName . ' ' .
            $condition->getComparator() . ' :' .
            $columnName;
          $args[$columnName] = $condition->getColumnValue();
        }
        else {
          $commandText .= $condition;
        }
      }
    }
    // Usage of $count/$offset has to implemented in overriding implementations!
    return $commandText;
  }

  /**
   * Select one or more table rows.
   * @param array $tableName The name of the table to select from.
   * @param array $conditions The conditions for the select command. An array of strings mixed with sql parameters.
   * @param array $columnNames The names of the columns to select.
   * @return array The data that the select command provides. A array of mapping arrays.
   */
  public function select(
    string $tableName,
    array $conditions = [],
    array $columnNames = [],
    int $count = 0,
    int $offset = 0):
    array
  {
    $args = [];
    $commandText = $this->selectCommandText($args, $tableName, $conditions, $columnNames, $count, $offset);
    return $this->executeAndFetch($commandText, $args);
  }

  /**
   * Command text for inserting of a new table row.
   *
   * @param string $tableName
   * @param array $columnData
   * @return void
   */
  protected function insertCommandText(string $tableName, array $columnData): string {
    $insertInto = 'insert into ' . $tableName . ' (';
    $values = 'values (';
    $initial = true;
    foreach (array_keys($columnData) as $columnName) {
      if ($initial) {
        $initial = false;
      }
      else {
        $insertInto .= ', ';
        $values .= ', ';
      }
      $insertInto .= $columnName;
      $values .= ':' . $columnName;
    }
    $insertInto .= ') ';
    $values .= ')';
    return $insertInto . $values;
  }

  /**
   * Insert a new table row.
   *
   * @param string $tableName
   * @param array $columnData
   * @return void
   */
  public function insert(string $tableName, array $columnData): void {
    $commandText = $this->insertCommandText($tableName, $columnData);
    $this->execute($commandText, $columnData);
  }

  /**
   * Command text for updating a table row chosen by its ID.
   *
   * @param string $tableName
   * @param array $idColumnNames
   * @param array $oldData
   * @param array $newData
   * @return void
   */
  protected function updateCommandText(string $tableName, array $idColumnNames, array $oldData, array $newData): string {
    $commandText = 'update ' . $tableName . ' set ';
    $initial = true;
    foreach ($newData as $columnName => $columnValue) {
      if (!isset($oldData[$columnName]) || $oldData[$columnName] != $columnValue) {
        if ($initial) {
          $initial = false;
        }
        else {
          $commandText .= ', ';
        }
        $commandText .= $columnName . ' = :' . $columnName;
      }
    }
    $commandText .= ' where ';
    foreach ($idColumnNames as $idColumnName) {
      $commandText .= $idColumnName . ' = :' . $idColumnName;
    }
    return $commandText;
  }

  /**
   * Update a table row chosen by its ID.
   *
   * @param string $tableName
   * @param array $idColumnNames
   * @param array $oldData
   * @param array $newData
   * @return void
   */
  public function update(string $tableName, array $idColumnNames, array $oldData = [], array $newData = []): void {
    if (count($newData) > 0) {
      $commandText = $this->updateCommandText($tableName, $idColumnNames, $oldData, $newData);
      $this->execute($commandText, $newData);
    }
  }

  /**
   * Delete a table row chosen by its ID.
   *
   * @param string $tableName
   * @param array $idColumnNames
   * @return void
   */
  public function delete(string $tableName, array $idColumnNames, array $columnData): void {
    $commandText = 'delete from ' . $tableName . ' where ' . $idColumnNames . ' = :' . $idColumnNames;
    $args = [];
    foreach ($idColumnNames as $idColumnName) {
      $args[$idColumnName] = $columnData[$idColumnName];
    }
    $this->execute($commandText, $args);
  }

  /**
   * Get the column information for the given table.
   *
   * @param string $tableName
   * @return array
   */
  abstract protected function getTableColumns(string $tableName): array;

}
