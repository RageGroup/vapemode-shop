<?php namespace Core\Data;

/**
* A DateTime that is supposed to be usable in a simply way.
*/
final class DateTime extends \DateTime {

  const STANDARD_FORMAT = 'Y-m-d H:i:s';

  /**
  * Create a DateTime object.
  *
  * @param string $valueText
  */
  public function __construct(string $valueText = 'now') {
    parent::__construct($valueText);
  }

  /**
  * Return a date time string in the standard format.
  *
  * @return String
  */
  public function __toString() {
    return $this->format(self::STANDARD_FORMAT);
  }

}
