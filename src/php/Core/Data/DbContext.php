<?php namespace Core\Data;

use \Core\IO;
use \Core\Logging\Logger;
use \Core\Data\DbConnection;
use \Core\Models\Users;
use \Core\Models\User;

abstract class DbContext {

  protected $connection;
  protected $tableAccesses = [];
  protected $languageCode;

  public function getDbConnection(): DbConnection { return $this->connection; }
  public function getTableAccesses(): array { return $this->tableAccesses; }

  /**
   * Get the current language code (ISO 639-1).
   *
   * @return string the current language code (ISO 639-1).
   */
  public function getLanguageCode(): string { return $this->languageCode; }

  /**
   * Set the current language code (ISO 639-1).
   *
   * @param string $value The current language code (ISO 639-1).
   * @return void
   */
  public function setLanguageCode(string $value): void { $this->languageCode = $value; }

  abstract public function getUserTable(): Users;

  public function __construct(string $baseDir) {
    $this->connection = DbConnection::getNewConnection($baseDir);
    $this->languageCode = 'en';
  }

  public function saveChanges(): void {
    $this->connection->beginTransaction();
    foreach ($this->tableAccesses as $tableAccess) {
      $tableAccess->saveChanges();
    }
    $this->connection->commit();
  }

}
