<?php namespace Core;

function isNullOrWhiteSpace(string $value): bool {
  return isset($value) && strlen(trim($value)) > 0;
}

function isNullOrEmpty(string $value): bool {
  return isset($value) && strlen($value) > 0;
}
