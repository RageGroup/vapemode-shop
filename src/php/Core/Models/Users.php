<?php namespace Core\Models;

use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table for Users.
 */
class Users extends UserTable {

  /**
   * Create the table for a context and with table information.
   *
   * @param DbContext $context
   * @param array $columns
   */
  public function __construct(
    DbContext $context,
    string $tableName,
    string $textTableName,
    array $idColumnNames,
    array $columns)
  {
    parent::__construct($context, $tableName, $textTableName, $idColumnNames, $columns);
  }

  public function getByUserName(string $userName): ?User {
    return $this->getFirstRow([new SqlParameter('UserName', '=', $userName)]);
  }

}
