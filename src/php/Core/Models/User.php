<?php namespace Core\Models;

use \Core\Data\Uuid;
use \Core\Data\Table;

/**
* A base class for users.
*/
class User extends UserRow {

  private static $bcryptOptions = ['cost' => 11];

  public function setPassword(string $password): void {
    $this->setPasswordHash(password_hash($password, PASSWORD_BCRYPT, self::$bcryptOptions));
  }

  public function __construct(Table $table, array $data) {
    parent::__construct($table, $data);
  }

  public function verifyPassword(string $password): bool {
    return password_verify($password, $this->getPasswordHash());
  }

}
