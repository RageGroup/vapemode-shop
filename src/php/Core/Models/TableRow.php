<?php namespace Core\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Table;

/**
 * A data table row for one Table.
 * This class was auto-generated.
 */
abstract class TableRow extends \Core\Data\Row {

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public static function pkHash(string $tableName): string {
    $hash = '';
    $hash = \md5($hash . ((string) $tableName), true);
    return $hash;
  }

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public function getPkHash(): string {
    return self::pkHash($this->getString('TableName'));
  }

  /**
   * Get the localized description of the Table Name.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getTableNameDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('TableName', $languageCode);
  }

  /**
   * Get the Table Name.
   *
   * @return string
   */
  public function getTableName(): ?string {
    return $this->getString('TableName');
  }

  /**
   * Set the Table Name.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setTableName(?string $value): void {
    $this->set('TableName', $value);
  }

  /**
   * Get the localized description of the Table Description.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getTableDescriptionDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('TableDescription', $languageCode, true);
  }

  /**
   * Get the Table Description.
   *
   * @return string
   */
  public function getTableDescription(?string $languageCode = null): ?string {
    return $this->table->getLocalizedString($this, 'TableDescription', $languageCode);
  }

  /**
   * Set the Table Description.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setTableDescription(?string $value, ?string $languageCode = null): void {
    $this->setLocalizedString('TableDescription', $languageCode, $value);
  }

  /**
   * Get the localized description of the Class Description.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getClassDescriptionDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('ClassDescription', $languageCode, true);
  }

  /**
   * Get the Class Description.
   *
   * @return string
   */
  public function getClassDescription(?string $languageCode = null): ?string {
    return $this->table->getLocalizedString($this, 'ClassDescription', $languageCode);
  }

  /**
   * Set the Class Description.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setClassDescription(?string $value, ?string $languageCode = null): void {
    $this->setLocalizedString('ClassDescription', $languageCode, $value);
  }

  /**
   * Create an entity object for a table with initial data.
   *
   * @param Table $table
   * @param array $data
   */
  public function __construct(Table $table, array $data, bool $inserted = true) {
    parent::__construct($table, $data, $inserted);
  }

}
