<?php namespace Core\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Row;
use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table base for all User Roles.
 * This class was auto-generated.
 */
class UserRoleTable extends \Core\Data\Table {

  /**
   * Create the table for a context and with table information.
   *
   * @param DbContext $context
   * @param array $columns
   */
  public function __construct(DbContext $context, array $columns) {
    parent::__construct($context, 'UserRoles', '', ['UserId', 'RoleId'], $columns);
  }

  /**
   * Get a row by its data.
   * Exists to be available for the table base implementation.
   *
   * @param array $data
   * @return Row
   */
  protected function newRow(array $data): Row {
    $row = new UserRole($this, $data, false);
    $this->selectedRows[$row->getPkHash()] = $row;
    return $row;
  }

  /**
  * Create a new User Role.
  *
  * @return UserRole
  */
  public function create(
    Uuid $userId,
    Uuid $roleId):
    UserRole
  {
    $entity = new UserRole($this, [
      'UserId' => $userId->toString(),
      'RoleId' => $roleId->toString()
    ], false);
    $hash = $entity->getPkHash();
    $this->selectedRows[$hash] = $entity;
    $this->insertedRows[$hash] = $entity;
    return $entity;
  }

  /**
   * Get an entity object by its primary key value.
   *
   * @param Uuid $userId
   * @param Uuid $roleId
   * @return UserRole
   */
  public function getByPk(Uuid $userId, Uuid $roleId): ?UserRole {
    return $this->getFirstRow([new SqlParameter('UserId', '=', $userId), ' and ',
                               new SqlParameter('RoleId', '=', $roleId)]);
  }


}
