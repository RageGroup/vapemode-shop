<?php namespace Core\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Row;
use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table base for all Languages.
 * This class was auto-generated.
 */
class LanguageTable extends \Core\Data\Table {

  /**
   * Create the table for a context and with table information.
   *
   * @param DbContext $context
   * @param array $columns
   */
  public function __construct(DbContext $context, array $columns) {
    parent::__construct($context, 'Languages', '', ['LanguageCode'], $columns);
  }

  /**
   * Get a row by its data.
   * Exists to be available for the table base implementation.
   *
   * @param array $data
   * @return Row
   */
  protected function newRow(array $data): Row {
    $row = new Language($this, $data, false);
    $this->selectedRows[$row->getPkHash()] = $row;
    return $row;
  }

  /**
  * Create a new Language.
  *
  * @return Language
  */
  public function create(
    string $languageCode,
    string $languageName):
    Language
  {
    $languageCode = Uuid::generate();
    $entity = new Language($this, [
      'LanguageCode' => $languageCode,
      'LanguageName' => $languageName
    ], false);
    $hash = $entity->getPkHash();
    $this->selectedRows[$hash] = $entity;
    $this->insertedRows[$hash] = $entity;
    return $entity;
  }

  /**
   * Get an entity object by its primary key value.
   *
   * @param string $languageCode
   * @return Language
   */
  public function getByPk(string $languageCode): ?Language {
    return $this->getFirstRow([new SqlParameter('LanguageCode', '=', $languageCode)]);
  }


}
