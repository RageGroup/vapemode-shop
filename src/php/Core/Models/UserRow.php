<?php namespace Core\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Table;

/**
 * A data table row for one User.
 * This class was auto-generated.
 */
abstract class UserRow extends \Core\Data\Row {

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public static function pkHash(Uuid $userId): string {
    $hash = '';
    $hash = \md5($hash . ((string) $userId), true);
    return $hash;
  }

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public function getPkHash(): string {
    return self::pkHash($this->getUuid('UserId'));
  }

  /**
   * Get the localized description of the User ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getUserIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('UserId', $languageCode);
  }

  /**
   * Get the User ID.
   *
   * @return Uuid
   */
  public function getUserId(): ?Uuid {
    return $this->getUuid('UserId');
  }

  /**
   * Set the User ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setUserId(?Uuid $value): void {
    $this->set('UserId', $value);
  }

  /**
   * Get the localized description of the User Name.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getUserNameDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('UserName', $languageCode);
  }

  /**
   * Get the User Name.
   *
   * @return string
   */
  public function getUserName(): ?string {
    return $this->getString('UserName');
  }

  /**
   * Set the User Name.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setUserName(?string $value): void {
    $this->set('UserName', $value);
  }

  /**
   * Get the localized description of the Password Hash.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getPasswordHashDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('PasswordHash', $languageCode);
  }

  /**
   * Get the Password Hash.
   *
   * @return string
   */
  public function getPasswordHash(): ?string {
    return $this->getString('PasswordHash');
  }

  /**
   * Set the Password Hash.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setPasswordHash(?string $value): void {
    $this->set('PasswordHash', $value);
  }

  /**
   * Get the localized description of the Expiration Date.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getExpirationDateDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('ExpirationDate', $languageCode);
  }

  /**
   * Get the Expiration Date.
   *
   * @return DateTime
   */
  public function getExpirationDate(): ?DateTime {
    return $this->getDateTime('ExpirationDate');
  }

  /**
   * Set the Expiration Date.
   *
   * @param DateTime $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setExpirationDate(?DateTime $value): void {
    $this->set('ExpirationDate', $value);
  }

  /**
   * Get the localized description of the Last Login Date.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getLastLoginDateDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('LastLoginDate', $languageCode);
  }

  /**
   * Get the Last Login Date.
   *
   * @return DateTime
   */
  public function getLastLoginDate(): ?DateTime {
    return $this->getDateTime('LastLoginDate');
  }

  /**
   * Set the Last Login Date.
   *
   * @param DateTime $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setLastLoginDate(?DateTime $value): void {
    $this->set('LastLoginDate', $value);
  }

  /**
   * Get the localized description of the Last Login Date.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getLastActivityDateDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('LastActivityDate', $languageCode);
  }

  /**
   * Get the Last Login Date.
   *
   * @return DateTime
   */
  public function getLastActivityDate(): ?DateTime {
    return $this->getDateTime('LastActivityDate');
  }

  /**
   * Set the Last Login Date.
   *
   * @param DateTime $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setLastActivityDate(?DateTime $value): void {
    $this->set('LastActivityDate', $value);
  }

  /**
   * Create an entity object for a table with initial data.
   *
   * @param Table $table
   * @param array $data
   */
  public function __construct(Table $table, array $data, bool $inserted = true) {
    parent::__construct($table, $data, $inserted);
  }

}
