<?php namespace Core\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Table;

/**
 * A data table row for one User Role.
 * This class was auto-generated.
 */
abstract class UserRoleRow extends \Core\Data\Row {

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public static function pkHash(Uuid $userId,Uuid $roleId): string {
    $hash = '';
    $hash = \md5($hash . ((string) $userId), true);
    $hash = \md5($hash . ((string) $roleId), true);
    return $hash;
  }

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public function getPkHash(): string {
    return self::pkHash($this->getUuid('UserId'),
                        $this->getUuid('RoleId'));
  }

  /**
   * Get the localized description of the User ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getUserIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('UserId', $languageCode);
  }

  /**
   * Get the User ID.
   *
   * @return Uuid
   */
  public function getUserId(): ?Uuid {
    return $this->getUuid('UserId');
  }

  /**
   * Set the User ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setUserId(?Uuid $value): void {
    $this->set('UserId', $value);
  }

  /**
   * Get the localized description of the Role ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getRoleIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('RoleId', $languageCode);
  }

  /**
   * Get the Role ID.
   *
   * @return Uuid
   */
  public function getRoleId(): ?Uuid {
    return $this->getUuid('RoleId');
  }

  /**
   * Set the Role ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setRoleId(?Uuid $value): void {
    $this->set('RoleId', $value);
  }

  /**
   * Create an entity object for a table with initial data.
   *
   * @param Table $table
   * @param array $data
   */
  public function __construct(Table $table, array $data, bool $inserted = true) {
    parent::__construct($table, $data, $inserted);
  }

}
