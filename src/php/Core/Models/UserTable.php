<?php namespace Core\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Row;
use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table base for all Users.
 * This class was auto-generated.
 */
class UserTable extends \Core\Data\Table {

  /**
   * Create the table for a context and with table information.
   *
   * @param DbContext $context
   * @param array $columns
   */
  public function __construct(DbContext $context, string $tableName, $textTableName, array $idColumnNames, array $columns) {
    parent::__construct($context, $tableName, $textTableName, $idColumnNames, $columns);
  }

  /**
   * Get a row by its data.
   * Exists to be available for the table base implementation.
   *
   * @param array $data
   * @return Row
   */
  protected function newRow(array $data): Row {
    $row = new User($this, $data, false);
    $this->selectedRows[$row->getPkHash()] = $row;
    return $row;
  }


}
