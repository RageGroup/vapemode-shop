<?php namespace Core\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Row;
use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table base for all Roles.
 * This class was auto-generated.
 */
class RoleTable extends \Core\Data\Table {

  /**
   * Create the table for a context and with table information.
   *
   * @param DbContext $context
   * @param array $columns
   */
  public function __construct(DbContext $context, array $columns) {
    parent::__construct($context, 'Roles', 'RoleTexts', ['RoleId'], $columns);
  }

  /**
   * Get a localizable string from the associated text table.
   *
   * @param Role $row The the row to which the localized text belongs.
   * @param string $columnName The name of the column to select.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string A localized string from the associated text table.
   */
  public function getLocalizedString(Role $row, string $columnName, ?string $languageCode): ?string {
    $conditions = [new SqlParameter('RoleId', '=', $row->getString('RoleId')),
                   ' and ',
                   new SqlParameter('LanguageCode', '=', $languageCode ?? $this->context->getLanguageCode())];
    $rowDataArray = $this->context->getDbConnection()->select($this->textTableName, $conditions, [$columnName]);
    foreach ($rowDataArray as $rowData) {
      return $rowData[$columnName];
    }
    return null;
  }

  /**
   * Get a row by its data.
   * Exists to be available for the table base implementation.
   *
   * @param array $data
   * @return Row
   */
  protected function newRow(array $data): Row {
    $row = new Role($this, $data, false);
    $this->selectedRows[$row->getPkHash()] = $row;
    return $row;
  }

  /**
  * Create a new Role.
  *
  * @return Role
  */
  public function create(
    string $roleName):
    Role
  {
    $roleId = Uuid::generate();
    $entity = new Role($this, [
      'RoleId' => $roleId->toString(),
      'RoleName' => $roleName
    ], false);
    $hash = $entity->getPkHash();
    $this->selectedRows[$hash] = $entity;
    $this->insertedRows[$hash] = $entity;
    return $entity;
  }

  /**
   * Get an entity object by its primary key value.
   *
   * @param Uuid $roleId
   * @return Role
   */
  public function getByPk(Uuid $roleId): ?Role {
    return $this->getFirstRow([new SqlParameter('RoleId', '=', $roleId)]);
  }


}
