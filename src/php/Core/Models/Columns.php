<?php namespace Core\Models;

use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

class Columns extends ColumnTable {

  public function __construct(DbContext $context, array $columns) {
    parent::__construct($context, $columns);
  }

}
