<?php namespace Core\Models;

use \Core\Data\Uuid;
use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

class UserRoles extends UserRoleTable {

  public function __construct(DbContext $context, array $columns) {
    parent::__construct($context, $columns);
  }

  /**
   * Get the roles of a user.
   *
   * @param Uuid $userId The ID of the user whose roles to get.
   * @return array The roles of a user.
   */
  public function getByUserId(Uuid $userId): array {
    return $this->getRows([new SqlParameter('UserId', '=', $userId)]);
  }

}
