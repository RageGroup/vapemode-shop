<?php namespace Core\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Table;

/**
 * A data table row for one Table Column.
 * This class was auto-generated.
 */
abstract class ColumnRow extends \Core\Data\Row {

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public static function pkHash(string $tableName,string $columnName): string {
    $hash = '';
    $hash = \md5($hash . ((string) $tableName), true);
    $hash = \md5($hash . ((string) $columnName), true);
    return $hash;
  }

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public function getPkHash(): string {
    return self::pkHash($this->getString('TableName'),
                        $this->getString('ColumnName'));
  }

  /**
   * Get the localized description of the Table Name.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getTableNameDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('TableName', $languageCode);
  }

  /**
   * Get the Table Name.
   *
   * @return string
   */
  public function getTableName(): ?string {
    return $this->getString('TableName');
  }

  /**
   * Set the Table Name.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setTableName(?string $value): void {
    $this->set('TableName', $value);
  }

  /**
   * Get the localized description of the Column Name.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getColumnNameDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('ColumnName', $languageCode);
  }

  /**
   * Get the Column Name.
   *
   * @return string
   */
  public function getColumnName(): ?string {
    return $this->getString('ColumnName');
  }

  /**
   * Set the Column Name.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setColumnName(?string $value): void {
    $this->set('ColumnName', $value);
  }

  /**
   * Get the localized description of the Column Description.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getColumnDescriptionDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('ColumnDescription', $languageCode, true);
  }

  /**
   * Get the Column Description.
   *
   * @return string
   */
  public function getColumnDescription(?string $languageCode = null): ?string {
    return $this->table->getLocalizedString($this, 'ColumnDescription', $languageCode);
  }

  /**
   * Set the Column Description.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setColumnDescription(?string $value, ?string $languageCode = null): void {
    $this->setLocalizedString('ColumnDescription', $languageCode, $value);
  }

  /**
   * Create an entity object for a table with initial data.
   *
   * @param Table $table
   * @param array $data
   */
  public function __construct(Table $table, array $data, bool $inserted = true) {
    parent::__construct($table, $data, $inserted);
  }

}
