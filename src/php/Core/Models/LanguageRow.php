<?php namespace Core\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Table;

/**
 * A data table row for one Language.
 * This class was auto-generated.
 */
abstract class LanguageRow extends \Core\Data\Row {

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public static function pkHash(string $languageCode): string {
    $hash = '';
    $hash = \md5($hash . ((string) $languageCode), true);
    return $hash;
  }

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public function getPkHash(): string {
    return self::pkHash($this->getString('LanguageCode'));
  }

  /**
   * Get the localized description of the Language Code (ISO 639-1).
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getLanguageCodeDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('LanguageCode', $languageCode);
  }

  /**
   * Get the Language Code (ISO 639-1).
   *
   * @return string
   */
  public function getLanguageCode(): ?string {
    return $this->getString('LanguageCode');
  }

  /**
   * Set the Language Code (ISO 639-1).
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setLanguageCode(?string $value): void {
    $this->set('LanguageCode', $value);
  }

  /**
   * Get the localized description of the Language Name.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getLanguageNameDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('LanguageName', $languageCode);
  }

  /**
   * Get the Language Name.
   *
   * @return string
   */
  public function getLanguageName(): ?string {
    return $this->getString('LanguageName');
  }

  /**
   * Set the Language Name.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setLanguageName(?string $value): void {
    $this->set('LanguageName', $value);
  }

  /**
   * Create an entity object for a table with initial data.
   *
   * @param Table $table
   * @param array $data
   */
  public function __construct(Table $table, array $data, bool $inserted = true) {
    parent::__construct($table, $data, $inserted);
  }

}
