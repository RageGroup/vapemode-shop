<?php namespace Core\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Table;

/**
 * A data table row for one Role.
 * This class was auto-generated.
 */
abstract class RoleRow extends \Core\Data\Row {

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public static function pkHash(Uuid $roleId): string {
    $hash = '';
    $hash = \md5($hash . ((string) $roleId), true);
    return $hash;
  }

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public function getPkHash(): string {
    return self::pkHash($this->getUuid('RoleId'));
  }

  /**
   * Get the localized description of the Role ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getRoleIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('RoleId', $languageCode);
  }

  /**
   * Get the Role ID.
   *
   * @return Uuid
   */
  public function getRoleId(): ?Uuid {
    return $this->getUuid('RoleId');
  }

  /**
   * Set the Role ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setRoleId(?Uuid $value): void {
    $this->set('RoleId', $value);
  }

  /**
   * Get the localized description of the Role Name.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getRoleNameDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('RoleName', $languageCode);
  }

  /**
   * Get the Role Name.
   *
   * @return string
   */
  public function getRoleName(): ?string {
    return $this->getString('RoleName');
  }

  /**
   * Set the Role Name.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setRoleName(?string $value): void {
    $this->set('RoleName', $value);
  }

  /**
   * Get the localized description of the Role Description.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getRoleDescriptionDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('RoleDescription', $languageCode, true);
  }

  /**
   * Get the Role Description.
   *
   * @return string
   */
  public function getRoleDescription(?string $languageCode = null): ?string {
    return $this->table->getLocalizedString($this, 'RoleDescription', $languageCode);
  }

  /**
   * Set the Role Description.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setRoleDescription(?string $value, ?string $languageCode = null): void {
    $this->setLocalizedString('RoleDescription', $languageCode, $value);
  }

  /**
   * Create an entity object for a table with initial data.
   *
   * @param Table $table
   * @param array $data
   */
  public function __construct(Table $table, array $data, bool $inserted = true) {
    parent::__construct($table, $data, $inserted);
  }

}
