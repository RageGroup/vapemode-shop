<?php namespace Core\Logging;

use \Core\Logging\Logger;

/**
 * Provides the ability to log messages by their type to a log file.
 */
final class FileLogger extends Logger {

  protected $filepath;

  public function __construct(string $baseDir) {
    parent::__construct();
    // The log file will be stored in "{$baseDir}../log/Error.log"
    $this->filepath = realpath($baseDir) . DIRECTORY_SEPARATOR . 'log' . DIRECTORY_SEPARATOR . 'Error.log';
    $this->file = fopen($this->filepath, 'a');
  }

  protected function log(string $type, ?string $message): void {
    $date = date('Y-m-d H:i:s');
    fwrite($this->file, $date . ' - ' . $type . ' - ' . ($message ?? '') . "\n");
  }

}
