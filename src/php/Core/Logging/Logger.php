<?php namespace Core\Logging;

/**
 * Provides the base for the ability to log messages by their type.
 * This implementation dismisses every message.
 * Derived classes may handle messages in another way.
 */
class Logger {

  public function __construct() {}

  /**
   * Log a message and its type.
   *
   * @param string $type The type of message to log.
   * @param string $message The message to log.
   * @return void
   */
  protected function log(string $type, ?string $message): void {}

  /**
   * Log an exception (throwable).
   *
   * @param \Throwable $x The Throwable to log.
   * @return void
   */
  public function logX(?\Throwable $x): void {
    if (get_class($x) == 'ErrorException') {
      $severity = $x->getSeverity();
    }
    else {
      $severity = E_ERROR;
    }
    switch ($severity) {
      case E_NOTICE:
      case E_USER_NOTICE:
      case E_STRICT:
        $type = self::NOTICE;
        break;
      case E_WARNING:
      case E_USER_WARNING:
        $type = self::WARNING;
        break;
      case E_ERROR:
      case E_USER_ERROR:
      case E_RECOVERABLE_ERROR:
      default:
        $type = self::ERROR;
    }
    $message = $x->getFile() . ' (Line ' . $x->getLine() . ') - ' . $x->getMessage();
    $this->log($type, $message);
  }

  public function logDebug(?string $message): void { $this->log(self::DEBUG, $message); }
  public function logNotice(?string $message): void { $this->log(self::NOTICE, $message); }
  public function logWarning(?string $message): void { $this->log(self::WARNING, $message); }
  public function logError(?string $message): void { $this->log(self::ERROR, $message); }

  protected const DEBUG = 'DEBUG  ';
  protected const NOTICE = 'NOTICE ';
  protected const WARNING = 'WARNING';
  protected const ERROR = 'ERROR  ';

  private static $instance;

  public static function get(): Logger {
    if (is_null(self::$instance)) {
      self::$instance = new Logger();
    }
    return self::$instance;
  }

  public static function set(Logger $value): void {
    self::$instance = $value;
  }

}
