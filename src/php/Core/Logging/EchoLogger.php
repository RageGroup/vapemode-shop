<?php namespace Core\Logging;

use \Core\Logging\Logger;

/**
 * Provides the ability to log messages by their type to the output.
 */
final class EchoLogger extends Logger {

  public function __construct() { parent::__construct(); }

  protected function log(string $type, ?string $message): void {
    echo $type . ' - ' . ($message ?? '') . '<br />';
  }

}
