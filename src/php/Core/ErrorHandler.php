<?php namespace Core;

/**
* A handler for all kinds of errors and warnings.
* 
* Handles all kinds of errors and warnings
* by creating and throwing exceptions.
*/
final class ErrorHandler {

  private $oldErrorHandler;

  public function __construct() {}

  /**
  * Start using this error handler.
  *
  * @return void
  */
  public function start() {
    $oldErrorHandler = set_error_handler([$this, 'handle'], E_ALL);
  }

  /**
  * Stop using this error handler.
  *
  * @return void
  */
  public function stop() {
    set_error_handler($oldErrorHandler);
  }

  /**
  * Handles PHP errors and warnings.
  *
  * @param integer $errno
  * @param string $errstr
  * @param string $errfile
  * @param integer $errline
  * @return boolean
  */
  public function handle(int $errno, string $errstr, string $errfile = __FILE__, int $errline = __LINE__): bool {
    if (!(error_reporting() & $errno)) {
      // This error code is not included in error_reporting, so let it fall
      // through to the standard PHP error handler
      // (error was suppressed with the @-operator)
      return false;
    }

    // Every problem has to be handled by exception
    // and *MAY NOT* show up in HTML code!
    // Therefore remember to catch possible exceptions!
    throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);

    return true;
  }

}
