<?php namespace Core;

/**
* Loads class files automatically by defined rules.
*/
final class AutoLoader {

  private $loadRules = [];
  private $loadedClassPaths = [];

  public function __construct(
    string $folder = null,
    string $classNamePrefix = '',
    string $classNameSuffix = '')
  {
    if (isset($folder)) {
      $this->addLoadRule($folder, $classNamePrefix, $classNameSuffix);
    }
  }

  /**
  * Get an array of all auto-loaded class paths.
  * @return array An array of all auto-loaded class paths.
  */
  public function getLoadedClassPaths(): array {
    return $this->loadedClassPaths;
  }

  /**
  * Define a rule to to load class files.
  * @param $folder Base path to search for files.
  * @param $classNamePrefix The class prefix, defaults to an empty string.
  * @param $classNameSuffix The class suffix, defaults to an empty string.
  */
  public function addLoadRule(string $folder, string $classNamePrefix = '', string $classNameSuffix = ''): void
  {
    $this->loadRules[] = [
      'classNamePrefix' => $classNamePrefix,
      'classNameSuffix' => $classNameSuffix,
      'folder' => $folder];
  }

  /**
  * Start the auto loader.
  */
  public function start(): void {
    spl_autoload_register([$this, 'loadClass']);
  }

  /**
  * Load a code object by its name.
  * @param string $codeObjName The name of the code object to load.
  */
  private function loadClass(string $codeObjName): void {
    $firstLetter = substr($codeObjName, 0, 1);
    // Class names are expected to start with a upper case letter,
    // and function names are expected to start with a lower case letter.
    if ($firstLetter == strtoupper($firstLetter)) {
      // It's a class, not a function.
      foreach ($this->loadRules as $loadRule) {
        $path = $loadRule['folder'];
        $path .= DIRECTORY_SEPARATOR;
        $path .= $loadRule['classNamePrefix'];
        $path .= str_replace('\\', '/', $codeObjName);
        $path .= $loadRule['classNameSuffix'];
        $path .= '.php';
        if (file_exists($path)) {
          require_once $path;
        }
      }
    }
    else {
      // It's not a class, but a function.
      // This funcion is expexted to reside in a file named 'Util.php'.
      foreach ($this->loadRules as $loadRule) {
        $path = $loadRule['folder'].DIRECTORY_SEPARATOR.'Util.php';
        if (file_exists($path)) {
          require_once $path;
        }
      }
    }
  }

}
