<?php namespace Core;

class AppException extends \Exception {

  private static $oldErrorHandler;

  protected $severity;

  final public function getSeverity(): int { return $this->severity; }

  public function __construct (
    string $message = '',
    int $code = 0,
    int $severity = E_ERROR,
    string $filename = __FILE__,
    int $lineno = __LINE__,
    Exception $previous = NULL)
  {
    $this->severity = $severity;
  }

  public static function useForErrorHandling() {
    $oldErrorHandler = set_error_handler(array(AppException::class, 'handle'));
  }

  public static function handle(int $errno, string $errstr, string $errfile, int $errline): bool {
    if (!(error_reporting() & $errno)) {
      // This error code is not included in error_reporting, so let it fall
      // through to the standard PHP error handler
      // (error was suppressed with the @-operator)
      return false;
    }

    // Every problem has to be handled by exception
    // and *MAY NOT* show up in HTML code!
    throw new AppException($errstr, 0, $errno, $errfile, $errline);

    return true;
  }

}
