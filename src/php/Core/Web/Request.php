<?php namespace Core\Web;

/**
* A HTTP request.
*/
final class Request {

  public const GET = 'GET';
  public const POST = 'POST';

  /**
  * Get the domain of this request (and thus server).
  *
  * @return string
  */
  public static function getDomain(): string { return $_SERVER['HTTP_HOST']; }

  /**
   * Get the base URI of this request (without get parameters).
   *
   * @return string
   */
  public static function getBaseUri(): string {
    $uri = $_SERVER['REQUEST_URI'] ?? '';
    $i = strpos($uri, '?');
    if ($i > 0) {
      $uri = substr($uri, 0, $i);
    }
    return $uri;
  }

  /**
   * Get the URI of this request.
   *
   * @return string
   */
  public static function getUri(): string {
    return $_SERVER['REQUEST_URI'] ?? '';
  }

  // /**
  //  * Get the full URL of this request.
  //  *
  //  * @return string
  //  */
  // public static function getUrl() {
  //   $urlParts = \parse_url(
  //     (isset($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'off') ? 'https://' : 'http://') .
  //     (isset($_SERVER['HTTP_HOST']) ?
  //      $_SERVER['HTTP_HOST'] :
  //      (isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '')) . self::getUri()
  //   );
  //   $urlParts['port'] = $_SERVER['SERVER_PORT'];
  //   return \http_build_url('', $urlParts);
  // }

  /**
  * Get the route path for this request.
  *
  * @return string
  */
  public static function getRoute(): string { return htmlspecialchars($_GET['r'] ?? ''); }

  /**
  * Get the HTTP method of this request.
  *
  * @return string
  */
  public static function getMethod(): string { return $_SERVER['REQUEST_METHOD']; }

  /**
  * Get the URL of this request (without get parameters).
  *
  * @return string
  */
  public static function getBaseUrl(): string { return Request::getDomain() . Request::getRoute(); }

  /**
  * Is this GET request?
  *
  * @return boolean
  */
  public static function isGet(): bool { return Request::getMethod() === self::GET; }

  /**
  * Is this POST request?
  *
  * @return boolean
  */
  public static function isPost(): bool { return Request::getMethod() === self::POST; }

  /**
  * Get the language code of this request, and thus browser (ISO 639-1).
  *
  * @return string The language code of this request, and thus browser (ISO 639-1).
  */
  public static function getLanguageCode(): string {
    return substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
  }

}
