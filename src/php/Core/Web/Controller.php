<?php namespace Core\Web;

use \Core\Logging\Logger;
use \Core\Data\DbContext;
use \Core\Models\User;
use \Core\Web\Request;
use \Core\Web\Router;
use \Core\Web\Response;
use \Core\Web\Cookie;

/**
* The base implementation for Controllers.
*/
abstract class Controller {

  protected $route;
  protected $user;
  protected $context;
  protected $cookies;

  public function hasUser(): bool { return !is_null($this->user); }
  public function getUser(): ?User { return $this->user; }
  public function setUser(?User $value): void { $this->user = $value; }
  public function getContext(): DbContext { return $this->context; }
  public function getCookies(): array { return $this->cookies; }

  public function __construct(Route $route, DbContext $context) {
    $this->route = $route;
    $this->user = null;
    $this->context = $context;
    $this->cookies = [];
  }

  protected function getRouter(): Router { return $this->route->getRouter(); }
  protected function getLogger(): Logger { return $this->route->getRouter()->getLogger(); }

  /**
   * Get a localized text.
   *
   * @param string $textId The text ID of the localized text to get.
   * @return string A localized text.
   */
  public function getLocalizedText(string $textId): ?string {
    return $this->route->getRouter()->getLocalizedText($textId);
  }

  protected function setCookie(string $name, ?string $value = null, int $duration = 0): void {
    $this->cookies[$name] = new Cookie($name, $value, $duration);
  }

  protected function unsetCookie(string $name): void {
    $this->cookies[$name] = new Cookie($name);
  }

  private function setCookies(): void {
    $baseRoute = $this->getRouter()->getBaseRoute();
    foreach ($this->cookies as $cookie) {
      $cookie->set($baseRoute);
    }
  }

  /**
   * Write an associative array as JSON code into a HTTP response.
   *
   * @param array $model
   * @param integer $statusCode
   * @return Response
   */
  protected function json(array $model, int $statusCode = Response::CODE_OK): Response {
    $content = \json_encode($model);
    return Response::create($statusCode, $content, Response::JSON_CONTENT_TYPE);
  }

  /**
   * Use a view to write HTML code into a HTTP response.
   *
   * @param string $viewName
   * @param array $model
   * @param integer $statusCode
   * @return Response
   */
  protected function view(string $viewName, array $model = [], int $statusCode = Response::CODE_OK): Response {
    $router = $this->getRouter();
    $path = str_replace('\\', '/', get_class($this));
    $path = str_replace($router->getControllerPrefix(), $router->getViewPrefix(), $path);
    $path = str_replace('Controller', '', $path);
    $viewNameParts = explode('::', $viewName);
    $path .= DIRECTORY_SEPARATOR . $viewNameParts[count($viewNameParts) - 1] . '.php';
    $this->setCookies();
    return Html::view($this->getRouter(), $this, $statusCode, $path, $model);
  }

  /**
   * Redirect to the given url.
   *
   * @param string $url
   * @param boolean $permanent
   * @return Response
   */
  public function redirectUrl(string $url = '', bool $permanent = false): Response {
    return Response::redirect($url, $permanent);
  }

  /**
   * Redirect to the given route.
   *
   * @param string $route
   * @param boolean $permanent
   * @return Response
   */
  public function redirect(string $route = '', bool $permanent = false): Response {
    $this->setCookies();
    $url = Request::getBaseUri();
    if (strlen($route) > 0) {
      $url .= '?r=' . urlencode($route);
    }
    return Response::redirect($url, $permanent);
  }

  /**
   * Deliver a response that informs the user that the requested site could not be found.
   *
   * @return Response
   */
  public function notFound(): Response { return $this->getRouter()->notFound(); }

  /**
   * Deliver a response that informs the user that an errror occurred.
   *
   * @return Response
   */
  public function error(): Response { return $this->getRouter()->error(); }

}
