<?php namespace Core\Web;

/**
* A cookie that is to be set in a response.
*/
final class Cookie {

  private $name;
  private $value;
  private $duration;
  private $path;

  public function getName(): string { return $this->name; }
  public function getDuration(): int { return $this->duration; }

  public function __construct(string $name, ?string $value = null, int $duration = 0, string $path = '') {
    $this->name = $name;
    $this->value = $value;
    $this->duration = $duration;
    $this->path = $path;
  }

  public function getValue(): ?string {
    if (is_null($this->value) || strlen($this->path) == 0) {
      return null;
    }
    return $this->value;
  }

  private function getPath(): string {
    if (strlen($this->path) > 0) {
      return $this->path;
    }
    return '';
  }

  private function getExpire(): int {
    if ($this->duration > 0 && !is_null($this->value) && strlen($this->value) > 0) {
      return time() + $this->duration;
    }
    return -1;
  }

  public function set(string $basePath = '', string $domain = ''): void {
    \setcookie($this->name, $this->getValue(), $this->getExpire(), $basePath . $this->getPath(), $domain);
  }

}
