<?php namespace Core\Web;

use \Core\Logging\Logger;
use \Core\Data\Column;
use \Core\Models\User;
use \Core\Web\Router;
use \Core\Web\Request;
use \Core\Web\Response;

/**
 * A class to handle HTML code generation.
 */
final class Html {

  private $router;
  private $controller;
  private $user;
  private $baseRoute;
  private $assetBaseRoute;
  private $stylePrefix;
  private $scriptPrefix;
  private $imagePrefix;
  private $styles;
  private $title;

  private function __construct(Router $router, ?Controller $controller) {
    $this->router = $router;
    $this->controller = $controller;
    if (is_null($controller)) {
      $this->user = null;
    }
    else {
      $this->user = $controller->getUser();
    }
    $this->baseRoute = Request::getBaseUri();
    $this->assetBaseRoute = str_replace('src/php/', '', $this->baseRoute);
    $this->stylePrefix = $router->getStylePrefix();
    $this->scriptPrefix = $router->getScriptPrefix();
    $this->imagePrefix = $router->getImagePrefix();
    $this->styles = [];
    $this->title = '';
  }

  /**
   * Get a localized text.
   *
   * @param string $textId The text ID of the localized text to get.
   * @return string A localized text.
   */
  public function getLocalizedText(string $textId): ?string {
    return $this->router->getLocalizedText($textId);
  }

  public function hasUser(): bool {
    return !is_null($this->user);
  }

  public function getUser(): ?User {
    return $this->user;
  }

  public function addTitlePart(string $titlePart): void {
    $this->title = $titlePart . ' - ' . $this->title;
  }

  public function addLocalizedTitlePart(string $titlePartId): void {
    $this->addTitlePart($this->getLocalizedText($titlePartId));
  }

  public function actionUrl(string $actionPath = '', array $params = []): string {
    $getParamsStarted = false;
    $url = $this->baseRoute;
    if (strlen($actionPath) > 0) {
      $url .= '?r=' . urlencode($actionPath);
      $getParamsStarted = true;
    }
    foreach ($params as $paramKey => $paramValue) {
      if ($getParamsStarted) {
        $url .= '&';
      }
      else {
        $url .= '?';
        $getParamsStarted = true;
      }
      $url .= urlencode($paramKey) . '=' . urlencode($paramValue);
    }
    return $url;
  }

  public function actionLink(
    string $description,
    string $actionPath = '',
    array $params = [],
    string $class = ''):
    string
  {
    return '<a ' . (strlen($class) > 0 ? 'class="' . $class . '"' : '') .
      'href="' . $this->actionUrl($actionPath, $params) . '">' . $description . '</a>';
  }

  public function localizedActionLink(
    string $textId,
    string $actionPath = '',
    array $params = [],
    string $class = ''):
    string
  {
    return $this->actionLink($this->getLocalizedText($textId), $actionPath, $params, $class);
  }

  public function button(string $class, string $description, string $type = 'button'): string {
    return '<button class="' . $class . '" type="' . $type . '">' . $description . '</button>';
  }

  public function addStyle(string $style): void {
    $this->styles[] = $style;
  }

  protected function style(string $style): string {
    if (strpos($style, 'http') !== false) {
      return '<link rel="stylesheet" type="text/css" href="' . $style . '" crossorigin="anonymous" />';
    }
    return '<link rel="stylesheet" type="text/css" href="' . $this->assetBaseRoute . $this->stylePrefix . $style . '" />';
  }

  public function script(string $script): string {
    return '<script type="text/javascript" src="' . $this->assetBaseRoute . $this->scriptPrefix . $script . '"></script>';
  }

  public function label(string $class, string $for = '', string $description = ''): string {
      return '<label' .
        (strlen($for) > 0 ? ' for="' . $for . '"' : '') .
        ' class="' . $class . '">' .
        $description .
        '</label>';
  }

  /**
   * Get code for a token (hidden input).
   *
   * @param string $name
   * @param string $value
   * @return string
   */
  public function token(string $name, string $value = ''): string {
    return '<input type="hidden" name="' . $name . '" id="' . $name . '" value="' . $value . '" />';
  }

  /**
   * Get code for a number input.
   *
   * @param string $name
   * @param string $value
   * @param string $min
   * @param string $max
   * @return string
   */
  public function numberInput(
    string $class,
    string $name,
    string $value = '',
    string $step = '',
    string $min = '',
    string $max = ''):
    string
  {
    return '<input class="' . $class . '" ' .
      'type="number" name="' . $name . '" id="' . $name . '" value="' . $value . '"' .
      (strlen($step) > 0 ? ' step="' . $step . '"' : '') .
      (strlen($min) > 0 ? ' min="' . $min . '"' : '') .
      (strlen($max) > 0 ? ' max="' . $max . '"' : '') .
      '/>';
  }

  public function input(
    string $class,
    string $type,
    string $name,
    string $placeholder,
    bool $required = false,
    string $pattern = '',
    string $value = '',
    bool $autofocus = true):
    string
  {
      if (stripos($name, 'password') === false && strlen($value) === 0) {
        // Prefill the form input with already given data.
        $value = htmlspecialchars($_POST[$name] ?? '');
      }
      return '<input class="' . $class .
        '" type="' . $type .
        '" name="' . $name .
        '" id="' . $name .
        '"' . (strlen($value) > 0 ? ' value="' . $value . '"' : ' ' . ($autofocus ? 'autofocus' : '')) .
        (strlen($placeholder) > 0 ? ' placeholder="' . $placeholder . '"' : '') .
        ($required ? ' required' : '') .
        (strlen($pattern) > 0 ? ' pattern="' . $pattern . '"' : '') .
        '" />';
  }

  public function localizedInput(
    string $class,
    string $type,
    string $name,
    string $placeholderTextId,
    bool $required = false,
    string $pattern = '',
    string $value = '',
    bool $autofocus = true):
    string
  {
      return $this->input($class, $type, $name,
                          $this->getLocalizedText($placeholderTextId),
                          $required, $pattern, $value, $autofocus);
  }

  public function formGroupWithInput(
    string $groupClass,
    string $labelClass,
    string $contentClass,
    string $inputClass,
    string $type,
    string $name,
    string $placeholder,
    bool $required = false,
    string $pattern = '',
    string $value = '',
    bool $autofocus = true):
    string
  {
    return '<div class="' . $groupClass . '">' .
      $this->label($labelClass, $name, $placeholder) .
      '<div class="' . $contentClass . '">' .
      $this->input($inputClass, $type, $name, $placeholder, $required, $pattern, $value, $autofocus) .
      '</div></div>';
  }

  public function formGroupMessage(string $class, string $message): string {
    return '<div class="' . $class . '">' . $message . '</div>';
  }

  public function image(string $path, string $description = '', string $class = ''): string {
    return '<img class="' . $class . '" src="' . $this->assetBaseRoute . $this->imagePrefix . $path . '" alt="' . $description . '">';
  }

  public function figure(string $path, string $caption = ''): string {
    return '<figure><img src="' . $this->assetBaseRoute . $this->imagePrefix . $path .
      '" alt="' . $caption .
      '"><figcaption>' . $caption .
      '</figcaption></figure>';
  }

  protected function include(string $filename, array $model = []): void {
    $html = $this;
    $path = $this->router->getViewPrefix() . $filename . '.php';
    if (file_exists($path)) {
      include $path;
    }
  }

  /**
   * Use a view to render HTML code into a HTTP response.
   *
   * @param Router $router
   * @param Controller $controller
   * @param integer $statusCode
   * @param string $path
   * @param object|null $model
   * @return Response
   */
  public static function view(
    Router $router,
    ?Controller $controller,
    int $statusCode,
    string $path,
    array $model = []):
    Response
  {
    $html = new Html($router, $controller);
    if (file_exists($path)) {
      try {
        ob_start();

        include $router->getHeaderViewPartPath();
        include $path;
        include $router->getFooterViewPartPath();

        $output = ob_get_contents();

        $content = '<!DOCTYPE html><html lang="de"';
        if (strlen($router->getHtmlTagClass()) > 0) {
          $content .= ' class="' . $router->getHtmlTagClass() . '"';
        }
        $content .= '><head>';
        if (strlen($html->title) > 0) {
          $content .= '<title>' . $html->title . '</title>';
        }
        $content .= '<meta charset="UTF-8" />';
        $content .= '<meta name="viewport" content="width=device-width, initial-scale=1.0" />';
        foreach ($html->styles as $style) {
          $content .= $html->style($style);
        }
        $content .= '</head><body>';
        $content .= $output;
        $content .= '</body></html>';

        $response = Response::create($statusCode, $content);
        ob_end_clean();
        return $response;
      }
      catch (\Throwable $x) {
        // Catch all throwables and exceptions, cleanup and rethrow.
        ob_end_clean();
        throw $x;
      }
    }
    else {
      $router->getLogger()->logError('View not found: "' . $path . '"');
      return $router->error();
    }
  }

}
