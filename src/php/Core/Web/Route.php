<?php namespace Core\Web;

use \Core\Web\Router;
use \Core\Web\Request;

/**
* A HTTP request route.
*/
final class Route {

  /**
  * The regex patterns for the parameter data types.
  *
  * @var array
  */
  private static $regexPatterns = [
    'int' => '\d+',
    'float' => '\d+\.\d+',
    'string' => '\w+'
  ];

  private $router;
  private $path;
  private $pattern;
  private $roleNames;
  private $controllerName;
  private $httpGetActionName;
  private $httpPostActionName;

  public function getRouter(): Router { return $this->router; }
  public function getPath(): string { return $this->path; }
  public function getPattern(): string { return $this->pattern; }
  public function getControllerName(): string { return $this->controllerName; }
  public function getRoleNames(): array { return $this->roleNames; }

  private function __construct(Router $router, string $route, array $info) {
    $this->router = $router;
    $this->path = $route;
    $this->pattern = $route;
    // G => guest, C => customer, A => admin
    $this->roleNames = Route::getArrayValueAsArray($info, 'roles', ['G', 'C', 'A']);
    $this->controllerName = $info['controller'];
    $this->httpGetActionName = Route::getArrayValue($info, 'getAction');
    $this->httpPostActionName = Route::getArrayValue($info, 'postAction');
    $this->params = [];

    if (!in_array('A', $this->roleNames)) {
      $this->roleNames[] = 'A';
    }

    if (isset($info['params'])) {
      foreach ($info['params'] as $name => $type) {
        $this->pattern = str_replace(':' . $name, self::$regexPatterns[$type], $this->pattern);
        $this->params[$name] = $type;
      }
    }

    $this->pattern = "@^{$this->pattern}$@";
  }

  public function getActionName(string $httpMethod): string {
    if ($httpMethod == Request::GET) {
      return $this->httpGetActionName;
    }
    elseif ($httpMethod == Request::POST) {
      return $this->httpPostActionName;
    }
    return '';
  }

  /**
  * Extract arguments from the request for this route.
  *
  * @return array
  */
  public function extractArgs(): array {
    $args = [];
    $path = Request::getRoute();
    $pathParts = explode('/', $path);
    $routeParts = explode('/', $this->path);
    foreach ($routeParts as $key => $routePart) {
      if (strpos($routePart, ':') === 0) {
        $name = substr($routePart, 1);
        $args[$name] = $pathParts[$key];
        switch ($this->params[$name]) {
          case 'int':
            $args[$name] = (int) $args[$name];
            break;
          case 'float':
            $args[$name] = (float) $args[$name];
            break;
          case 'string':
            $args[$name] = (string) $args[$name];
            break;
          default:
            break;
        }
      }
    }
    return $args;
  }

  /**
  * Get all routes for a router and its config.
  *
  * @param Router $router
  * @param array $data
  * @return array
  */
  public static function getRoutes(Router $router, array $data): array {
    $routes = [];
    foreach ($data as $route => $info) {
      $routes[] = new Route($router, $route, $info);
    }
    return $routes;
  }

  private static function getArrayValue(array &$array, string $key, string $default = ''): string {
    if (isset($array[$key])) {
      return $array[$key];
    }
    return $default;
  }

  private static function getArrayValueAsArray(array $array, string $key, array $defaults = []): array {
    $result = [];
    if (isset($array[$key])) {
      $values = $array[$key];
      if (gettype($values) === 'array') {
        foreach ($values as $value) {
          $result[] = strtoupper(substr($value, 0, 1));
        }
      }
      else {
        $result[] = strtoupper(substr($values, 0, 1));
      }
    }
    else {
      foreach ($defaults as $default) {
        $result[] = $default;
      }
    }
    return $result;
  }

}
