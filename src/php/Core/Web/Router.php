<?php namespace Core\Web;

use \Core\IO;
use \Core\Data\Uuid;
use \Core\Data\Localization;
use \Core\Web\Route;
use \Core\Web\Request;
use \Core\Web\Response;
use \Core\Web\Controller;
use \Core\Logging\Logger;
use \Core\Logging\EchoLogger;
use \Core\Logging\FileLogger;

/**
* A router for processing HTTP requests to provide a matching response.
*/
final class Router {

  /**
  * The base directory of the app.
  *
  * @var string
  */
  private $baseDir;

  /**
  * Get the base directory of the app.
  *
  * @return string
  */
  public function getBaseDir(): string { return $this->baseDir; }

  /**
  * The base route for the app url.
  *
  * @var string
  */
  private $baseRoute;

  /**
  * Get the base route for the app url.
  *
  * @return string
  */
  public function getBaseRoute(): string { return $this->baseRoute; }

  /**
  * The view to use as header in views.
  *
  * @var string
  */
  private $headerViewPart;

  /**
  * Get the view to use as header in views.
  *
  * @return string
  */
  public function getHeaderViewPartPath(): string {
    return $this->viewPrefix . $this->headerViewPart . '.php';
  }

  /**
  * The view to use as footer in views.
  *
  * @var string
  */
  private $footerViewPart;

  /**
  * Get the view to use as footer in views.
  *
  * @return string
  */
  public function getFooterViewPartPath(): string {
    return $this->viewPrefix . $this->footerViewPart . '.php';
  }

  /**
  * The view to use when requested page couldn't be found.
  *
  * @var string
  */
  private $notFoundView;

  /**
  * Get the view to use when requested page couldn't be found.
  *
  * @return string
  */
  public function getNotFoundView(): string { return $this->notFoundView; }

  /**
  * The view to use when requested page couldn't be found.
  *
  * @var string
  */
  private $errorView;

  /**
  * Get the view to use when requested page couldn't be found.
  *
  * @return string
  */
  public function getErrorView(): string { return $this->errorView; }

  /**
  * The relative directory for controllers.
  *
  * @var string
  */
  private $controllerPrefix;

  /**
  * Get the relative directory for controllers.
  *
  * @return string
  */
  public function getControllerPrefix(): string { return $this->controllerPrefix; }

  /**
  * The relative directory for views.
  *
  * @var string
  */
  private $viewPrefix;

  /**
  * Get the relative directory for views.
  *
  * @return string
  */
  public function getViewPrefix(): string { return $this->viewPrefix; }

  /**
  * The relative route for stylesheets.
  *
  * @var string
  */
  private $stylePrefix;

  /**
  * Get the relative route for stylesheets.
  *
  * @return string
  */
  public function getStylePrefix(): string { return $this->stylePrefix; }

  /**
  * The relative route for javascripts.
  *
  * @var string
  */
  private $scriptPrefix;

  /**
  * Get the relative route for javascripts.
  *
  * @return string
  */
  public function getScriptPrefix(): string { return $this->scriptPrefix; }

  /**
  * The relative route for images.
  *
  * @var string
  */
  private $imagePrefix;

  /**
  * Get the relative route for images.
  *
  * @return string
  */
  public function getImagePrefix(): string { return $this->imagePrefix; }

  /**
   * The localization to use.
   *
   * @var Localization
   */
  protected $localization;

  /**
   * Get the localization to use.
   *
   * @var Localization
   */
  public function getLocalization(): Localization { return $this->localization; }

  /**
   * The ID of the language to use.
   *
   * @var string
   */
  protected $languageCode;

  /**
   * Get the code of the language to use (ISO 639-1).
   *
   * @var string The code of the language to use (ISO 639-1).
   */
  public function getLanguageCode(): string { return $this->languageCode; }

  /**
  * The logger to use.
  *
  * @var Logger
  */
  private $logger;

  /**
  * Get the logger to use.
  *
  * @return Logger
  */
  public function getLogger(): Logger { return $this->logger; }

  /**
  * The CSS class to apply to the HTML tag.
  *
  * @var string
  */
  private $htmlTagClass;

  /**
  * Get the CSS class to apply to the HTML tag.
  *
  * @return string
  */
  public function getHtmlTagClass(): string { return $this->htmlTagClass; }

  /**
  * The available routes of the app.
  *
  * @var string
  */
  private $routes;

  /**
  * Create a router with the base directory of the app.
  * 
  * @param string $baseDir
  */
  public function __construct(string $baseDir) {
    $routerConfig = IO::getJsonData($baseDir, '../../config/router-config.json');
    $this->baseDir = $baseDir;
    $this->baseRoute = Request::getRoute();
    $this->headerViewPart = $routerConfig['headerViewPart'];
    $this->footerViewPart = $routerConfig['footerViewPart'];
    $this->notFoundView = $routerConfig['notFoundView'];
    $this->errorView = $routerConfig['errorView'];
    $this->controllerPrefix = str_replace('\\', '/', $routerConfig['controllerPrefix']);
    $this->viewPrefix = str_replace('\\', '/', $routerConfig['viewPrefix']);
    $this->stylePrefix = $routerConfig['stylePrefix'];
    $this->scriptPrefix = $routerConfig['scriptPrefix'];
    $this->imagePrefix = $routerConfig['imagePrefix'];
    $this->htmlTagClass = $routerConfig['htmlTagClass'] ?? '';
    $this->routes = Route::getRoutes($this, $routerConfig['routes']);
    $this->logType = $routerConfig['logType'];
    $this->localization = new Localization($baseDir);
    // /*
    $languageCode = $_SESSION['LangId'] ?? Request::getLanguageCode();
    $this->languageCode = (in_array($languageCode,
                                  $routerConfig['languageCodes'] ?? ['en', 'de']) ?
                         $languageCode : 'en');
    //*/$this->languageCode = 'en';
    $this->init();
  }

  private function init() {
    if (isset($this->logType)) {
      switch ($this->logType) {
        case 'echo':
          $this->logger = new EchoLogger();
          break;
        case 'file':
          $this->logger = new FileLogger($this->baseDir . '/../..');
          break;
        default:
          $this->logger = new Logger();
      }
    }
    else {
      $this->logger = new Logger();
    }
    Logger::set($this->logger);
    if (isset($_SESSION['LangId'])) {
      $this->languageCode = $_SESSION['LangId'];
    }
  }

  public static function getDefault(string $baseDir, bool $useCaching = true): Router {
    session_start();
    if (!$useCaching || !isset($_SESSION['Router'])) {
      $router = new Router($baseDir);
      $_SESSION['Router'] = $router;
    }
    else {
      $router = $_SESSION['Router'];
      $router->init();
    }
    return $router;
  }

  /**
  * Route a HTTP request to take the action that creates the matching response.
  *
  * @return string
  */
  public function route(): Response {
    $path = Request::getRoute();
    foreach ($this->routes as $route) {
      if (strcmp($path, $route->getPath()) == 0 || preg_match($route->getPattern(), $path) > 0) {
        return $this->executeControllerAction($route);
      }
    }
    $this->logger->logError('Unable to find a route: \'' . $path . '\'');
    return $this->notFound();
  }

  /**
  * Execute a controller action.
  *
  * @param Route $route The route matching the HTTP-Request.
  * @return string
  */
  private function executeControllerAction(Route $route): Response {
    try {
      $controllerName = $this->controllerPrefix . $route->getControllerName() . 'Controller';
      $controllerName = str_replace('/', '\\', $controllerName);
      $class = new \ReflectionClass($controllerName);
      try {
        $controller = $class->newInstanceArgs([$route]);
        $context = $controller->getContext();
        $context->setLanguageCode($this->languageCode);
        if (isset($_SESSION['UserId'])) {
          $userId = Uuid::createFromBin($_SESSION['UserId']);
          $user = $context->getUserTable()->getByPk($userId);
          $controller->setUser($user);
          if (!is_null($user)) {
            $userRole = $user->getRole();
          }
          else {
            $userRole = 'G';
          }
        }
        else {
          $userRole = 'G';
        }
        $allowance = in_array($userRole, $route->getRoleNames());
        $method = $route->getActionName(Request::getMethod());
        if (!$class->hasMethod($method) || !$allowance) {
          $response = $this->redirect();
        }
        else {
          $args = $route->extractArgs();
          $response = \call_user_func_array([$controller, $method], $args);
        }
      }
      catch (\Throwable $x) {
        // Catch all throwables and exceptions. Errors and warnings
        // should be translated into exceptions to catch them here.
        $this->logger->logX($x);
        $response = $this->error();
      }
    }
    catch (\Throwable $x) {
      $response = $this->notFound();
    }
    return $response;
  }

  /**
  * Redirect to the given path.
  *
  * @param string $path
  * @param boolean $permanent
  * @return Response
  */
  public function redirect(string $path = '', bool $permanent = false): Response {
    $url = Request::getBaseUri();
    if (strlen($path) > 0) {
      $url .= '?r=' . urlencode($path);
    }
    return Response::redirect($url, $permanent);
  }

  /**
  * Use a view to render HTML code into a HTTP response.
  *
  * @param integer $statusCode
  * @param string $viewName
  * @param object|null $model
  * @return Response
  */
  protected function view(int $statusCode, string $viewName, array $model = []): Response {
    $path = $this->viewPrefix . $viewName . '.php';
    return Html::view($this, null, $statusCode, $path, $model);
  }

  /**
  * Deliver a response that informs the user that the requested site could not be found.
  *
  * @return Response
  */
  public function notFound(): Response {
    return $this->view(Response::CODE_NOT_FOUND, $this->notFoundView);
  }

  /**
  * Deliver a response that informs the user that an errror occurred.
  *
  * @return Response
  */
  public function error(): Response {
    return $this->view(Response::CODE_INTERNAL_SERVER_ERROR, $this->errorView);
  }

  /**
   * Get a localized text.
   *
   * @param string $textId The text ID of the localized text to get.
   * @return string A localized text.
   */
  public function getLocalizedText(string $textId): ?string {
    return $this->localization->getText($textId, $this->languageCode);
  }

}
