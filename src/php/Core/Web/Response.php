<?php namespace Core\Web;

/**
* A HTTP response.
*/
final class Response {

  public const CODE_OK = 200;
  public const CODE_MULTIPLE_CHOICES = 300;
  public const CODE_SEE_OTHER = 303;
  public const CODE_BAD_REQUEST = 400;
  public const CODE_UNAUTHORIZED = 401;
  public const CODE_FORBIDDEN = 403;
  public const CODE_NOT_FOUND = 404;
  public const CODE_METHOD_NOT_ALLOWED = 405;
  public const CODE_INTERNAL_SERVER_ERROR = 500;

  public const HTML_CONTENT_TYPE = 'text/html; charset=utf-8';
  public const JSON_CONTENT_TYPE = 'application/json; charset=utf-8';
  public const DEFAULT_CONTENT_TYPE = self::HTML_CONTENT_TYPE;

  private $statusCode;
  private $headers;
  private $content;
  private $caching;

  private function __construct(int $statusCode = Response::CODE_OK, array $headers = [], string $content = '') {
    $this->statusCode = $statusCode;
    $this->headers = $headers;
    $this->caching = false;
    $this->content = $content;
  }

  public static function create(
    int $statusCode,
    string $content,
    string $contentType = Response::DEFAULT_CONTENT_TYPE,
    bool $caching = false):
    Response
  {
    $response = new Response(Response::CODE_OK, ['Content-Type: ' => $contentType], $content);
    // TODO: Check caching.
    if ($statusCode !== Response::CODE_OK) {
      $response->useCaching($caching);
    }
    return $response;
  }

  public static function ok(
    string $content,
    string $contentType = Response::DEFAULT_CONTENT_TYPE,
    bool $caching = false):
    Response
  {
    return view(CODE_OK, $content, $contentType, $caching);
  }

  public static function redirect(string $url, bool $permanent = false): Response {
    return new Response(Response::CODE_SEE_OTHER, [ 'Location' => $url ]);
  }

  public static function notFound(string $content, string $contentType = Response::DEFAULT_CONTENT_TYPE): Response {
    return new Response(Response::CODE_NOT_FOUND, [ 'Content-Type: ' => $contentType ], $content);
  }

  public static function error(string $content, string $contentType = Response::DEFAULT_CONTENT_TYPE): Response {
    return new Response(Response::CODE_INTERNAL_SERVER_ERROR, [ 'Content-Type: ' => $contentType ], $content);
  }

  private function setStatusCode(int $value): void { $this->statusCode = $value; }
  public function getStatusCode(): int { return $this->statusCode; }

  public function setHeader(string $key, string $value): void { $this->headers[$key] = $value; }
  public function unsetHeader(string $key): void { unset($this->headers[$key]); }
  public function getHeader(string $key): string { return $this->headers[$key]; }

  public function setContent(string $value): void { $this->content = $value; }

  public function hasCaching(): bool { return $this->caching; }
  public function useCaching(bool $value): void {
    $this->caching = $value;
    if ($value) {
      $this->headers['Cache-Control'] = 'max-age=3600';
      unset($this->headers['Expires']);
    }
    else {
      $this->headers['Cache-Control'] = 'no-cache, must-revalidate';
      $this->headers['Expires'] = 'Thu, 9 Nov 1989 18:57:00 GMT';
    }
  }

  /**
  * Render the HTTP request.
  *
  * @return void
  */
  public function render(): void {
    // Set the HTTP response code.
    http_response_code($this->statusCode);
    // Set the HTTP headers.
    foreach ($this->headers as $key => $values) {
      if (is_array($values)) {
        foreach ($values as $value) {
          header($key . ': ' . $value);
        }
      }
      else {
        header($key . ': ' . $values);
      }
    }
    if ($this->statusCode >= Response::CODE_MULTIPLE_CHOICES &&
        $this->statusCode < Response::CODE_BAD_REQUEST)
    {
      // When redirecting, no output is allowed and termination must follow.
      exit();
    }
    else {
      echo $this->content;
    }
  }

}
