<?php namespace Core;

/**
* IO.
*/
final class IO {

  private function __construct() {}

  /**
  * Get json file data.
  * @return array The json file data.
  */
  public static function getJsonData(string $filepath, string $filename = null): array {
    if (isset($filename)) {
      $filepath .= DIRECTORY_SEPARATOR . $filename;
    }
    $json = \file_get_contents($filepath);
    return \json_decode($json, true);
  }

}
