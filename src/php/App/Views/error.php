<?php
$html->addTitlePart('Fehler');
?>
<main class="vm-main">
  <p>
    Upps! Leider trat ein Fehler auf.
    <br />
    <br />
    <a href="<?=$html->actionUrl()?>">Zurück zur Startseite</a>
  </p>
</main>
