<?php
$html->addLocalizedTitlePart('RegisterTask');
?>
<main class="vm-main">
  <fieldset class="vm-container">
    <legend class="vm-container-label"><?=$html->getLocalizedText('AccountCreate')?></legend>
    <form id="login-form" method="post" action="<?=$html->actionUrl('register')?>">

      <?=$html->formGroupMessage('vm-form-message', $model['Message'] ?? '')?>
      <?=$html->formGroupWithInput(
        'vm-form-group', 'vm-form-label', 'vm-form-content', 'vm-form-control',
        'email', 'Email', $model['Users']->getDescription('Email'), true, '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$')
      ?>
      <?=$html->formGroupWithInput(
        'vm-form-group', 'vm-form-label', 'vm-form-content', 'vm-form-control',
        'password', 'Password', $html->getLocalizedText('Password'), true, '')
      ?>
      <?=$html->formGroupWithInput(
        'vm-form-group', 'vm-form-label', 'vm-form-content', 'vm-form-control',
        'password', 'PasswordRepeated', $html->getLocalizedText('PasswordRepeated'), true, '')
      ?>
      <?=$html->formGroupWithInput(
        'vm-form-group', 'vm-form-label', 'vm-form-content', 'vm-form-control',
        'text', 'FirstName', $model['Users']->getDescription('FirstName'), true, '')
      ?>
      <?=$html->formGroupWithInput(
        'vm-form-group', 'vm-form-label', 'vm-form-content', 'vm-form-control',
        'text', 'LastName', $model['Users']->getDescription('LastName'), true, '')
      ?>
      <div class="vm-form-group">
        <label for="IdCardNo" class="vm-form-label"><?=$model['Customers']->getDescription('IdCardNo')?></label>
        <div class="vm-form-content">
          <?=$html->input('vm-form-control',
            'text', 'IdCardNo', $model['Customers']->getDescription('IdCardNo'), true,
            '((\d{10}[A-Za-z]((\d{2}(0\d|1[0-2])([0-2]\d|3[0-1]))\d){2})|([CFGHJKLMNPRTVWXYZcfghjklmnprtvwxyz0123456789]{10}((\d{2}(0\d|1[0-2])([0-2]\d|3[0-1]))\d){2}[A-Za-z]))\d')
          ?>
          <!-- Use the following for testing!
          1220001297d640812517103198
          t22000129364081252010315d4
          -->
        </div>
        <label class="vm-form-label"></label>
        <div class="vm-form-content">
          <p>
            <?=$html->getLocalizedText('IdCardNoNotes')?>
          </p>
          <?=$html->image('ID-Card-Data-Old.png', 'Personalausweis bis 2010'); ?>
          <?=$html->image('ID-Card-Data-New.png', 'Personalausweis seit 2010'); ?>
        </div>
      </div>
      <?=$html->formGroupWithInput(
        'vm-form-group', 'vm-form-label', 'vm-form-content', 'vm-form-control',
        'text', 'Postcode', $model['Addresses']->getDescription('Postcode'), true, '\d{5}')
      ?>
      <?=$html->formGroupWithInput(
        'vm-form-group', 'vm-form-label', 'vm-form-content', 'vm-form-control',
        'text', 'City', $model['Addresses']->getDescription('City'), true, '')
      ?>
      <?=$html->formGroupWithInput(
        'vm-form-group', 'vm-form-label', 'vm-form-content', 'vm-form-control',
        'text', 'StreetHouseNo', $model['Addresses']->getDescription('StreetHouseNo'), true, '')
      ?>
      <button class="vm-button vm-primary" type="submit" form="login-form"><?=$html->getLocalizedText('Register')?></button>

    </form>
  </fieldset>
</main>
