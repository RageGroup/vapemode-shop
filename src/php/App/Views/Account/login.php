<?php
$html->addLocalizedTitlePart('LoginTask');
?>
<main class="vm-main">
  <fieldset class="vm-container">
    <legend class="vm-container-label"><?=$html->getLocalizedText('LoginTask')?></legend>
    <form id="login-form" method="post" action="<?=$html->actionUrl('login')?>">

      <?=$html->formGroupMessage('vm-form-message', $model['Message'] ?? '')?>
      <?=$html->formGroupWithInput(
        'vm-form-group', 'vm-form-label', 'vm-form-content', 'vm-form-control',
        'email', 'Email', $model['Users']->getDescription('Email'), true, '[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[A-Za-z]{2,}$')
      ?>
      <?=$html->formGroupWithInput(
        'vm-form-group', 'vm-form-label', 'vm-form-content', 'vm-form-control',
        'password', 'Password', $html->getLocalizedText('Password'), true)
      ?>
      <button class="vm-button vm-primary" type="submit" form="login-form"><?=$html->getLocalizedText('Login')?></button>

    </form>
  </fieldset>
</main>
