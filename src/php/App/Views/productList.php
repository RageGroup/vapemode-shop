<?php

$productType = $model['productType'] ?? '';
$productTotalCount = $model['productTotalCount'] ?? 0;
$productCount = $model['productCount'] ?? 12;
$productOffset = $model['productOffset'] ?? 0;
$products = $model['products'] ?? [];
if (count($products) == 0) {
  echo 'Keine passenden Liquids gefunden.';
  echo '<br /><br />';
  echo $html->actionLink('Zurück zur Startseite');
}
else {
  $prev = $productOffset > 0;
  $next = $productTotalCount > $productOffset + $productCount;
  $prevUrlParams = [
    'c' => $productCount,
    'o' => $productOffset - $productCount < 0 ? 0 : $productOffset - $productCount
  ];
  $nextUrlParams = ['c' => $productCount, 'o' => $productOffset + $productCount];
  if (strlen($productType) > 0) {
    $prevUrlParams['type'] = $productType;
    $nextUrlParams['type'] = $productType;
  }
  $prevLink = '<a class="vm-button vm-secondary' . ($prev ? '' : ' vm-disabled') . '" ' .
    ($prev ? '' : 'tabindex="-1" ') .
    ' href="' . $html->actionUrl('products', $prevUrlParams) . '">' . $html->getLocalizedText('Prev') . '</a>';
  $nextLink = '<a class="vm-button vm-secondary' . ($next ? '' : ' vm-disabled') . '" ' .
    ($next ? '' : 'tabindex="-1" ') .
    ' href="' . $html->actionUrl('products', $nextUrlParams) . '">' . $html->getLocalizedText('Next') . '</a>';
  echo $prevLink;
  echo $nextLink;
  echo '<br /><br />';
  echo '<ul class="vm-list-products">';
  foreach ($products as $product) {
    echo '<li><a href="' . $html->actionUrl('product/show/' . $product->getProductId()->toHexString()) . '">' .
      $html->figure($product->getImageFileName(), $product->getProductName()) .
      '</a></li>';
  }
  echo '</ul>';
  echo '<br />';
  echo $prevLink;
  echo $nextLink;
}
