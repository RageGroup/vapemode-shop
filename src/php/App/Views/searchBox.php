<fieldset class="vm-container">
  <form id="search-form" method="get" action="<?=$html->actionUrl('product/search')?>">
    <?=$html->token('r', 'product/search')?>
    <div class="vm-form-group">
      <div class="vm-form-content">
        <?=$html->localizedInput('vm-form-control', 'text', 'q', 'SearchLiquids', true, '.+', $model['searchText'] ?? '', false)?>
      </div>
      <button class="vm-button vm-primary" type="submit" form="search-form"><?=$html->getLocalizedText('Search')?></button>
    </div>
  </form>
</fieldset>
