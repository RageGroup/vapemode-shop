<?php
$html->addTitlePart('Dokumentation');
?>
<main class="vm-main">
  <div class="vm-container vm-doc">
    <h1>Dokumentation</h1>

    <h2>1 Inhalt</h2>
    <p>Die Kunden sind Nutzer einer elektrischen Zigarette. <br />
    Um ein, für den Kunden, ansprechendes Sortiment zu erstellen haben wir nur Premiumliquids, welche sehr beliebt sind, in unser Sortiment aufgenommen. <br />
    Dabei zu bedenken dass wir im Gegensatz zu allen anderen Anbietern keine Hardware, wie Akkuträger, Akkus, Köpfe usw,  verkaufen. <br />
    Da wir jedoch aktuell nur eine kleine Gruppe sind und nicht über entsprechende Mittel verfügen, haben wir nur eine kleine Auswahl.
    </p>

    <h2>2 Recherche</h2>
    <p><a href="https://www.riccardo-zigarette.de/">Riccardo Zigarette</a> <br />
    <a href="https://vaporexmachina.de/">VaporExMachina</a> <br />
    <a href="https://shop.dampfer-liquid.de/">Dampfer-Liquids</a><br />
    Und diverse andere Internetseiten u.a. auch Facebook
    </p>



    <h2>3 Design</h2>
    <p>Das Layout soll in der Produkt Übersicht eine ordentlich Liste aufzeigen und auch auf den anderen Seiten zB. der Landing-Page einen guten Eindruck hinterlassen.<br />
    Die Farbgebung haben wir so gewählt, um einen hohen Kontrast zu erzeugen. <br />
    Um eine Hohe Lesbarkeit auf unserer Website zu erreichen haben wir die Schriftart "Merriweather" gewählt. Für unser Logo jedoch um nutzen wir "UnifrakturCook", um Aufmerksamkeit auf uns zu ziehen.
    </p>

    <h2>4 Funktionaltitäten</h2>
    <h3><li>Landing Page</li></h3>
    <p>Die Quicklinks in diesem Bild werden wir auf den anderen Bildern nicht weiter erläutern, da diese auf jeder Seite vorkommen.</p>
    
    <p><?=$html->image('function_landing.png')?></p>
    <h3><li>Produktübersicht in der Gesamtübersicht, und Typ-abhängigen Übersicht </li></h3>
    <p><?=$html->image('function_multiple.png')?></p>

    <h3><li>Produkteinzelansicht</li></h3>
    <p><?=$html->image('function_single.png')?></p>

    <h3><li>Registrierung</li></h3>
    <p><?=$html->image('function_registry.png')?></p>

    <h3><li>Login</li></h3>
    <p><?=$html->image('function_login.png')?></p>
   
    <h3><li>Menü</li></h3>
    <p><?=$html->image('function_menu.png')?></p>
    

    <h2>5 Datenbank</h2>
    <h3>5.1 Entity-Relationship-Modell</h3>
    <p><?=$html->image('eERM.png')?></p>
    <h3>5.2 Relationales-Modell</h3>
    <p><?=$html->image('RM.png')?></p>

    <h2>6 Rollen</h2>
    <p>Bei uns herrscht eine klare und einfache Rollenverteilung - der Admin darf alles außer einkaufen, Kunden dürfen einkaufen, Gäste dürfen gucken. Die Fähigkeiten des Admin sind aber bislang nicht implementiert.</p>

    <h2>7 Besonderheiten</h2>
    <p>
      <h3>Unvollständige Implementierung</h3>
      <ul>
        <li>Sonderangebote</li>
        <li>Selbst Mischen von Aromen</li>
        <li>Anlegen neuer Aromen, Basen und Liquids dazu entsprechen die Rolle "Admin"</li>
        <li>Überführung des Warenkorbs zu einem Auftrag; Auftragshistorie</li>
        <li>Bestandsverwaltung</li>
        <li>Benutzerverwaltung: Expiration-Date, Last-Login-Date</li>
      </ul>
    </p>

    <p>
      <h3>Zusatzfeatures</h3>
      <ul>
        <li>
          Alles Projektspezifische rund um das Datenmodell wird über zwei Excel-Tabellen durch ein Werkzeugprogramm generiert.
          <ul>
            <li>SQL-Skripte zur Tabellenerstellung</li>
            <li>SQL-Skripte zur Befüllung mit grundlegenden Datensätzen</li>
            <li>PHP-Klassen zur Kapselung von Tabellenzugriffen und Datensatzmanipulation (Typ-Hints werden genutzt)</li>
          </ul>
        </li>
      </ul>
    </p>

    <h2>8 Projektmanagement</h2>
    <p>
      Gemeinsames Erarbeiten und Entwerfen der Website
      <br />
      Tätigkeitsverteilung:
      <br />
      <ul>
        <li>
          Funktionalität: Stefan Woyde
        </li>
        <li>
          Design und Entwurf: Christoph Düben
        </li>
      </ul>

      <br />
      Durch das gemeinsame Erarbeiten des Projektes wurden in Einzelarbeit sowie in Pairprogramming Fortschritte gemacht.
      <br />
      Aufgrund dieser Tatsache lässt sich eine genaue Aufgabenverteilung und Aufwandsanpassung?! nicht erstellen.
      <br />

    </p>

  </div>
</main>
