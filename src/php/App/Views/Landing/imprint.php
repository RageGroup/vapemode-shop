<?php
$html->addTitlePart('Impressum');
?>
<main class="vm-main">
  <div class="vm-container">
    <h1>Impressum</h1>
    <p>Entwickler: Christoph Düben & Stefan Woyde</p>
    <p>Repository mit dem Quellcode des Projekts: <a href="https://gitlab.com/RageGroup/vapemode-shop">https://gitlab.com/RageGroup/vapemode-shop</a></p>
    <p><?=$html->actionLink('Hier geht es zur Dokumentation.', 'documentation')?></p>
  </div>
</main>
