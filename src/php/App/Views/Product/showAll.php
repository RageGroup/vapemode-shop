<?php
$html->addTitlePart('Liquids');
?>
<main class="vm-main">
  <?=$html->include('searchBox', $model)?>
  <?=$html->include('productLinks', $model)?>
  <div class="vm-container">
    <?=$html->include('productList', $model)?>
  <div>
</main>
