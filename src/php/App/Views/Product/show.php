<?php
$customer = $model['customer'] ?? null;
$product = $model['product'] ?? null;
$html->addTitlePart($product->getProductName());
?>
<main class="vm-main">
  <div class="vm-container vm-product vm-flexparent">
    <div class="vm-flexchild">
      <?php
        $product = $model['product'] ?? null;
      
        echo $html->image($product->getImageFileName());
      ?>
    </div>
    <div class="vm-flexchild">
      <form method="post" action="<?=$html->actionUrl('basket/change')?>">
        <?=$html->token('productId', $product->getProductId()->toHexString())?>
        <table>
          <tr>
            <td><?=$product->getProductNameDescription()?></td>
            <td><?=$product->getProductName()?></td>
          </tr>
          <tr>
            <td><?=$product->getUnitPriceDescription()?></td>
            <td><?=number_format($product->getUnitPrice(), 2, ',', '.') . ' €'?></td>
          </tr>
          <tr>
            <td><?=$product->getManufacturerDescription()?></td>
            <td><?=$product->getManufacturer()?></td>
          </tr>
        
          <?php if ($product->getProductType()=='A' || $product->getProductType()=='L'){
          echo '<tr> <td>' . $product->getFlavourDescription() . '</td>' . '<td>' . $product->getFlavour() . '</td></tr>';
          
          if ($product->getProductType()=='A') {
            echo '<tr><td>' . $product->getMixRecommendationMinDescription() . '</td>' . '<td>' . $product->getMixRecommendationMin() . ' %</td></tr>' .
              '<tr><td>' . $product->getMixRecommendationMaxDescription() . '</td>' . '<td>' . $product->getMixRecommendationMax() . ' %</td></tr>';
          }}?>
          <tr></tr>
          <tr></tr>
          <tr></tr>
          <?=isset($customer) ?
            '<tr>
              <td>
                ' . $html->getLocalizedText('Quantity') . ':
                <input type="number" name="quantity" id="number" min="1" max="10" value="1">
              </td>
              <td>
                <button class="vm-button vm-primary" type="submit" name="addcart" id="addcart">
                  ' . $html->getLocalizedText('BasketAdd') . '
                </button>
              </td>
            </tr>' : ''
          ?>
        </table>
      </form>
    </div>
  </div>
</main>
