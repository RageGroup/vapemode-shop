<?php
$html->addTitlePart('Liquid mischen');
?>
<main class="vm-main">
  <fieldset class="vm-container">
    <legend class="vm-container-label">Liquid mischen</legend>
    <form action="" method="post">
      <div class="vm-mix">

        <label><input type="checkbox" value="fresh" /> Frische/Menthol </label> <br />
        <label><input type="checkbox" value="chocolate" /> Schokolade </label> <br />
        <label><input type="checkbox" value="vanille" /> Vanille </label> <br />
        <label><input type="checkbox" value="mint" /> Minze </label> <br />
        <label><input type="checkbox" value="lime" /> Limette </label> <br />
        <label><input type="checkbox" value="citrus" /> Zitrone </label> <br />
        <label><input type="checkbox" value="apple" /> Apfel </label> <br />
        <label><input type="checkbox" value="banana" /> Banane </label> <br />
        <label><input type="checkbox" value="pear" /> Birne </label> <br />
        <label><input type="checkbox" value="blueberry" /> Blaubeere </label> <br />
        <label><input type="checkbox" value="blackberry" /> Brombeere </label> <br />
        <label><input type="checkbox" value="strawberry" /> Erdbeere </label> <br />
        <label><input type="checkbox" value="raspberry" /> Himbeere </label> <br />

        <!-- optionen via db einbinden? -->
        <div class="vm-form-group">
        </div>
      </div>

      <div class="vm-mixed">
        <select name="mixcontent" id="mixcontent">
          <option value="10ml">10 ml</option>
          <option value="25ml">25 ml</option>
          <option value="50ml">50 ml</option>
          <option value="100ml">100 ml</option>
        </select>

        <button id="addcart" name="addcart" class="vm-button vm-primary" type="submit" form="login-form">Mischen</button>
      </div>
    <!-- kleine fkt zum zählen der ausgewählten geschmäcker und entsprechende berechnung des preises
    -->
    </form>
  </fieldset>
</main>
