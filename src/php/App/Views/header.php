<?php
use \Core\Web\Request;
$html->title = 'VapeMode-Shop';
$html->addStyle('vapemode.css');
?>
<?=$html->script('vapemode.js');?>
<header id="vm-header" class="vm-nojs">

  <span class="vm-name"><span>VapeMode</span><span> - <?=$html->getLocalizedText('Slogan')?></span></span>

  <?=
    $html->hasUser() ?
    '<p>' . $html->getLocalizedText('Hello') . ' ' . $html->getUser()->getAlias() . '.</p>' .
    $html->localizedActionLink('Logout', 'logout') :
    $html->localizedActionLink('Login', 'login') . ' <span>|</span> ' .
    $html->localizedActionLink('Register', 'register')
  ?>

  <?=$html->hasUser() ? '<a href="' . $html->actionUrl('basket') . '">' : ''?>
  <?=$html->image('Shopping-Cart.svg', 'Shopping-Cart', 'vm-basket')?>
  <?=$html->hasUser() ? '</a>': ''?>
</header>

<nav id="vm-menu" class="vm-nojs">
  <div id="vm-menu-icon-background">
    <div id="vm-menu-icon" onclick="menuClick()">
      <div></div>
      <div></div>
      <div></div>
    </div>
  </div>
  <ul>
    <li><?=$html->localizedActionLink('Landing')?></li>
    <li class="vm-submenu">
      <a onclick="submenuClick(this)"><?=$html->getLocalizedText('Account')?></a>
      <ul>
        <?=
          $html->hasUser() ?
          '<li>' . $html->localizedActionLink('Logout', 'logout') . '</li>' .
          '<li>' . $html->localizedActionLink('Basket', 'basket') . '</li>' .
          '<li>' . $html->localizedActionLink('Orders', 'orders') . '</li>' :
          '<li>' . $html->localizedActionLink('Login', 'login') . '</li>' .
          '<li>' . $html->localizedActionLink('Register', 'register') . '</li>'
        ?>
      </ul>
    </li>
    <li class="vm-submenu">
      <a onclick="submenuClick(this)"><?=$html->getLocalizedText('Products')?></a>
      <ul>
        <li><?=$html->localizedActionLink('AllProducts', 'products')?></li>
        <li><?=$html->localizedActionLink('Bases', 'products', ['type' => 'B'])?></li>
        <li><?=$html->localizedActionLink('Mixes', 'products', ['type' => 'L'])?></li>
        <li><?=$html->localizedActionLink('Aromas', 'products', ['type' => 'A'])?></li>
      </ul>
    </li>
    <li><?=$html->localizedActionLink('Imprint', 'imprint')?></li>
  </ul>
  <ul>
    <li><?=$html->actionLink('English', 'lang/en', ['ref' => Request::getUri()])?></li>
    <li><?=$html->actionLink('Deutsch', 'lang/de', ['ref' => Request::getUri()])?></li>
  </ul>
</nav>
