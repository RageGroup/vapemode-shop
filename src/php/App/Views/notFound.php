<?php
$html->addTitlePart('Seite nicht gefunden');
?>
<main class="vm-main">
  <p class="vm-container">
    Das ist nicht die Seite, nach der sie suchen! &#x1f609;
    <br />
    <br />
    <a href="<?=$html->actionUrl()?>">Zurück zur Startseite</a>
  </p>
</main>
