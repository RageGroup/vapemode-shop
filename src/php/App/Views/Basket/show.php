<?php
$html->addLocalizedTitlePart('Basket');
?>
<main class="vm-main">
  <div class="vm-container">
    <?php
      $basketItems = $model['basketItems'] ?? [];
      $productsById = $model['productsById'] ?? [];
      if (count($basketItems) == 0) {
        echo $html->getLocalizedText('BasketEmpty');
        echo '<br /><br />';
        echo $html->LocalizedActionLink('Localized');
      }
      else {
        $basketChangeUrl = $html->actionUrl('basket/set');
        echo '<ul class="vm-list-basket">';
        foreach ($basketItems ?? [] as $basketItem) {
          $product = $productsById[$basketItem->getProductId()->toHexString()];
          echo '<li class="vm-form-content">' .
            '<form method="post" action="' . $basketChangeUrl . '">' .
            '<button class="vm-button vm-secondary" type="submit" name="set" id="set">' . $html->getLocalizedText('Refresh') . '</button>' .
            $html->numberInput('', 'quantity', $basketItem->getQuantity(), '1', '0', '100') . ' ' . $html->getLocalizedText('Pieces') . ' ' .
            $product->getProductName() . ' ' . $html->getLocalizedText('PriceEach') . ' ' .
            $html->token('productId', $product->getProductId()->toHexString()) .
            number_format($product->getUnitPrice(), 2, ',', '.') . ' €' .
            '</form>' .
            '</li>';
        }
        echo '</ul>';
        echo '<form method="post" action="' . $html->actionUrl('order/new') . '">' .
          '<button class="vm-button vm-primary" type="submit" name="order" id="order">' . $html->getLocalizedText('Order') . '</button>' .
          '</form>';
      }
    ?>
  <div>
</main>
