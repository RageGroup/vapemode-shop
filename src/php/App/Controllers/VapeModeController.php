<?php namespace App\Controllers;

use \Core\Web\Request;
use \Core\Web\Route;

use \App\Models\VapeModeContext;
use \App\Models\User;

/**
 * Base controller for this web app.
 */
abstract class VapeModeController extends \Core\Web\Controller {

  /**
   * Creates a base controller for this web app.
   *
   * @param Route $route
   */
  public function __construct(Route $route) {
    parent::__construct($route, new VapeModeContext($route->getRouter()->getBaseDir()));
  }

}
