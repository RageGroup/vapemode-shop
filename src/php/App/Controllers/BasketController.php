<?php namespace App\Controllers;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Web\Request;
use \Core\Web\Route;
use \Core\Web\Response;

/**
 * Controller for accessing the basket.
 */
final class BasketController extends VapeModeController {

  /**
   * Creates a controller for accessing the basket.
   */
  public function __construct(Route $route) {
    parent::__construct($route);
  }

  /**
   * Show the basket.
   *
   * @return Response
   */
  public function show(): Response {
    $model = [];
    if ($this->hasUser()) {
      $customer = $this->context->getCustomers()->getByPk($this->getUser()->getUserId());
      if (isset($customer)) {
        $model['basketItems'] = $this->context->getBasketItems()->getByCustomerId($customer->getCustomerId());
        $model['productsById'] = [];
        foreach ($model['basketItems'] as $basketItem) {
          $productId = $basketItem->getProductId();
          $model['productsById'][$productId->toHexString()] = $this->context->getProducts()->getByPk($productId);
        }
      }
    }
    return $this->view(__METHOD__, $model);
  }

  /**
   * Set the quantity for a basket item. Remove it when zero.
   *
   * @return Response
   */
  public function set(): Response {
    $productId = $_POST['productId'] ?? '';
    $quantity = (int) ($_POST['quantity'] ?? '');
    if ($this->hasUser() && strlen($productId) > 0) {
      $customer = $this->context->getCustomers()->getByPk($this->getUser()->getUserId());
      if (isset($customer)) {
        $productId = Uuid::createFromHex($productId);
        $basketItem = $this->context->getBasketItems()->getByProductId($productId);
        if ($quantity > 0) {
          if (is_null($basketItem)) {
            $basketItem = $this->context->getBasketItems()->create(
              $customer->getCustomerId(),
              new DateTime(),
              $productId,
              $quantity);
          }
          else {
            $basketItem->setQuantity($quantity);
          }
        }
        elseif (isset($basketItem)) {
          $this->context->getBasketItems()->delete($basketItem);
        }
        $this->context->saveChanges();
      }
    }
    return $this->redirect('basket');
  }

  /**
   * Change the quantity for a basket item.
   *
   * @return Response
   */
  public function change(): Response {
    $productId = $_POST['productId'] ?? '';
    $quantity = (int) ($_POST['quantity'] ?? '');
    if ($this->hasUser() && strlen($productId) > 0) {
      $customer = $this->context->getCustomers()->getByPk($this->getUser()->getUserId());
      if (isset($customer)) {
        $productId = Uuid::createFromHex($productId);
        $basketItem = $this->context->getBasketItems()->getByProductId($productId);
        if (is_null($basketItem)) {
          if ($quantity > 0) {
            $basketItem = $this->context->getBasketItems()->create(
              $customer->getCustomerId(),
              new DateTime(),
              $productId,
              $quantity);
          }
        }
        else {
          $quantity += $basketItem->getQuantity();
          if ($quantity > 0) {
            $basketItem->setQuantity($quantity);
          }
          else {
            $this->context->getBasketItems()->delete($basketItem);
          }
        }
        $this->context->saveChanges();
      }
    }
    return $this->redirect('basket');
  }

}
