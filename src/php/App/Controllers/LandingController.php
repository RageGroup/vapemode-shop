<?php namespace App\Controllers;

use \Core\Web\Request;
use \Core\Web\Route;
use \Core\Web\Response;

/**
 * Controller for the landing page.
 */
final class LandingController extends VapeModeController {

  /**
   * Creates a controller for the landing page.
   *
   * @param Route $route
   */
  public function __construct(Route $route) {
    parent::__construct($route);
  }

  /**
   * Show the landing page.
   *
   * @return Response
   */
  public function show(): Response {
    $model['products'] = $this->context->getProducts()->getRows();
    return $this->view(__METHOD__, $model);
  }

  /**
   * Set the client language.
   *
   * @return Response
   */
  public function setLang(string $langId): Response {
    $_SESSION['LangId'] = $langId;
    return $this->redirectUrl($_GET['ref'] ?? '');
  }

  /**
   * Show the imprint.
   *
   * @return Response
   */
  public function imprint(): Response {
    return $this->view(__METHOD__);
  }

  /**
   * Show the documentation.
   *
   * @return Response
   */
  public function documentation(): Response {
    return $this->view(__METHOD__);
  }

}
