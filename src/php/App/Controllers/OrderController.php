<?php namespace App\Controllers;

use \Core\Web\Request;
use \Core\Web\Route;
use \Core\Web\Response;

/**
 * Controller for accessing orders.
 */
final class OrderController extends VapeModeController {

  /**
   * Creates a controller for accessing orders.
   */
  public function __construct(Route $route) {
    parent::__construct($route);
  }

  /**
   * Show all orders.
   *
   * @return Response
   */
  public function showAll(): Response {
    return $this->view(__METHOD__);
  }

  /**
   * Show the given order.
   *
   * @return Response
   */
  public function show(string $orderId): Response {
    return $this->view(__METHOD__, ['id' => $orderId]);
  }

  /**
   * Prepare to change the given order.
   *
   * @return Response
   */
  public function getChange(string $orderId): Response {
    return $this->view('change', ['id' => $orderId]);
  }

  /**
   * Change the given order.
   *
   * @return Response
   */
  public function postChange(string $orderId): Response {
    return $this->view('change', ['id' => $orderId]);
  }

  /**
   * Create a new order.
   *
   * @return Response
   */
  public function new(): Response {
    return $this->view(__METHOD__);
  }

}
