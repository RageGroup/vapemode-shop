<?php namespace App\Controllers;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Logging\Logger;
use \Core\Web\Request;
use \Core\Web\Route;
use \Core\Web\Response;

use \App\Models\User;

/**
 * Controller for accessing user accounts.
 */
final class AccountController extends VapeModeController {

  /**
   * Creates a controller for accessing user accounts.
   *
   * @param Route $route
   */
  public function __construct(Route $route) {
    parent::__construct($route);
  }

  /**
   * Prepare to register a new user.
   *
   * @return Response
   */
  public function getRegister(): Response {
    if (!$this->hasUser()) {
      return $this->view('register', [
        'Users' => $this->context->getUsers(),
        'Addresses' => $this->context->getAddresses(),
        'Customers' => $this->context->getCustomers()]);
    }
    return $this->redirect();
  }

  /**
   * Register a new user (customer).
   *
   * @return Response
   */
  public function postRegister(): Response {
    $model = [];
    $email = $_POST['Email'] ?? '';
    $firstName = $_POST['FirstName'] ?? '';
    $lastName = $_POST['LastName'] ?? '';
    $idCardNo = $_POST['IdCardNo'] ?? '';
    $postcode = $_POST['Postcode'] ?? '';
    $city = $_POST['City'] ?? '';
    $streetHouseNo = $_POST['StreetHouseNo'] ?? '';
    if (strlen($email) > 5) {
    }
    else {
      $model['Message'] = $this->getLocalizedText('PasswordRepetitionInvalid');
    }
    $user = $this->context->getUsers()->getByEmail($email);
    if (is_null($user)) {
      if (($_POST['Password'] ?? '') === ($_POST['PasswordRepeated'] ?? '')) {
        $user = $this->context->getUsers()->create($firstName . ' '  . $lastName, $email, $firstName, $lastName);
        $address = $this->context->getAddresses()->create($postcode, $city, $streetHouseNo);
        $customer = $this->context->getCustomers()->create(
          $user->getUserId(),
          $idCardNo,
          $address->getAddressId(),
          $address->getAddressId());
        $user->setPassword($_POST['Password'] ?? '');
        $this->context->saveChanges();
        if (!is_null($user)) {
          $_SESSION['UserId'] = $user->getUserId();
          return $this->redirect();
        }
      }
      else {
        $model['Message'] = $this->getLocalizedText('PasswordRepetitionInvalid');
      }
    }
    else {
      $model['Message'] = $this->getLocalizedText('EmailTaken');
    }
    return $this->view('register', $model);
  }

  /**
   * Logout the user.
   *
   * @return Response
   */
  public function logout(): Response {
    unset($_SESSION['UserId']);
    return $this->redirect();
  }

  /**
   * Prepare to login a user.
   *
   * @return Response
   */
  public function getLogin(): Response {
    if (!$this->hasUser()) {
      return $this->view('login', [
        'Users' => $this->context->getUsers()]);
    }
    return $this->redirect();
  }

  /**
   * Login a user.
   *
   * @return Response
   */
  public function postLogin(): Response {
    $model = [];
    $user = $this->context->getUsers()->getByEmail($_POST['Email'] ?? '');
    if (!is_null($user)) {
      if ($user->verifyPassword($_POST['Password'] ?? '')) {
        $_SESSION['UserId'] = $user->getUserId();
        return $this->redirect();
      }
      else {
        $model['Message'] = $this->getLocalizedText('EmailOrPasswordInvalid');
      }
    }
    else {
      $model['Message'] = $this->getLocalizedText('EmailOrPasswordInvalid');
    }
    return $this->view('login', $model);
  }

}
