<?php namespace App\Controllers;

use \Core\Data\Uuid;
use \Core\Web\Request;
use \Core\Web\Route;
use \Core\Web\Response;

/**
* Controller for accessing products.
*/
final class ProductController extends VapeModeController {

  /**
  * Creates a controller for accessing products.
  */
  public function __construct(Route $route) {
    parent::__construct($route);
  }

  /**
  * Show all products.
  *
  * @return Response
  */
  public function showAll(): Response {
    $model['productType'] = htmlspecialchars($_GET['type'] ?? '');
    $model['productCount'] = (int) ($_GET['c'] ?? '12');
    $model['productOffset'] = (int) ($_GET['o'] ?? '0');
    $model['productTotalCount'] = $this->context->getProducts()->countByType($model['productType']);
    $model['products'] = $this->context->getProducts()->getRowsByType(
      $model['productType'],
      $model['productCount'],
      $model['productOffset']);
    return $this->view(__METHOD__, $model);
  }

  /**
  * Search products.
  *
  * @return Response
  */
  public function search(): Response {
    $model['searchText'] = $_GET['q'];
    $model['productCount'] = $_GET['c'] ?? 12;
    $model['productOffset'] = $_GET['o'] ?? 0;
    $model['products'] = $this->context->getProducts()->getRowsBySearchText(
      $model['searchText'],
      $model['productCount'],
      $model['productOffset']);
    $model['productTotalCount'] = $this->context->getProducts()->countBySearchText(
      $model['searchText']);
    return $this->view(__METHOD__, $model);
  }

  /**
  * Show the given product.
  *
  * @return Response
  */
  public function show(string $productId): Response {
    $model = [];
    $model['product'] = $this->context->getProducts()->getByPk(Uuid::createFromHex($productId));
    if ($this->hasUser()) {
      $customer = $this->context->getCustomers()->getByPk($this->getUser()->getUserId());
      if (isset($customer)) {
        $model['customer'] = $customer;
      }
    }
    return $this->view(__METHOD__, $model);
  }

  /**
  * Mix a product.
  *
  * @return Response
  */
  public function mix(): Response {
    $model = [];
    return $this->view(__METHOD__, $model);
  }

}
