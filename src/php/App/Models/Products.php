<?php namespace App\Models;

use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table for Products.
 */
class Products extends ProductTable {

  /**
   * Create the table for Products.
   *
   * @param VapeModeContext $context
   * @param array $columns
   */
  public function __construct(VapeModeContext $context, array $columns) {
    parent::__construct($context, $columns);
  }

  /**
   * Get products by it type.
   *
   * @param string $productType
   * @param int $count The count of rows to fetch.
   * @param int $offset The offset of rows to fetch.
   * @return array
   */
  public function getRowsByType(string $productType, int $count = 0, int $offset = 0): array {
    $conditions = [];
    if (strlen($productType) > 0) {
      $conditions[] = new SqlParameter('ProductType', '=', $productType);
    }
    return $this->getRows($conditions, $count, $offset);
  }

  /**
   * Get product count by type.
   *
   * @param string $productType
   * @return int
   */
  public function countByType(string $productType): int {
    $conditions = [];
    if (strlen($productType) > 0) {
      $conditions[] = new SqlParameter('ProductType', '=', $productType);
    }
    return $this->count($conditions);
  }

  /**
   * Get product count by a search text.
   *
   * @param string $searchText
   * @return array
   */
  public function getSearchTextConditions(string $searchText): array {
    // A proper search engine would require moch mor time to develop,
    // and choosing & using frameworks wasn't the goal.
    $conditions = [];
    if (strlen($searchText) > 0) {
      $searchText = '%' . $searchText . '%';
      $conditions[] = new SqlParameter('ProductName', 'like', $searchText);
      $conditions[] = ' or ';
      $conditions[] = new SqlParameter('Flavour', 'like', $searchText);
      $conditions[] = ' or ';
      $conditions[] = new SqlParameter('Manufacturer', 'like', $searchText);
    }
    return $conditions;
  }

  /**
   * Get products by a search text.
   *
   * @param string $searchText
   * @param int $count The count of rows to fetch.
   * @param int $offset The offset of rows to fetch.
   * @return array
   */
  public function getRowsBySearchText(string $searchText, int $count = 0, int $offset = 0): array {
    return $this->getRows($this->getSearchTextConditions($searchText), $count, $offset);
  }

  /**
   * Get product count by a search text.
   *
   * @param string $searchText
   * @return int
   */
  public function countBySearchText(string $searchText): int {
    return $this->count($this->getSearchTextConditions($searchText));
  }

}
