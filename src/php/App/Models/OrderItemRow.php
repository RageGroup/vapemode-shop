<?php namespace App\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Table;

/**
 * A data table row for one Order Item.
 * This class was auto-generated.
 */
abstract class OrderItemRow extends \Core\Data\Row {

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public static function pkHash(Uuid $orderLineId): string {
    $hash = '';
    $hash = \md5($hash . ((string) $orderLineId), true);
    return $hash;
  }

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public function getPkHash(): string {
    return self::pkHash($this->getUuid('OrderLineId'));
  }

  /**
   * Get the localized description of the Order Line ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getOrderLineIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('OrderLineId', $languageCode);
  }

  /**
   * Get the Order Line ID.
   *
   * @return Uuid
   */
  public function getOrderLineId(): ?Uuid {
    return $this->getUuid('OrderLineId');
  }

  /**
   * Set the Order Line ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setOrderLineId(?Uuid $value): void {
    $this->set('OrderLineId', $value);
  }

  /**
   * Get the localized description of the Order ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getOrderIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('OrderId', $languageCode);
  }

  /**
   * Get the Order ID.
   *
   * @return Uuid
   */
  public function getOrderId(): ?Uuid {
    return $this->getUuid('OrderId');
  }

  /**
   * Set the Order ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setOrderId(?Uuid $value): void {
    $this->set('OrderId', $value);
  }

  /**
   * Get the localized description of the Creation Date.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getCreationDateDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('CreationDate', $languageCode);
  }

  /**
   * Get the Creation Date.
   *
   * @return DateTime
   */
  public function getCreationDate(): ?DateTime {
    return $this->getDateTime('CreationDate');
  }

  /**
   * Set the Creation Date.
   *
   * @param DateTime $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setCreationDate(?DateTime $value): void {
    $this->set('CreationDate', $value);
  }

  /**
   * Get the localized description of the Pruduct ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getProductIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('ProductId', $languageCode);
  }

  /**
   * Get the Pruduct ID.
   *
   * @return Uuid
   */
  public function getProductId(): ?Uuid {
    return $this->getUuid('ProductId');
  }

  /**
   * Set the Pruduct ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setProductId(?Uuid $value): void {
    $this->set('ProductId', $value);
  }

  /**
   * Get the localized description of the Quantity.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getQuantityDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('Quantity', $languageCode);
  }

  /**
   * Get the Quantity.
   *
   * @return int
   */
  public function getQuantity(): ?int {
    return $this->getInt('Quantity');
  }

  /**
   * Set the Quantity.
   *
   * @param int $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setQuantity(?int $value): void {
    $this->set('Quantity', $value);
  }

  /**
   * Get the localized description of the Unit Price.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getUnitPriceDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('UnitPrice', $languageCode);
  }

  /**
   * Get the Unit Price.
   *
   * @return float
   */
  public function getUnitPrice(): ?float {
    return $this->getFloat('UnitPrice');
  }

  /**
   * Set the Unit Price.
   *
   * @param float $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setUnitPrice(?float $value): void {
    $this->set('UnitPrice', $value);
  }

  /**
   * Create an entity object for a table with initial data.
   *
   * @param Table $table
   * @param array $data
   */
  public function __construct(Table $table, array $data, bool $inserted = true) {
    parent::__construct($table, $data, $inserted);
  }

}
