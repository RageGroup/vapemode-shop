<?php namespace App\Models;

use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table for OrderItems.
 */
class OrderItems extends OrderItemTable {

  /**
   * Create the table for OrderItems.
   *
   * @param VapeModeContext $context
   * @param array $columns
   */
  public function __construct(VapeModeContext $context, array $columns) {
    parent::__construct($context, $columns);
  }

}
