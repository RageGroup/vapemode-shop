<?php namespace App\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Row;
use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table base for all Order Items.
 * This class was auto-generated.
 */
class OrderItemTable extends \Core\Data\Table {

  /**
   * Create the table for a context and with table information.
   *
   * @param DbContext $context
   * @param array $columns
   */
  public function __construct(DbContext $context, array $columns) {
    parent::__construct($context, 'OrderItems', '', ['OrderLineId'], $columns);
  }

  /**
   * Get a row by its data.
   * Exists to be available for the table base implementation.
   *
   * @param array $data
   * @return Row
   */
  protected function newRow(array $data): Row {
    $row = new OrderItem($this, $data, false);
    $this->selectedRows[$row->getPkHash()] = $row;
    return $row;
  }

  /**
  * Create a new Order Item.
  *
  * @return OrderItem
  */
  public function create(
    Uuid $orderId,
    DateTime $creationDate):
    OrderItem
  {
    $orderLineId = Uuid::generate();
    $entity = new OrderItem($this, [
      'OrderLineId' => $orderLineId->toString(),
      'OrderId' => $orderId->toString(),
      'CreationDate' => $creationDate
    ], false);
    $hash = $entity->getPkHash();
    $this->selectedRows[$hash] = $entity;
    $this->insertedRows[$hash] = $entity;
    return $entity;
  }

  /**
   * Get an entity object by its primary key value.
   *
   * @param Uuid $orderLineId
   * @return OrderItem
   */
  public function getByPk(Uuid $orderLineId): ?OrderItem {
    return $this->getFirstRow([new SqlParameter('OrderLineId', '=', $orderLineId)]);
  }


}
