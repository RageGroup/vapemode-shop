<?php namespace App\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Row;
use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table base for all Addresses.
 * This class was auto-generated.
 */
class AddressTable extends \Core\Data\Table {

  /**
   * Create the table for a context and with table information.
   *
   * @param DbContext $context
   * @param array $columns
   */
  public function __construct(DbContext $context, array $columns) {
    parent::__construct($context, 'Addresses', '', ['AddressId'], $columns);
  }

  /**
   * Get a row by its data.
   * Exists to be available for the table base implementation.
   *
   * @param array $data
   * @return Row
   */
  protected function newRow(array $data): Row {
    $row = new Address($this, $data, false);
    $this->selectedRows[$row->getPkHash()] = $row;
    return $row;
  }

  /**
  * Create a new Address.
  *
  * @return Address
  */
  public function create(
    string $postcode,
    string $city,
    string $streetHouseNo):
    Address
  {
    $addressId = Uuid::generate();
    $entity = new Address($this, [
      'AddressId' => $addressId->toString(),
      'Postcode' => $postcode,
      'City' => $city,
      'StreetHouseNo' => $streetHouseNo
    ], false);
    $hash = $entity->getPkHash();
    $this->selectedRows[$hash] = $entity;
    $this->insertedRows[$hash] = $entity;
    return $entity;
  }

  /**
   * Get an entity object by its primary key value.
   *
   * @param Uuid $addressId
   * @return Address
   */
  public function getByPk(Uuid $addressId): ?Address {
    return $this->getFirstRow([new SqlParameter('AddressId', '=', $addressId)]);
  }


}
