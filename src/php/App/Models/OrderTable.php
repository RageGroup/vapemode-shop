<?php namespace App\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Row;
use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table base for all Orders.
 * This class was auto-generated.
 */
class OrderTable extends \Core\Data\Table {

  /**
   * Create the table for a context and with table information.
   *
   * @param DbContext $context
   * @param array $columns
   */
  public function __construct(DbContext $context, array $columns) {
    parent::__construct($context, 'Orders', '', ['OrderId'], $columns);
  }

  /**
   * Get a row by its data.
   * Exists to be available for the table base implementation.
   *
   * @param array $data
   * @return Row
   */
  protected function newRow(array $data): Row {
    $row = new Order($this, $data, false);
    $this->selectedRows[$row->getPkHash()] = $row;
    return $row;
  }

  /**
  * Create a new Order.
  *
  * @return Order
  */
  public function create(
    DateTime $orderDate,
    string $orderState,
    Uuid $customerId,
    Uuid $shippingAddressId,
    float $totalPrice):
    Order
  {
    $orderId = Uuid::generate();
    $entity = new Order($this, [
      'OrderId' => $orderId->toString(),
      'OrderDate' => $orderDate,
      'OrderState' => $orderState,
      'CustomerId' => $customerId->toString(),
      'ShippingAddressId' => $shippingAddressId->toString(),
      'TotalPrice' => $totalPrice
    ], false);
    $hash = $entity->getPkHash();
    $this->selectedRows[$hash] = $entity;
    $this->insertedRows[$hash] = $entity;
    return $entity;
  }

  /**
   * Get an entity object by its primary key value.
   *
   * @param Uuid $orderId
   * @return Order
   */
  public function getByPk(Uuid $orderId): ?Order {
    return $this->getFirstRow([new SqlParameter('OrderId', '=', $orderId)]);
  }


}
