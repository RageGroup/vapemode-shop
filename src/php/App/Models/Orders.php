<?php namespace App\Models;

use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table for Orders.
 */
class Orders extends OrderTable {

  /**
   * Create the table for Orders.
   *
   * @param VapeModeContext $context
   * @param array $columns
   */
  public function __construct(VapeModeContext $context, array $columns) {
    parent::__construct($context, $columns);
  }

}
