<?php namespace App\Models;

use \Core\Data\DbContext;
//use \Core\Data\Column;
//use \Core\Data\UserTableBase;
use \Core\Models\Language;
use \Core\Models\Languages;
use \Core\Models\Table;
use \Core\Models\Tables;
use \Core\Models\Column;
use \Core\Models\Columns;
use \Core\Models\Role;
use \Core\Models\Roles;
use \Core\Models\UserRole;
use \Core\Models\UserRoles;
use \App\Models\User;
use \App\Models\Users;
use \App\Models\Address;
use \App\Models\Addresses;
use \App\Models\Customer;
use \App\Models\Customers;
use \App\Models\Product;
use \App\Models\Products;
use \App\Models\Order;
use \App\Models\Orders;
use \App\Models\OrderItem;
use \App\Models\OrderItems;
use \App\Models\BasketItem;
use \App\Models\BasketItems;

/**
 * The data context for model "VapeMode".
 * This class was auto-generated.
 */
class VapeModeContext extends DbContext {

  /**
   * The table for Languages.
   *
   * @var Languages
   */
  private $languages;

  /**
   * Get the table for Languages.
   *
   * @return Languages
   */
  public function getLanguages(): Languages { return $this->languages; }

  /**
   * The table for Tables.
   *
   * @var Tables
   */
  private $tables;

  /**
   * Get the table for Tables.
   *
   * @return Tables
   */
  public function getTables(): Tables { return $this->tables; }

  /**
   * The table for Table Columns.
   *
   * @var Columns
   */
  private $columns;

  /**
   * Get the table for Table Columns.
   *
   * @return Columns
   */
  public function getColumns(): Columns { return $this->columns; }

  /**
   * The table for Roles.
   *
   * @var Roles
   */
  private $roles;

  /**
   * Get the table for Roles.
   *
   * @return Roles
   */
  public function getRoles(): Roles { return $this->roles; }

  /**
   * The table for User Roles.
   *
   * @var UserRoles
   */
  private $userRoles;

  /**
   * Get the table for User Roles.
   *
   * @return UserRoles
   */
  public function getUserRoles(): UserRoles { return $this->userRoles; }

  /**
   * The table for Users.
   *
   * @var Users
   */
  private $users;

  /**
   * Get the table for Users.
   *
   * @return Users
   */
  public function getUsers(): Users { return $this->users; }

  /**
   * The table for Addresses.
   *
   * @var Addresses
   */
  private $addresses;

  /**
   * Get the table for Addresses.
   *
   * @return Addresses
   */
  public function getAddresses(): Addresses { return $this->addresses; }

  /**
   * The table for Customers.
   *
   * @var Customers
   */
  private $customers;

  /**
   * Get the table for Customers.
   *
   * @return Customers
   */
  public function getCustomers(): Customers { return $this->customers; }

  /**
   * The table for Products.
   *
   * @var Products
   */
  private $products;

  /**
   * Get the table for Products.
   *
   * @return Products
   */
  public function getProducts(): Products { return $this->products; }

  /**
   * The table for Orders.
   *
   * @var Orders
   */
  private $orders;

  /**
   * Get the table for Orders.
   *
   * @return Orders
   */
  public function getOrders(): Orders { return $this->orders; }

  /**
   * The table for Order Items.
   *
   * @var OrderItems
   */
  private $orderItems;

  /**
   * Get the table for Order Items.
   *
   * @return OrderItems
   */
  public function getOrderItems(): OrderItems { return $this->orderItems; }

  /**
   * The table for Basket Items.
   *
   * @var BasketItems
   */
  private $basketItems;

  /**
   * Get the table for Basket Items.
   *
   * @return BasketItems
   */
  public function getBasketItems(): BasketItems { return $this->basketItems; }

  /**
   * Get the user table.
   * Exists to be available for the context base implementation.
   *
   * @return Users
   */
  public function getUserTable(): \Core\Models\Users { return $this->users; }

  /**
   * Create a context object.
   *
   * @param string $baseDir The base directory that contains the config file.
   */
  public function __construct(string $baseDir) {
    parent::__construct($baseDir);
    $this->languages = new Languages($this, [
      'LanguageCode' => 'LanguageCode',
      'LanguageName' => 'LanguageName'
    ]);
    $this->tableAccesses[] = $this->languages;
    $this->tables = new Tables($this, [
      'TableName' => 'TableName'
    ]);
    $this->tableAccesses[] = $this->tables;
    $this->columns = new Columns($this, [
      'TableName' => 'TableName',
      'ColumnName' => 'ColumnName'
    ]);
    $this->tableAccesses[] = $this->columns;
    $this->roles = new Roles($this, [
      'RoleId' => 'RoleId',
      'RoleName' => 'RoleName'
    ]);
    $this->tableAccesses[] = $this->roles;
    $this->userRoles = new UserRoles($this, [
      'UserId' => 'UserId',
      'RoleId' => 'RoleId'
    ]);
    $this->tableAccesses[] = $this->userRoles;
    $this->users = new Users($this, [
      'UserId' => 'UserId',
      'UserName' => 'UserName',
      'PasswordHash' => 'PasswordHash',
      'ExpirationDate' => 'ExpirationDate',
      'LastLoginDate' => 'LastLoginDate',
      'LastActivityDate' => 'LastActivityDate',
      'Email' => 'Email',
      'FirstName' => 'FirstName',
      'LastName' => 'LastName',
      'UserAgent' => 'UserAgent',
      'RemoteIpAddress' => 'RemoteIpAddress',
      'ForwardedIpAddress' => 'ForwardedIpAddress'
    ]);
    $this->tableAccesses[] = $this->users;
    $this->addresses = new Addresses($this, [
      'AddressId' => 'AddressId',
      'Postcode' => 'Postcode',
      'City' => 'City',
      'StreetHouseNo' => 'StreetHouseNo'
    ]);
    $this->tableAccesses[] = $this->addresses;
    $this->customers = new Customers($this, [
      'CustomerId' => 'CustomerId',
      'IdCardNo' => 'IdCardNo',
      'BillingAddressId' => 'BillingAddressId',
      'DefaultShippingAddressId' => 'DefaultShippingAddressId'
    ]);
    $this->tableAccesses[] = $this->customers;
    $this->products = new Products($this, [
      'ProductId' => 'ProductId',
      'ProductType' => 'ProductType',
      'Stock' => 'Stock',
      'UnitPrice' => 'UnitPrice',
      'Nicotine' => 'Nicotine',
      'Content' => 'Content',
      'MixRecommendationMin' => 'MixRecommendationMin',
      'MixRecommendationMax' => 'MixRecommendationMax',
      'Manufacturer' => 'Manufacturer',
      'ImageFileName' => 'ImageFileName'
    ]);
    $this->tableAccesses[] = $this->products;
    $this->orders = new Orders($this, [
      'OrderId' => 'OrderId',
      'OrderDate' => 'OrderDate',
      'OrderState' => 'OrderState',
      'CustomerId' => 'CustomerId',
      'ShippingAddressId' => 'ShippingAddressId',
      'TotalPrice' => 'TotalPrice'
    ]);
    $this->tableAccesses[] = $this->orders;
    $this->orderItems = new OrderItems($this, [
      'OrderLineId' => 'OrderLineId',
      'OrderId' => 'OrderId',
      'CreationDate' => 'CreationDate',
      'ProductId' => 'ProductId',
      'Quantity' => 'Quantity',
      'UnitPrice' => 'UnitPrice'
    ]);
    $this->tableAccesses[] = $this->orderItems;
    $this->basketItems = new BasketItems($this, [
      'BasketItemId' => 'BasketItemId',
      'CustomerId' => 'CustomerId',
      'CreationDate' => 'CreationDate',
      'ProductId' => 'ProductId',
      'Quantity' => 'Quantity'
    ]);
    $this->tableAccesses[] = $this->basketItems;
  }

}
