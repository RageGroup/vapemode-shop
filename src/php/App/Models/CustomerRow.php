<?php namespace App\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Table;

/**
 * A data table row for one Customer.
 * This class was auto-generated.
 */
abstract class CustomerRow extends \Core\Data\Row {

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public static function pkHash(Uuid $customerId): string {
    $hash = '';
    $hash = \md5($hash . ((string) $customerId), true);
    return $hash;
  }

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public function getPkHash(): string {
    return self::pkHash($this->getUuid('CustomerId'));
  }

  /**
   * Get the localized description of the Customer ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getCustomerIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('CustomerId', $languageCode);
  }

  /**
   * Get the Customer ID.
   *
   * @return Uuid
   */
  public function getCustomerId(): ?Uuid {
    return $this->getUuid('CustomerId');
  }

  /**
   * Set the Customer ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setCustomerId(?Uuid $value): void {
    $this->set('CustomerId', $value);
  }

  /**
   * Get the localized description of the ID Card No..
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getIdCardNoDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('IdCardNo', $languageCode);
  }

  /**
   * Get the ID Card No..
   *
   * @return string
   */
  public function getIdCardNo(): ?string {
    return $this->getString('IdCardNo');
  }

  /**
   * Set the ID Card No..
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setIdCardNo(?string $value): void {
    $this->set('IdCardNo', $value);
  }

  /**
   * Get the localized description of the Billing Address ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getBillingAddressIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('BillingAddressId', $languageCode);
  }

  /**
   * Get the Billing Address ID.
   *
   * @return Uuid
   */
  public function getBillingAddressId(): ?Uuid {
    return $this->getUuid('BillingAddressId');
  }

  /**
   * Set the Billing Address ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setBillingAddressId(?Uuid $value): void {
    $this->set('BillingAddressId', $value);
  }

  /**
   * Get the localized description of the Default Shipping Address ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getDefaultShippingAddressIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('DefaultShippingAddressId', $languageCode);
  }

  /**
   * Get the Default Shipping Address ID.
   *
   * @return Uuid
   */
  public function getDefaultShippingAddressId(): ?Uuid {
    return $this->getUuid('DefaultShippingAddressId');
  }

  /**
   * Set the Default Shipping Address ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setDefaultShippingAddressId(?Uuid $value): void {
    $this->set('DefaultShippingAddressId', $value);
  }

  /**
   * Create an entity object for a table with initial data.
   *
   * @param Table $table
   * @param array $data
   */
  public function __construct(Table $table, array $data, bool $inserted = true) {
    parent::__construct($table, $data, $inserted);
  }

}
