<?php namespace App\Models;

use \Core\Data\Table;

/**
* A basket item.
*/
final class BasketItem extends BasketItemRow {

  /**
  * Create a basket item object for a table with initial data.
  * May not be called directly.
  *
  * @param Table $table
  * @param array $data
  */
  public function __construct(Table $table, array $data) {
    parent::__construct($table, $data);
  }

}
