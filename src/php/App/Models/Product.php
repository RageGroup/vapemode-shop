<?php namespace App\Models;

use \Core\Data\Uuid;
use \Core\Data\Table;

/**
* A class for Products.
*/
class Product extends ProductRow {

  public function __construct(Table $table, array $data) {
    parent::__construct($table, $data);
  }

}
