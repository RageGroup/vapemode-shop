<?php namespace App\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Row;
use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table base for all Customers.
 * This class was auto-generated.
 */
class CustomerTable extends \Core\Data\Table {

  /**
   * Create the table for a context and with table information.
   *
   * @param DbContext $context
   * @param array $columns
   */
  public function __construct(DbContext $context, array $columns) {
    parent::__construct($context, 'Customers', '', ['CustomerId'], $columns);
  }

  /**
   * Get a row by its data.
   * Exists to be available for the table base implementation.
   *
   * @param array $data
   * @return Row
   */
  protected function newRow(array $data): Row {
    $row = new Customer($this, $data, false);
    $this->selectedRows[$row->getPkHash()] = $row;
    return $row;
  }

  /**
  * Create a new Customer.
  *
  * @return Customer
  */
  public function create(
    Uuid $customerId,
    string $idCardNo,
    Uuid $billingAddressId,
    Uuid $defaultShippingAddressId):
    Customer
  {
    $entity = new Customer($this, [
      'CustomerId' => $customerId->toString(),
      'IdCardNo' => $idCardNo,
      'BillingAddressId' => $billingAddressId->toString(),
      'DefaultShippingAddressId' => $defaultShippingAddressId->toString()
    ], false);
    $hash = $entity->getPkHash();
    $this->selectedRows[$hash] = $entity;
    $this->insertedRows[$hash] = $entity;
    return $entity;
  }

  /**
   * Get an entity object by its primary key value.
   *
   * @param Uuid $customerId
   * @return Customer
   */
  public function getByPk(Uuid $customerId): ?Customer {
    return $this->getFirstRow([new SqlParameter('CustomerId', '=', $customerId)]);
  }


}
