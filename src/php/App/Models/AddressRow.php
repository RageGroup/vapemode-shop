<?php namespace App\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Table;

/**
 * A data table row for one Address.
 * This class was auto-generated.
 */
abstract class AddressRow extends \Core\Data\Row {

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public static function pkHash(Uuid $addressId): string {
    $hash = '';
    $hash = \md5($hash . ((string) $addressId), true);
    return $hash;
  }

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public function getPkHash(): string {
    return self::pkHash($this->getUuid('AddressId'));
  }

  /**
   * Get the localized description of the Address ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getAddressIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('AddressId', $languageCode);
  }

  /**
   * Get the Address ID.
   *
   * @return Uuid
   */
  public function getAddressId(): ?Uuid {
    return $this->getUuid('AddressId');
  }

  /**
   * Set the Address ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setAddressId(?Uuid $value): void {
    $this->set('AddressId', $value);
  }

  /**
   * Get the localized description of the Postcode.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getPostcodeDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('Postcode', $languageCode);
  }

  /**
   * Get the Postcode.
   *
   * @return string
   */
  public function getPostcode(): ?string {
    return $this->getString('Postcode');
  }

  /**
   * Set the Postcode.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setPostcode(?string $value): void {
    $this->set('Postcode', $value);
  }

  /**
   * Get the localized description of the City.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getCityDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('City', $languageCode);
  }

  /**
   * Get the City.
   *
   * @return string
   */
  public function getCity(): ?string {
    return $this->getString('City');
  }

  /**
   * Set the City.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setCity(?string $value): void {
    $this->set('City', $value);
  }

  /**
   * Get the localized description of the Street and House No..
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getStreetHouseNoDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('StreetHouseNo', $languageCode);
  }

  /**
   * Get the Street and House No..
   *
   * @return string
   */
  public function getStreetHouseNo(): ?string {
    return $this->getString('StreetHouseNo');
  }

  /**
   * Set the Street and House No..
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setStreetHouseNo(?string $value): void {
    $this->set('StreetHouseNo', $value);
  }

  /**
   * Create an entity object for a table with initial data.
   *
   * @param Table $table
   * @param array $data
   */
  public function __construct(Table $table, array $data, bool $inserted = true) {
    parent::__construct($table, $data, $inserted);
  }

}
