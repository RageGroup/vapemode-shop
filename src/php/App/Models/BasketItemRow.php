<?php namespace App\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Table;

/**
 * A data table row for one Basket Item.
 * This class was auto-generated.
 */
abstract class BasketItemRow extends \Core\Data\Row {

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public static function pkHash(Uuid $basketItemId): string {
    $hash = '';
    $hash = \md5($hash . ((string) $basketItemId), true);
    return $hash;
  }

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public function getPkHash(): string {
    return self::pkHash($this->getUuid('BasketItemId'));
  }

  /**
   * Get the localized description of the Basket Item ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getBasketItemIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('BasketItemId', $languageCode);
  }

  /**
   * Get the Basket Item ID.
   *
   * @return Uuid
   */
  public function getBasketItemId(): ?Uuid {
    return $this->getUuid('BasketItemId');
  }

  /**
   * Set the Basket Item ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setBasketItemId(?Uuid $value): void {
    $this->set('BasketItemId', $value);
  }

  /**
   * Get the localized description of the Customer ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getCustomerIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('CustomerId', $languageCode);
  }

  /**
   * Get the Customer ID.
   *
   * @return Uuid
   */
  public function getCustomerId(): ?Uuid {
    return $this->getUuid('CustomerId');
  }

  /**
   * Set the Customer ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setCustomerId(?Uuid $value): void {
    $this->set('CustomerId', $value);
  }

  /**
   * Get the localized description of the Creation Date.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getCreationDateDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('CreationDate', $languageCode);
  }

  /**
   * Get the Creation Date.
   *
   * @return DateTime
   */
  public function getCreationDate(): ?DateTime {
    return $this->getDateTime('CreationDate');
  }

  /**
   * Set the Creation Date.
   *
   * @param DateTime $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setCreationDate(?DateTime $value): void {
    $this->set('CreationDate', $value);
  }

  /**
   * Get the localized description of the Product ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getProductIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('ProductId', $languageCode);
  }

  /**
   * Get the Product ID.
   *
   * @return Uuid
   */
  public function getProductId(): ?Uuid {
    return $this->getUuid('ProductId');
  }

  /**
   * Set the Product ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setProductId(?Uuid $value): void {
    $this->set('ProductId', $value);
  }

  /**
   * Get the localized description of the Quantity.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getQuantityDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('Quantity', $languageCode);
  }

  /**
   * Get the Quantity.
   *
   * @return int
   */
  public function getQuantity(): ?int {
    return $this->getInt('Quantity');
  }

  /**
   * Set the Quantity.
   *
   * @param int $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setQuantity(?int $value): void {
    $this->set('Quantity', $value);
  }

  /**
   * Create an entity object for a table with initial data.
   *
   * @param Table $table
   * @param array $data
   */
  public function __construct(Table $table, array $data, bool $inserted = true) {
    parent::__construct($table, $data, $inserted);
  }

}
