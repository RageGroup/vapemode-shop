<?php namespace App\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Table;

/**
 * A data table row for one Product.
 * This class was auto-generated.
 */
abstract class ProductRow extends \Core\Data\Row {

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public static function pkHash(Uuid $productId): string {
    $hash = '';
    $hash = \md5($hash . ((string) $productId), true);
    return $hash;
  }

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public function getPkHash(): string {
    return self::pkHash($this->getUuid('ProductId'));
  }

  /**
   * Get the localized description of the Product ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getProductIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('ProductId', $languageCode);
  }

  /**
   * Get the Product ID.
   *
   * @return Uuid
   */
  public function getProductId(): ?Uuid {
    return $this->getUuid('ProductId');
  }

  /**
   * Set the Product ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setProductId(?Uuid $value): void {
    $this->set('ProductId', $value);
  }

  /**
   * Get the localized description of the Product Name.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getProductNameDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('ProductName', $languageCode, true);
  }

  /**
   * Get the Product Name.
   *
   * @return string
   */
  public function getProductName(?string $languageCode = null): ?string {
    return $this->table->getLocalizedString($this, 'ProductName', $languageCode);
  }

  /**
   * Set the Product Name.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setProductName(?string $value, ?string $languageCode = null): void {
    $this->setLocalizedString('ProductName', $languageCode, $value);
  }

  /**
   * Get the localized description of the Produkt Type ('A' => Aroma, 'B' => Base, 'L' => Liquid).
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getProductTypeDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('ProductType', $languageCode);
  }

  /**
   * Get the Produkt Type ('A' => Aroma, 'B' => Base, 'L' => Liquid).
   *
   * @return string
   */
  public function getProductType(): ?string {
    return $this->getString('ProductType');
  }

  /**
   * Set the Produkt Type ('A' => Aroma, 'B' => Base, 'L' => Liquid).
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setProductType(?string $value): void {
    $this->set('ProductType', $value);
  }

  /**
   * Get the localized description of the Stock.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getStockDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('Stock', $languageCode);
  }

  /**
   * Get the Stock.
   *
   * @return int
   */
  public function getStock(): ?int {
    return $this->getInt('Stock');
  }

  /**
   * Set the Stock.
   *
   * @param int $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setStock(?int $value): void {
    $this->set('Stock', $value);
  }

  /**
   * Get the localized description of the Unit Price.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getUnitPriceDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('UnitPrice', $languageCode);
  }

  /**
   * Get the Unit Price.
   *
   * @return float
   */
  public function getUnitPrice(): ?float {
    return $this->getFloat('UnitPrice');
  }

  /**
   * Set the Unit Price.
   *
   * @param float $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setUnitPrice(?float $value): void {
    $this->set('UnitPrice', $value);
  }

  /**
   * Get the localized description of the Nicotine.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getNicotineDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('Nicotine', $languageCode);
  }

  /**
   * Get the Nicotine.
   *
   * @return int
   */
  public function getNicotine(): ?int {
    return $this->getInt('Nicotine');
  }

  /**
   * Set the Nicotine.
   *
   * @param int $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setNicotine(?int $value): void {
    $this->set('Nicotine', $value);
  }

  /**
   * Get the localized description of the Content.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getContentDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('Content', $languageCode);
  }

  /**
   * Get the Content.
   *
   * @return int
   */
  public function getContent(): ?int {
    return $this->getInt('Content');
  }

  /**
   * Set the Content.
   *
   * @param int $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setContent(?int $value): void {
    $this->set('Content', $value);
  }

  /**
   * Get the localized description of the Mix Recommendation, Minumum.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getMixRecommendationMinDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('MixRecommendationMin', $languageCode);
  }

  /**
   * Get the Mix Recommendation, Minumum.
   *
   * @return int
   */
  public function getMixRecommendationMin(): ?int {
    return $this->getInt('MixRecommendationMin');
  }

  /**
   * Set the Mix Recommendation, Minumum.
   *
   * @param int $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setMixRecommendationMin(?int $value): void {
    $this->set('MixRecommendationMin', $value);
  }

  /**
   * Get the localized description of the Mix Recommendation, Maximum.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getMixRecommendationMaxDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('MixRecommendationMax', $languageCode);
  }

  /**
   * Get the Mix Recommendation, Maximum.
   *
   * @return int
   */
  public function getMixRecommendationMax(): ?int {
    return $this->getInt('MixRecommendationMax');
  }

  /**
   * Set the Mix Recommendation, Maximum.
   *
   * @param int $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setMixRecommendationMax(?int $value): void {
    $this->set('MixRecommendationMax', $value);
  }

  /**
   * Get the localized description of the Flavour.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getFlavourDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('Flavour', $languageCode, true);
  }

  /**
   * Get the Flavour.
   *
   * @return string
   */
  public function getFlavour(?string $languageCode = null): ?string {
    return $this->table->getLocalizedString($this, 'Flavour', $languageCode);
  }

  /**
   * Set the Flavour.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setFlavour(?string $value, ?string $languageCode = null): void {
    $this->setLocalizedString('Flavour', $languageCode, $value);
  }

  /**
   * Get the localized description of the Manufacturer.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getManufacturerDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('Manufacturer', $languageCode);
  }

  /**
   * Get the Manufacturer.
   *
   * @return string
   */
  public function getManufacturer(): ?string {
    return $this->getString('Manufacturer');
  }

  /**
   * Set the Manufacturer.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setManufacturer(?string $value): void {
    $this->set('Manufacturer', $value);
  }

  /**
   * Get the localized description of the Image File Name.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getImageFileNameDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('ImageFileName', $languageCode);
  }

  /**
   * Get the Image File Name.
   *
   * @return string
   */
  public function getImageFileName(): ?string {
    return $this->getString('ImageFileName');
  }

  /**
   * Set the Image File Name.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setImageFileName(?string $value): void {
    $this->set('ImageFileName', $value);
  }

  /**
   * Create an entity object for a table with initial data.
   *
   * @param Table $table
   * @param array $data
   */
  public function __construct(Table $table, array $data, bool $inserted = true) {
    parent::__construct($table, $data, $inserted);
  }

}
