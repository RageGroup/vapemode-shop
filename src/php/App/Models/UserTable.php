<?php namespace App\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Row;
use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table base for all Users.
 * This class was auto-generated.
 */
class UserTable extends \Core\Models\Users {

  /**
   * Create the table for a context and with table information.
   *
   * @param DbContext $context
   * @param array $columns
   */
  public function __construct(DbContext $context, array $columns) {
    parent::__construct($context, 'Users', '', ['UserId'], $columns);
  }

  /**
   * Get a row by its data.
   * Exists to be available for the table base implementation.
   *
   * @param array $data
   * @return Row
   */
  protected function newRow(array $data): Row {
    $row = new User($this, $data, false);
    $this->selectedRows[$row->getPkHash()] = $row;
    return $row;
  }

  /**
  * Create a new User.
  *
  * @return User
  */
  public function create(
    string $userName,
    string $email,
    string $firstName,
    string $lastName):
    User
  {
    $userId = Uuid::generate();
    $entity = new User($this, [
      'UserId' => $userId->toString(),
      'UserName' => $userName,
      'Email' => $email,
      'FirstName' => $firstName,
      'LastName' => $lastName
    ], false);
    $hash = $entity->getPkHash();
    $this->selectedRows[$hash] = $entity;
    $this->insertedRows[$hash] = $entity;
    return $entity;
  }

  /**
   * Get an entity object by its primary key value.
   *
   * @param Uuid $userId
   * @return User
   */
  public function getByPk(Uuid $userId): ?User {
    return $this->getFirstRow([new SqlParameter('UserId', '=', $userId)]);
  }


}
