<?php namespace App\Models;

use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table for Users.
 */
class Users extends UserTable {

  /**
   * Create the table for a context and with table information.
   *
   * @param VapeModeContext $context
   * @param array $columns
   */
  public function __construct(VapeModeContext $context, array $columns) {
    parent::__construct($context, $columns);
  }

  public function getByEmail(string $email): ?User {
    return $this->getFirstRow([new SqlParameter('Email', '=', $email)]);
  }

}
