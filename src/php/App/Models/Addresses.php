<?php namespace App\Models;

use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table for Addresses.
 */
class Addresses extends AddressTable {

  /**
   * Create the table for Addresses.
   *
   * @param VapeModeContext $context
   * @param array $columns
   */
  public function __construct(VapeModeContext $context, array $columns) {
    parent::__construct($context, $columns);
  }

}
