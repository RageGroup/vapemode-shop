<?php namespace App\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Table;

/**
 * A data table row for one User.
 * This class was auto-generated.
 */
abstract class UserRow extends \Core\Models\User {

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public static function pkHash(Uuid $userId): string {
    $hash = '';
    $hash = \md5($hash . ((string) $userId), true);
    return $hash;
  }

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public function getPkHash(): string {
    return self::pkHash($this->getUuid('UserId'));
  }

  /**
   * Get the localized description of the E-Mail Address.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getEmailDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('Email', $languageCode);
  }

  /**
   * Get the E-Mail Address.
   *
   * @return string
   */
  public function getEmail(): ?string {
    return $this->getString('Email');
  }

  /**
   * Set the E-Mail Address.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setEmail(?string $value): void {
    $this->set('Email', $value);
  }

  /**
   * Get the localized description of the First Name.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getFirstNameDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('FirstName', $languageCode);
  }

  /**
   * Get the First Name.
   *
   * @return string
   */
  public function getFirstName(): ?string {
    return $this->getString('FirstName');
  }

  /**
   * Set the First Name.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setFirstName(?string $value): void {
    $this->set('FirstName', $value);
  }

  /**
   * Get the localized description of the Last Name.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getLastNameDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('LastName', $languageCode);
  }

  /**
   * Get the Last Name.
   *
   * @return string
   */
  public function getLastName(): ?string {
    return $this->getString('LastName');
  }

  /**
   * Set the Last Name.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setLastName(?string $value): void {
    $this->set('LastName', $value);
  }

  /**
   * Get the localized description of the User Agent.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getUserAgentDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('UserAgent', $languageCode);
  }

  /**
   * Get the User Agent.
   *
   * @return string
   */
  public function getUserAgent(): ?string {
    return $this->getString('UserAgent');
  }

  /**
   * Set the User Agent.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setUserAgent(?string $value): void {
    $this->set('UserAgent', $value);
  }

  /**
   * Get the localized description of the Remote IP Address.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getRemoteIpAddressDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('RemoteIpAddress', $languageCode);
  }

  /**
   * Get the Remote IP Address.
   *
   * @return string
   */
  public function getRemoteIpAddress(): ?string {
    return $this->getString('RemoteIpAddress');
  }

  /**
   * Set the Remote IP Address.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setRemoteIpAddress(?string $value): void {
    $this->set('RemoteIpAddress', $value);
  }

  /**
   * Get the localized description of the Forwarded IP Address.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getForwardedIpAddressDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('ForwardedIpAddress', $languageCode);
  }

  /**
   * Get the Forwarded IP Address.
   *
   * @return string
   */
  public function getForwardedIpAddress(): ?string {
    return $this->getString('ForwardedIpAddress');
  }

  /**
   * Set the Forwarded IP Address.
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setForwardedIpAddress(?string $value): void {
    $this->set('ForwardedIpAddress', $value);
  }

  /**
   * Create an entity object for a table with initial data.
   *
   * @param Table $table
   * @param array $data
   */
  public function __construct(Table $table, array $data, bool $inserted = true) {
    parent::__construct($table, $data, $inserted);
  }

}
