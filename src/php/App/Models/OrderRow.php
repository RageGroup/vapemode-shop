<?php namespace App\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Table;

/**
 * A data table row for one Order.
 * This class was auto-generated.
 */
abstract class OrderRow extends \Core\Data\Row {

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public static function pkHash(Uuid $orderId): string {
    $hash = '';
    $hash = \md5($hash . ((string) $orderId), true);
    return $hash;
  }

  /**
   * Get the hash of the entity objects primary key.
   *
   * @return string
   */
  public function getPkHash(): string {
    return self::pkHash($this->getUuid('OrderId'));
  }

  /**
   * Get the localized description of the Order ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getOrderIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('OrderId', $languageCode);
  }

  /**
   * Get the Order ID.
   *
   * @return Uuid
   */
  public function getOrderId(): ?Uuid {
    return $this->getUuid('OrderId');
  }

  /**
   * Set the Order ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setOrderId(?Uuid $value): void {
    $this->set('OrderId', $value);
  }

  /**
   * Get the localized description of the Order Date.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getOrderDateDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('OrderDate', $languageCode);
  }

  /**
   * Get the Order Date.
   *
   * @return DateTime
   */
  public function getOrderDate(): ?DateTime {
    return $this->getDateTime('OrderDate');
  }

  /**
   * Set the Order Date.
   *
   * @param DateTime $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setOrderDate(?DateTime $value): void {
    $this->set('OrderDate', $value);
  }

  /**
   * Get the localized description of the Order State ('O' => Open, 'C' => Closed, 'I' => Invalid).
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getOrderStateDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('OrderState', $languageCode);
  }

  /**
   * Get the Order State ('O' => Open, 'C' => Closed, 'I' => Invalid).
   *
   * @return string
   */
  public function getOrderState(): ?string {
    return $this->getString('OrderState');
  }

  /**
   * Set the Order State ('O' => Open, 'C' => Closed, 'I' => Invalid).
   *
   * @param string $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setOrderState(?string $value): void {
    $this->set('OrderState', $value);
  }

  /**
   * Get the localized description of the Customer ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getCustomerIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('CustomerId', $languageCode);
  }

  /**
   * Get the Customer ID.
   *
   * @return Uuid
   */
  public function getCustomerId(): ?Uuid {
    return $this->getUuid('CustomerId');
  }

  /**
   * Set the Customer ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setCustomerId(?Uuid $value): void {
    $this->set('CustomerId', $value);
  }

  /**
   * Get the localized description of the Shipping Address ID.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getShippingAddressIdDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('ShippingAddressId', $languageCode);
  }

  /**
   * Get the Shipping Address ID.
   *
   * @return Uuid
   */
  public function getShippingAddressId(): ?Uuid {
    return $this->getUuid('ShippingAddressId');
  }

  /**
   * Set the Shipping Address ID.
   *
   * @param Uuid $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setShippingAddressId(?Uuid $value): void {
    $this->set('ShippingAddressId', $value);
  }

  /**
   * Get the localized description of the Total Price.
   *
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return string
   */
  public function getTotalPriceDescription(?string $languageCode = null): ?string {
    return $this->table->getDescription('TotalPrice', $languageCode);
  }

  /**
   * Get the Total Price.
   *
   * @return float
   */
  public function getTotalPrice(): ?float {
    return $this->getFloat('TotalPrice');
  }

  /**
   * Set the Total Price.
   *
   * @param float $value The column value.
   * @param string $languageCode The code of the language (ISO 639-1).
   * @return void
   */
  public function setTotalPrice(?float $value): void {
    $this->set('TotalPrice', $value);
  }

  /**
   * Create an entity object for a table with initial data.
   *
   * @param Table $table
   * @param array $data
   */
  public function __construct(Table $table, array $data, bool $inserted = true) {
    parent::__construct($table, $data, $inserted);
  }

}
