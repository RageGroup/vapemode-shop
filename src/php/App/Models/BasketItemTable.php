<?php namespace App\Models;

use \Core\Data\DateTime;
use \Core\Data\Uuid;
use \Core\Data\Row;
use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table base for all Basket Items.
 * This class was auto-generated.
 */
class BasketItemTable extends \Core\Data\Table {

  /**
   * Create the table for a context and with table information.
   *
   * @param DbContext $context
   * @param array $columns
   */
  public function __construct(DbContext $context, array $columns) {
    parent::__construct($context, 'BasketItems', '', ['BasketItemId'], $columns);
  }

  /**
   * Get a row by its data.
   * Exists to be available for the table base implementation.
   *
   * @param array $data
   * @return Row
   */
  protected function newRow(array $data): Row {
    $row = new BasketItem($this, $data, false);
    $this->selectedRows[$row->getPkHash()] = $row;
    return $row;
  }

  /**
  * Create a new Basket Item.
  *
  * @return BasketItem
  */
  public function create(
    Uuid $customerId,
    DateTime $creationDate,
    Uuid $productId,
    int $quantity):
    BasketItem
  {
    $basketItemId = Uuid::generate();
    $entity = new BasketItem($this, [
      'BasketItemId' => $basketItemId->toString(),
      'CustomerId' => $customerId->toString(),
      'CreationDate' => $creationDate,
      'ProductId' => $productId->toString(),
      'Quantity' => $quantity
    ], false);
    $hash = $entity->getPkHash();
    $this->selectedRows[$hash] = $entity;
    $this->insertedRows[$hash] = $entity;
    return $entity;
  }

  /**
   * Get an entity object by its primary key value.
   *
   * @param Uuid $basketItemId
   * @return BasketItem
   */
  public function getByPk(Uuid $basketItemId): ?BasketItem {
    return $this->getFirstRow([new SqlParameter('BasketItemId', '=', $basketItemId)]);
  }


}
