<?php namespace App\Models;

use \Core\Data\Table;

/**
* A user of the web app.
*/
final class User extends UserRow {

  /**
  * Create an user object for a table with initial data.
  * May not be called directly.
  *
  * @param Table $table
  * @param array $data
  */
  public function __construct(Table $table, array $data) {
    parent::__construct($table, $data);
  }

  public function getRole(): string {
    // $context = $this->table->getContext();
    // $roles = $context->getUserRoles()->getByUserId($this->getUserId());
    // return count($roles) > 0 ? 'C' : 'G';
    return 'C';
  }

  public function getAlias(): string {
    return $this->getFirstName() . ' ' . $this->getLastName();
  }

}
