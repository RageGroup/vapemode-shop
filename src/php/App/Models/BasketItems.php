<?php namespace App\Models;

use \Core\Data\Uuid;
use \Core\Data\DbContext;
use \Core\Data\SqlParameter;

/**
 * A data table for BasketItems.
 */
class BasketItems extends BasketItemTable {

  /**
   * Create the table for BasketItems.
   *
   * @param VapeModeContext $context
   * @param array $columns
   */
  public function __construct(VapeModeContext $context, array $columns) {
    parent::__construct($context, $columns);
  }

  /**
  * Get basket items by their customer id.
  *
  * @param string $productId
  * @return array
  */
  public function getByCustomerId(Uuid $customerId): array {
    $conditions = [];
    $conditions[] = new SqlParameter('CustomerId', '=', $customerId->toString());
    return $this->getRows($conditions);
  }


  /**
  * Get a basket item by its product id.
  *
  * @param string $productId
  * @return Row
  */
  public function getByProductId(Uuid $productId): ?BasketItem {
    $conditions = [];
    $conditions[] = new SqlParameter('ProductId', '=', $productId->toString());
    return $this->getFirstRow($conditions);
  }


}
