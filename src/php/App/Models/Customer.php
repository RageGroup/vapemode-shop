<?php namespace App\Models;

use \Core\Data\Table;

/**
* A customer.
*/
final class Customer extends CustomerRow {

  /**
  * Create a customer object for a table with initial data.
  * May not be called directly.
  *
  * @param Table $table
  * @param array $data
  */
  public function __construct(Table $table, array $data) {
    parent::__construct($table, $data);
  }

}
