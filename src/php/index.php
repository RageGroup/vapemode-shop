<?php

// $useCaching = true;
$useCaching = false;

require_once __DIR__ . '/Core/AutoLoader.php';

$autoLoader = new \Core\AutoLoader(__DIR__);
$autoLoader->start();

$errorHandler = new \Core\ErrorHandler();
$errorHandler->start();

$router = \Core\Web\Router::getDefault(__DIR__, $useCaching);
$response = $router->route();
$response->render();
