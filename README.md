# VapeMode-Shop

Project Status: [TODO.md](TODO.md)

## Pre-Requisites

1. Apache web server with the ability to execute PHP
2. MySQL-Server

The project was developed & tested with XAMPP Version 7.2.7:
* Apache 2.4.33
* PHP 7.2.7 (VC15 X86 32bit thread safe) + PEAR)
* MariaDB 10.1.34

## Installation & Configuration

1. MariaDB/MySQL
    1. Execute "src/sql/Generated-Create-Tables.sql"
    2. Execute "src/sql/Generated-Insert-Rows.sql"
    3. Edit the file "config/db-config.json" - adapt access information to your database
2. Apache & PHP
    1. Simple way: place the project directory into apaches directory "htdocs"
    2. Call the landing page by browsing for example to **"localhost/vapemode-shop/src/php/"**; Only the file "src/php/index.php" will be used for content access (besides the assets)

## Sites

* General
    * Landing Page (links to categories; special offer; search box)
    * Support & Contact & Imprint
* Account
    * Registration
    * Login
    * Basket
    * Orders
* Products
    * Overview (searchbox; filter by: Liquids, Aromas, Nicotine Shots, Special Offers)
    * Search & Results Page
    * Detail
    * Mix Creation
